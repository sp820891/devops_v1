<x-app-layout :assets="$assets ?? []">
    <div class="card">
        <div class="card-header">
            <div class="header-title">
                <h5 class="card-title">Sprint Retrospective </h5>
            </div>
            <div class="row gx-5 selectors">
                <div class="form-group col-md-3">
                    <label class="form-label">Sprint</label>
                    <select class="form-select shadow-none text-secondary"
                        data-placeholder="Choose a role..." name="sprint" id="sprint">
                        <option selected value="">sprint</option>
                        @foreach ($sprint as $sprints)
                            <option value="{{ $sprints->id }}" class="data">{{ $sprints->name }}</option>
                        @endforeach
                    </select>
                    <small class="text-danger" id="sprint_small"></small>
                </div>
                @hasanyrole('DevOps Admin|DevOps Manager')
                <div class="form-group col-md-3 pp_hide">
                    <label class="form-label">Resource</label>
                    <select class="form-select  shadow-none text-secondary"
                        data-placeholder="Choose resource" name="resource" id="resource">
                        <option selected value="">Resource</option>
                        @foreach ($assignee as $user)
                            <option value="{{ $user->id }}">
                                {{ $user->full_name }}</option>
                        @endforeach
                    </select>
                </div>
                @endhasanyrole
                @can('can create sprint retrospective')
                <button type="button" class="btn btn-primary pp_hide_btn  col-md-2" id="create_sprint_retro">
                    <i class="btn-inner">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-2" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                        </svg>
                    </i>
                    <span>Create</span>
                </button>
                @endcan
                @can('can view sprint retrospective')
                @hasanyrole('DevOps Admin|DevOps Manager')
                <button type="button" class="btn btn-success pp_hide_btn  col-md-2" id="view_sprint_retro">
                    <i class="btn-inner">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 5C6.55576 5 3.53109 9.23425 2.45554 11.1164C2.23488 11.5025 2.12456 11.6956 2.1367 11.9836C2.14885 12.2716 2.27857 12.4598 2.53799 12.8362C3.8182 14.6935 7.29389 19 12 19C16.7061 19 20.1818 14.6935 21.462 12.8362C21.7214 12.4598 21.8511 12.2716 21.8633 11.9836C21.8754 11.6956 21.7651 11.5025 21.5445 11.1164C20.4689 9.23425 17.4442 5 12 5Z" stroke="currentColor" stroke-width="2"/>
                            <circle cx="12" cy="12" r="4" fill="currentColor"/>
                        </svg>
                    </i>
                    <span>View</span>
                </button>
                @else
                <button type="button" class="btn btn-success pp_hide_btn_emp  col-md-2" id="view_sprint_retro_emp">
                    <i class="btn-inner">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 5C6.55576 5 3.53109 9.23425 2.45554 11.1164C2.23488 11.5025 2.12456 11.6956 2.1367 11.9836C2.14885 12.2716 2.27857 12.4598 2.53799 12.8362C3.8182 14.6935 7.29389 19 12 19C16.7061 19 20.1818 14.6935 21.462 12.8362C21.7214 12.4598 21.8511 12.2716 21.8633 11.9836C21.8754 11.6956 21.7651 11.5025 21.5445 11.1164C20.4689 9.23425 17.4442 5 12 5Z" stroke="currentColor" stroke-width="2"/>
                            <circle cx="12" cy="12" r="4" fill="currentColor"/>
                        </svg>
                    </i>
                    <span>View</span>
                </button>
                @endhasanyrole
                @endcan
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive scrollbar">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th rowspan='2' colspan="1" id="sprint_col_retro"><span style="position: relative;bottom: 29px;">Sprint</span></th>
                            <th rowspan='2' colspan="1" id="emp_col_retro"><span style="position: relative;bottom: 29px;">Employee name</span></th>
                            <th rowspan='2' colspan="1"><span style="position: relative;bottom: 29px;">Key perfomance indicator</span></th>
                            <th rowspan='2' colspan="1"><span style="position: relative;bottom: 29px;">Point/score</span></th>
                            <th rowspan='2' colspan="1"><span style="position: relative;bottom: 29px;">Awarded points/score</span></th>
                            <th rowspan='1' colspan="2" style="text-align: center;">Reason for assessment score</th>
                            <th rowspan='2' colspan="1" id="action_col_retro"><span style="position: relative;bottom: 29px;">action</span></th>
                        </tr>
                        <tr>
                            <th rowspan='1' colspan="1">Assessmant score comments</th>
                            <th rowspan='1' colspan="1">Improvement comments</th>
                        </tr>
                    </thead>
                    <tbody id="tbody"></tbody>
                </table>
            </div>
        </div>
    </div>
</x-app-layout>
<script src="{{asset('js/sprint/sprint_retrospective.js')}}"></script>

