<x-app-layout :assets="$assets ?? []">
    <style>
        .sprint_day {
            display: none;
        }

        .sprint_show {
            display: block;
        }

        .ml-3 {
            margin-left: 17px;
        }

        .ml-4 {
            margin-left: 125px;
        }
    </style>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <h5 class="card-title mb-0"> <span>
                            <svg width="30" height="30" viewBox="0 0 25 24" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M7.5 4.43245C7.5 4.27684 7.5 4.19903 7.4491 4.15423C7.39819 4.10944 7.32244 4.11915 7.17094 4.13857C6.04965 4.28229 5.26806 4.57508 4.67157 5.17157C3.5 6.34315 3.5 8.22876 3.5 12C3.5 15.7712 3.5 17.6569 4.67157 18.8284C5.84315 20 7.72876 20 11.5 20H13.5C17.2712 20 19.1569 20 20.3284 18.8284C21.5 17.6569 21.5 15.7712 21.5 12C21.5 8.22876 21.5 6.34315 20.3284 5.17157C19.7319 4.57508 18.9504 4.28229 17.8291 4.13857C17.6776 4.11915 17.6018 4.10944 17.5509 4.15424C17.5 4.19903 17.5 4.27684 17.5 4.43245L17.5 6.5C17.5 7.32843 16.8284 8 16 8C15.1716 8 14.5 7.32843 14.5 6.5L14.5 4.30005C14.5 4.15898 14.5 4.08844 14.4561 4.04451C14.4123 4.00059 14.3418 4.0005 14.2009 4.00031C13.9748 4 13.7412 4 13.5 4H11.5C11.2588 4 11.0252 4 10.7991 4.00031C10.6582 4.0005 10.5877 4.00059 10.5439 4.04452C10.5 4.08844 10.5 4.15898 10.5 4.30005L10.5 6.5C10.5 7.32843 9.82843 8 9.00001 8C8.17158 8 7.5 7.32843 7.5 6.5L7.5 4.43245Z"
                                    fill="currentColor" fill-opacity="0.25" />
                                <path d="M9 2.5L9 6.5" stroke="currentColor" stroke-linecap="round" />
                                <path d="M16 2.5L16 6.5" stroke="currentColor" stroke-linecap="round" />
                                <circle cx="8" cy="10.5" r="0.5" fill="currentColor" />
                                <circle cx="11" cy="10.5" r="0.5" fill="currentColor" />
                                <circle cx="14" cy="10.5" r="0.5" fill="currentColor" />
                                <circle cx="17" cy="10.5" r="0.5" fill="currentColor" />
                                <circle cx="8" cy="13.5" r="0.5" fill="currentColor" />
                                <circle cx="11" cy="13.5" r="0.5" fill="currentColor" />
                                <circle cx="14" cy="13.5" r="0.5" fill="currentColor" />
                                <circle cx="17" cy="13.5" r="0.5" fill="currentColor" />
                                <circle cx="8" cy="16.5" r="0.5" fill="currentColor" />
                                <circle cx="11" cy="16.5" r="0.5" fill="currentColor" />
                                <circle cx="14" cy="16.5" r="0.5" fill="currentColor" />
                                <circle cx="17" cy="16.5" r="0.5" fill="currentColor" />
                            </svg>
                        </span>Sprint Planning</h5>
                    <a href="{{ url('sprint_planning') }}">
                        <img
                            src="data:image/png;base64,
                            iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAoUlEQVR4nN3UsQpBcRTH8c+AFzAY
                            TAaDwWRUZl0PYVQ2u8UbsPECZh7AinKT3epBdOsqlOFef5Fv/frXGb7/TqdzeKSHuUB0sUX5v2UJZ1wQZ8wBM9Q80cApfbNQQJR2Nwwl
                            TShh+Qnp7lX7E/mIMBWQYjqsoMQ/LaxgE1I4xiCUrIl9Opi36eCI+q3QypE2+lhjher9D4scSY7CKOdWfZkr7qstZzIrX1EAAAAASUVOR
                            K5CYII=">
                    </a>
                </div>
                {{-- <div class="buttons mb-3">
                    <button class="dayone  btn btn-outline-primary ml-4">Day1</button>
                    <button class="daytwo btn btn-outline-primary ml-4">Day2</button>
                    <button class="daythree btn btn-outline-primary ml-4">Day3</button>
                    <button class="dayfour btn btn-outline-primary ml-4">Day4</button>
                    <button class="dayfive btn btn-outline-primary ml-4">Day5</button>
                </div> --}}

                <div class="card-body">
                    <div class="table-responsive">

                        <table id="sprint_planning_table" class="table table-hover" data-ordering="false">
                            <thead>
                                <tr>
                                    <input type="hidden" id="sprint_project" value="{{ $id }}">
                                    <th>S.no</th>
                                    <th>Sprint </th>
                                    <th>Sprint Goal</th>
                                    <th>Name</th>

                                    {{-- <th>D1</th>
                                    <th>D2</th>
                                    <th>D3</th>
                                    <th>D4</th>
                                    <th>D5</th> --}}
                                    <th>Estimate Time</th>
                                    <th>Spent Time</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
<script type="text/javascript">
    $(function() {
        var i = 1;
        var id = $('#sprint_project').val();
        var table = $('#sprint_planning_table').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ajax: "/sprint-of-project/" + id,
            columns: [{
                    render: function(data, type, full, meta) {
                        return i++;
                    }
                },
                {
                    data: 'Sprint',
                    name: 'Sprint'
                },
                {
                    data: 'sprintgoal',
                    name: 'sprintgoal',
                    render: function(data, type, row) {
                        if (row.sprintgoal) {
                            $sprint = `<a href="${data}">${data}</h5>`
                            return $sprint;
                        }
                    }
                },
                {
                    data: 'full_name',
                    name: 'full_name'
                },


                // {
                //     data: 'day_1',
                //     name: 'day_1',
                //     class: 'sprint_day sprint_day_1',
                //     render: function(data, type, row) {
                //         if (row.day_1 === null) {
                //             return '_';
                //         } else {
                //             return data;
                //         }
                //     }
                // },
                // {
                //     data: 'day_2',
                //     name: 'day_2',
                //     class: 'sprint_day sprint_day_2',

                //     render: function(data, type, row) {
                //         if (row.day_2 === null) {
                //             return '_';
                //         } else {
                //             return data;
                //         }
                //     }
                // },
                // {
                //     data: 'day_3',
                //     name: 'day_3',
                //     class: 'sprint_day sprint_day_3',

                //     render: function(data, type, row) {
                //         if (row.day_3 === null) {
                //             return '_';
                //         } else {
                //             return data;
                //         }
                //     }
                // },
                // {
                //     data: 'day_4',
                //     name: 'day_4',
                //     class: 'sprint_day sprint_day_4',

                //     render: function(data, type, row) {
                //         if (row.day_4 === null) {
                //             return '_';
                //         } else {
                //             return data;
                //         }
                //     }
                // },
                // {
                //     data: 'day_5',
                //     name: 'day_5',
                //     class: 'sprint_day sprint_day_5',

                //     render: function(data, type, row) {
                //         if (row.day_5 === null) {
                //             return '_';
                //         } else {
                //             return data;
                //         }
                //     }
                // },
                {
                    data: 'estimate_time',
                    name: 'estimate_time',



                },
                {
                    data: 'spendtime',
                    name: 'spendtime',



                },
            ]
        });

    });

    $(document).ready(function() {
        var id = $('#sprint_project').val();
        $.ajax({
            url: "/sprint-of-project/" + id,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "json",
            success: function(data) {
                console.log('success:' + data);
            }
        })
    });

    // $(document).on('click', '.dayone ', function(e) {
    //     e.preventDefault();
    //     $(".sprint_day_1").addClass("sprint_show");
    //     $(".sprint_day_2").removeClass("sprint_show");
    //     $(".sprint_day_3").removeClass("sprint_show");
    //     $(".sprint_day_4").removeClass("sprint_show");
    //     $(".sprint_day_5").removeClass("sprint_show");
    // });
    // $(document).on('click', '.daytwo ', function(e) {
    //     e.preventDefault();
    //     $(".sprint_day_1").removeClass("sprint_show");
    //     $(".sprint_day_2").addClass("sprint_show");
    //     $(".sprint_day_3").removeClass("sprint_show");
    //     $(".sprint_day_4").removeClass("sprint_show");
    //     $(".sprint_day_5").removeClass("sprint_show");
    // });
    // $(document).on('click', '.daythree ', function(e) {
    //     e.preventDefault();
    //     $(".sprint_day_1").removeClass("sprint_show");
    //     $(".sprint_day_2").removeClass("sprint_show");
    //     $(".sprint_day_3").addClass("sprint_show");
    //     $(".sprint_day_4").removeClass("sprint_show");
    //     $(".sprint_day_5").removeClass("sprint_show");
    // });
    // $(document).on('click', '.dayfour ', function(e) {
    //     e.preventDefault();
    //     $(".sprint_day_1").removeClass("sprint_show");
    //     $(".sprint_day_2").removeClass("sprint_show");
    //     $(".sprint_day_3").removeClass("sprint_show");
    //     $(".sprint_day_4").addClass("sprint_show");
    //     $(".sprint_day_5").removeClass("sprint_show");


    // });
    // $(document).on('click', '.dayfive ', function(e) {
    //     e.preventDefault();
    //     $(".sprint_day_1").removeClass("sprint_show");
    //     $(".sprint_day_2").removeClass("sprint_show");
    //     $(".sprint_day_3").removeClass("sprint_show");
    //     $(".sprint_day_4").removeClass("sprint_show");
    //     $(".sprint_day_5").addClass("sprint_show");


    // });
</script>
