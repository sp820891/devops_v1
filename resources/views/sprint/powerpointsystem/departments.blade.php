<x-app-layout :assets="$assets ?? []">
    <div class="card">
        <div class="card-header d-flex justify-content-between">
           <div class="header-title">
              <h4 class="card-title">Departments</h4>
           </div>

           <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addDept">
            <i class="btn-inner">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                    viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                </svg>
            </i>
            <span>Create Department</span>
          </button>

        </div>
        <div class="card-body">

           <div class="table-responsive">
              <table id="departments_table" class="table table-hover">
                 <thead>
                    <tr>
                       <th>Id</th>
                       <th>Department</th>
                       <th>Role</th>
                       <th>created by</th>
                       <th>Action</th>
                    </tr>
                 </thead>
                 <tbody>
                 </tbody>
              </table>
           </div>
        </div>
     </div>
            {{--  model --}}
            <div class="modal fade" id="addDept" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header header-title">
                      <h5 class="modal-title card-title" id="exampleModalLabel">Add Department</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Create a Department with a Name and some Desription about it.</p>
                        <form>
                            <div class="form-group">
                                <label class="form-label" for="name">Name</label>
                                <input type="text" class="form-control" id="name">
                                <small class="text-danger" id="small_dept_name"></small>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Role </label>
                                <select class="form-select" data-placeholder="Choose a role..."
                                    name="role_as" id="role">
                                    <option selected value="">Choose a role</option>
                                    @forelse ($roles as $role)
                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                    @empty
                                        <option value="">No Roles Found</option>
                                    @endforelse
                                </select>
                                <small class="text-danger" id="role_small"></small>
                            </div>
                            <br>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-bs-dismiss="modal" id="close">cancel</button>
                                <button type="button" class="btn btn-primary" id="save-dept">Save</button>
                            </div>
                        </form>
                    </div>
                  </div>
                </div>
              </div>
            {{-- Delete MODEL start --}}
            <div class="modal fade" id="deleteDept" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                            style="margin-left: 29rem; margin-top: 11px;"></button>
                        <div class="modal-body" style=" margin-bottom: 10px;">
                            <input type="hidden" id="deleteDeptId">
                            <h5 class="text-center"> Are you sure want to delete department  <span id="delete_dept_name" class="text-danger text-center"></span>?</h5>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-danger" onclick="deleteDept(this)">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Delete MODEL END --}}
</x-app-layout>
<script src="{{asset("js/configuration/department.js")}}"></script>
<script>
    $(function () {
      var table = $('#departments_table').DataTable({
      processing: true,
      serverSide: true,
      autoWidth: false,
      ajax: "{{ url('departments') }}",
       columns: [{
              data: 'id',
              name: 'id'
              },
              {
              data: 'name',
              name: 'name',
              },

              {
              data: 'roles.name',
              name: 'roles.name',
              },
              {
              data: 'users.full_name',
              name: 'users.full_name',
              },
              {
              data: 'action',
              name: 'action',
              orderable: true,
              searchable: true
              },

              ]
              });
      });

</script>
