<x-app-layout :assets="$assets ?? []">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                         <h5 style="text-align: center;">Power Point System</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6" style="position: relative;top: -26px;padding-right:0;">
                <div class="card" style="height: 42rem;overflow: auto;">
                    <div class="card-body">
                        <h6>Departments</h6><br>
                        @forelse ($department as $dep)
                            <div class="col-md-6 pt-0 pb-0 " style="color: #232d42">

                                <input style="cursor:pointer;" class="dep_checked form-check-input" type="checkbox"
                                    value="{{ $dep->id }}" id="dep_check{{ $dep->id }}" name="dep_checked">
                                <label style="cursor:pointer;" class="form-check-label"
                                    for="dep_check{{ $dep->id }}">
                                    <p>{{ $dep->name }}</p>
                                </label>
                            </div>
                        @empty
                            <span style="font-size: xx-large;margin-left: 127px;position: relative;top: 245px;">No Records Found</span>
                        @endforelse
                    </div>
                </div>
            </div>

            <div class="col-md-6" style="position:relative;top:-26px;padding-right:6px;left:-9px;">
                    <div class="card" style="height: 42rem;overflow: auto;">
                    <div class="card-body">
                        <h6>Power Point Rules</h6><br>

                        @forelse ($pprules as $rule)
                            <div class="col-md-6 pt-0 pb-0 " style="color: #232d42">

                                <input style="cursor:pointer;" class="rule_assign form-check-input" type="checkbox"
                                    value="{{ $rule->id }}" id="rule_check{{ $rule->id }}" name="rule_check">
                                <label style="cursor:pointer;" class="form-check-label"
                                    for="rule_check{{ $rule->id }}">
                                    <p>{{ $rule->name }}</p>
                                </label>
                                <select class="pp_select" id="pp_select{{$rule->id}}" style="visibility: hidden;">
                                    <option value="" selected disabled>__</option>
                                    <option value="0.5" id="opt0point5">0.5</option>
                                    <option value="1" id="opt1">1</option>
                                    <option value="2" id="opt2">2</option>
                                    <option value="3" id="opt3">3</option>
                                </select>
                            </div>
                        @empty
                            <span style="font-size: xx-large;margin-left: 137px;position: relative;top: 245px;">No Records Found</span>
                        @endforelse
                        <button type="submit" style="position: absolute; top: 613px; right: 16px;" id="update_rule" class="btn btn-primary float-end">Update</button>
                    </div>
                </div>
                </div>
            </div>



        </div>
    </div>
</x-app-layout>
<script src="{{asset('js/sprint/pp_system.js')}}"></script>
