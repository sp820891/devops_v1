<x-app-layout :assets="$assets ?? []">
    {{-- For Manager Add Row & Columns To Power Points Table --}}
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="height: 42rem;overflow: auto;">
                    <div class="card-body">
                        <h4 class="card_title">Power Points Management Board</h4><br>
                        <div class="row">
                            <div class="col-md-6">
                                {{-- <div class="d-flex justify-content-between"> --}}
                                    <label for="">Sprint No</label>
                                    <select class="form-select">
                                        <option value="">Sprint 1</option>
                                        <option value="">Sprint 2</option>
                                        <option value="">Sprint 3</option>
                                        <option value="">Sprint 4</option>
                                        <option value="">Sprint 5</option>
                                    </select>
                                {{-- </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
