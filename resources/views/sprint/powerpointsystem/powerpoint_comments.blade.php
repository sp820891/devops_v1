<x-app-layout :assets="$assets ?? []">
    <div class="card">
        <div class="card-header d-flex justify-content-between">
           <div class="header-title">
              <h5 class="card-title">Power Point Comment</h5>
           </div>

           <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addComment">
            <i class="btn-inner">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                    viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                </svg>
            </i>
            <span>Create Comment</span>
          </button>

        </div>
        <div class="card-body">

           <div class="table-responsive">
              <table id="comments_table" class="table table-hover">
                 <thead>
                    <tr>
                       <th>Id</th>
                       <th>Comment Name</th>
                       <th>Created by</th>
                       <th>Action</th>
                    </tr>
                 </thead>
                 <tbody>
                 </tbody>
              </table>
           </div>
        </div>
     </div>
            {{--  model --}}
            <div class="modal fade" id="addComment" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header header-title">
                      <h5 class="modal-title card-title" id="exampleModalLabel">Add Power Point Comment</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="comment_close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Create a  with a name and some Desription about it.</p>
                        <form id="powerpoint_comment">
                            <div class="form-group">
                                <label class="form-label" for="name">Name</label>
                                <input type="Text" class="form-control" id="name" name="PowerpointComment">
                                <small class="text-danger" id="small_comment_name"></small>
                            </div>
                            <br>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-bs-dismiss="modal" id="close_comment">cancel</button>
                                <button type="button" class="btn btn-primary" id="save_comment">Save</button>
                            </div>
                        </form>
                    </div>
                  </div>
                </div>
              </div>
            {{-- Delete MODEL start --}}
            <div class="modal fade" id="deleteComment" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                            style="margin-left: 29rem; margin-top: 11px;"></button>
                        <div class="modal-body" style=" margin-bottom: 10px;">
                            <input type="hidden" id="deleteCommentId">
                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="100" height="100" viewBox="0,0,256,256"
                            style="fill:#000000;margin-left: 11.5rem;">
                            <g fill="#fcc419" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><g transform="scale(16,16)"><path d="M7.5,1c-3.58203,0 -6.5,2.91797 -6.5,6.5c0,3.58203 2.91797,6.5 6.5,6.5c3.58203,0 6.5,-2.91797 6.5,-6.5c0,-3.58203 -2.91797,-6.5 -6.5,-6.5zM7.5,2c3.04297,0 5.5,2.45703 5.5,5.5c0,3.04297 -2.45703,5.5 -5.5,5.5c-3.04297,0 -5.5,-2.45703 -5.5,-5.5c0,-3.04297 2.45703,-5.5 5.5,-5.5zM7,4v1h1v-1zM7,6v5h1v-5z"></path></g></g>
                            </svg>
                            <h4 class="text-center"> Are you sure ?<br><small id="delete_comment" class="text-danger text-center"></small></h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" onclick="deleteComment(this)">Yes, delete it!</button>
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancel</button>

                        </div>
                    </div>
                </div>
            </div>
            {{-- Delete MODEL END --}}
</x-app-layout>
 <script src="{{asset("js/sprint/power_point_comment.js")}}"></script>
<script>
    $(function () {
      var table = $('#comments_table').DataTable({
      processing: true,
      serverSide: true,
      autoWidth: false,
      ajax: "{{ url('power_point_comments') }}",
       columns: [{
              data: 'id',
              name: 'id'
              },
              {
              data: 'power_point_comments',
              name: 'power_point_comments',
              },
              {
              data: 'users.full_name',
              name: 'users.full_name',
              },
              {
              data: 'action',
              name: 'action',
              orderable: true,
              searchable: true
              },
              ]
              });
      });
</script>
