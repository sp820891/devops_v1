<x-app-layout :assets="$assets ?? []">
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <div class="header-title">
                <h5 class="card-title"> <span>
                        <svg width="30" height="30" viewBox="0 0 25 24" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M7.5 4.43245C7.5 4.27684 7.5 4.19903 7.4491 4.15423C7.39819 4.10944 7.32244 4.11915 7.17094 4.13857C6.04965 4.28229 5.26806 4.57508 4.67157 5.17157C3.5 6.34315 3.5 8.22876 3.5 12C3.5 15.7712 3.5 17.6569 4.67157 18.8284C5.84315 20 7.72876 20 11.5 20H13.5C17.2712 20 19.1569 20 20.3284 18.8284C21.5 17.6569 21.5 15.7712 21.5 12C21.5 8.22876 21.5 6.34315 20.3284 5.17157C19.7319 4.57508 18.9504 4.28229 17.8291 4.13857C17.6776 4.11915 17.6018 4.10944 17.5509 4.15424C17.5 4.19903 17.5 4.27684 17.5 4.43245L17.5 6.5C17.5 7.32843 16.8284 8 16 8C15.1716 8 14.5 7.32843 14.5 6.5L14.5 4.30005C14.5 4.15898 14.5 4.08844 14.4561 4.04451C14.4123 4.00059 14.3418 4.0005 14.2009 4.00031C13.9748 4 13.7412 4 13.5 4H11.5C11.2588 4 11.0252 4 10.7991 4.00031C10.6582 4.0005 10.5877 4.00059 10.5439 4.04452C10.5 4.08844 10.5 4.15898 10.5 4.30005L10.5 6.5C10.5 7.32843 9.82843 8 9.00001 8C8.17158 8 7.5 7.32843 7.5 6.5L7.5 4.43245Z"
                                fill="currentColor" fill-opacity="0.25" />
                            <path d="M9 2.5L9 6.5" stroke="currentColor" stroke-linecap="round" />
                            <path d="M16 2.5L16 6.5" stroke="currentColor" stroke-linecap="round" />
                            <circle cx="8" cy="10.5" r="0.5" fill="currentColor" />
                            <circle cx="11" cy="10.5" r="0.5" fill="currentColor" />
                            <circle cx="14" cy="10.5" r="0.5" fill="currentColor" />
                            <circle cx="17" cy="10.5" r="0.5" fill="currentColor" />
                            <circle cx="8" cy="13.5" r="0.5" fill="currentColor" />
                            <circle cx="11" cy="13.5" r="0.5" fill="currentColor" />
                            <circle cx="14" cy="13.5" r="0.5" fill="currentColor" />
                            <circle cx="17" cy="13.5" r="0.5" fill="currentColor" />
                            <circle cx="8" cy="16.5" r="0.5" fill="currentColor" />
                            <circle cx="11" cy="16.5" r="0.5" fill="currentColor" />
                            <circle cx="14" cy="16.5" r="0.5" fill="currentColor" />
                            <circle cx="17" cy="16.5" r="0.5" fill="currentColor" />
                        </svg></span>New Sprint </h5>

            </div>

        </div>
        <div class="card-body">
            {{-- <select name="" id="filter" class="filter">
                @foreach ($filter as $fill)
                    <option value="{{ $fill->sprint_id }}">{{ $fill->sprint->id }}</option>
                @endforeach

            </select> --}}


            <div class="table-responsive">
                <table id="sprint_plan" class="table table-hover">
                    <thead>
                        <tr>
                            <th>S.no</th>
                            <th>Sprint</th>
                            <th>project</th>
                            <th>Sprint owner</th>
                        </tr>
                    </thead>
                    <tbody>
                </table>
            </div>
        </div>
    </div>

</x-app-layout>
<script>
    $(function() {
        var i = 1;
        var table = $('#sprint_plan').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ajax: "{{ url('sprint_planning') }}",
            // dom: 'Bfrtip',
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            columns: [{
                    render: function(data, type, full, meta) {
                        return i++;
                    }
                },

                {
                    data: 'Sprint',
                    name: 'Sprint',

                },

                {
                    data: 'project',
                    name: 'project',

                },
                {
                    data: 'full_name',
                    name: 'full_name',
                    render: function(data, type, row) {
                        if (row.created_by === null) {
                            return 'NO RECORDS FOUND';
                        } else {
                            return data;
                        }
                    }
                },
            ]
        });
        $("#filter").on("change", function() {
            var value = $(this).val().toLowerCase();
            alert(value);
            $("#sprint_plan > tbody > tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });




        $('#sprint_plan').on('dblclick', 'tr', function() {
            window.location = `sprint-of-project/${this.id}`
        });
    });



    //     $(function() {

    // });

    //     $('#filter').on('change', function() {


    //     filter= $(this).val();




    //         console.log(filter);

    //         $.ajax({
    //             headers: {
    //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //             },
    //             url: 'sprintfilter',
    //             type: "get",
    //             dataType: "json",
    //             data:filter,
    //             success: function(response) {
    //                 $('#sprint_plan').DataTable().ajax.reload();
    //             }
    //         })
    //     });


    // #myInput is a <input type="text"> element
</script>
