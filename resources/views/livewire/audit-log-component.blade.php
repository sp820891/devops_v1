<div>

<div class="col-lg-12">
    <div class="card">

        <div class="card-body" style="height:30em; overflow-y: auto; ">
            <h5>Audit Logs<h5><br>
                    <div class="table-responsive">
                        <table class="table student-data-table " style="font-size:13px;">
                            <thead>
                                <tr>
                                    <th>UserName</th>
                                    <th>Event</th>
                                    <th>auditable_type</th>
                                    <th>Created_at</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($audit_log as $logs)
                                    <tr>
                                        <td>
                                            {{  $logs->user->full_name }}
                                        </td>
                                        <td>
                                            @if( $logs->event=='updated')
                                           <b> {{ $logs->event}}</b> from
                                            <b>{{ str_replace(['{', '"', '}'], ' ',         $logs->old_values) }} </b> <br>to
                                                <b>{{ str_replace(['{', '"', '}'], ' ', $logs->new_values) }}</b>
                                                @else
                                                {{ $logs->event}}
                                            @endif
                                          </td>
                                        <td>{{ substr($logs->auditable_type,11)}} ID
                                            -{{ $logs->auditable_id }}</td>
                                        <td>{{ $logs->created_at }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="link" style="    position: absolute;top: 434px;">
                            {{ $audit_log->links() }}
                        </div>
                    </div>
        </div>
    </div>
</div>
</div>
