<x-app-layout :assets="$assets ?? []">
    <div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="header-title">
                            <h4 class="card-title">User Tickets</h4>
                        </div>
                        <div class="card-action">
                            <div class="text-center ms-3 ms-lg-0 ms-md-0">
                                @can('can add users')
                                    <button type="button" class="btn  btn-primary" data-bs-toggle="modal"
                                        data-bs-target="#ticketModel">
                                        <i class="btn-inner">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                                viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                    d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                                            </svg>
                                        </i>
                                        <span>new issue</span>
                                    </button>
                                @endcan
                            </div>
                        </div>

                    </div>


                    <div class="card-body ">
                        <div class="table-responsive" style="overflow: hidden">
                            <table class="table table-striped table-hover " id="ticket-table">
                                <thead>
                                    <tr>
                                        <th> Id</th>
                                        <th>Status</th>
                                        <th>sprint_goal</th>
                                        <th>project</th>
                                        <th>assginee</th>
                                        <th>created_by</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Ticket MODEl START --}}
    <div class="modal fade" id="ticketModel" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">New issue</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                        id="close"></button>
                </div>
                <div class="modal-body">
                    <form id="ticketForm">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="form-label">Subject<span class="text-danger">*</span> </label>
                                <input class="form-control text-secondary text-secondary" placeholder="Enter the subject"
                                    type="text" name="subject" id="subject" />
                                <small class="text-danger" id="subject_small"></small>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label">Assignee</label>
                                <select class="form-select mb-3 shadow-none text-secondary"
                                    data-placeholder="Choose a role..." name="assignee" id="assignee">
                                    <option selected value="">Assignee</option>
                                    @foreach ($assignee as $user)
                                        <option value="{{ $user->id }}">
                                            {{ $user->full_name }}</option>
                                    @endforeach
                                </select>

                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label">project</label>
                                <select class="form-select mb-3 shadow-none text-secondary"
                                    data-placeholder="Choose a role..." name="project" id="project">
                                    <option selected value="">Project</option>
                                    @foreach ($project as $projects)
                                        <option value="{{ $projects->id }}">
                                            {{ $projects->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label">Sprint</label>
                                <select class="form-select mb-3 shadow-none text-secondary"
                                    data-placeholder="Choose a role..." name="sprint" id="sprint">
                                    <option selected value="">sprint</option>
                                    @foreach ($sprint as $sprints)
                                        <option value="{{ $sprints->id }}">{{ $sprints->name }}</option>
                                    @endforeach
                                </select>
                                <small class="text-danger" id="sprint_small"></small>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label">Status</label>
                                <select class="form-select mb-3 shadow-none text-secondary"
                                    data-placeholder="Choose a role..." name="status" id="status">
                                    <option selected value="">Select status</option>
                                    <option value="Backlog">Backlog</option>
                                    <option value="progress">In progress</option>
                                    <option value="Done">Done</option>
                                </select>
                                    <small class="text-danger" id="status_small"></small>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-bs-dismiss="modal"
                        class="btn btn-danger"id="cancel">Close</button>
                    <button type="button" class="btn btn-primary" id="saveticket">Save </button>
                </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Ticket MODEl END --}}

    {{-- edit User Start --}}

    {{-- edit User end --}}
    <div class="modal fade" id="DeleteUser" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                    style="margin-left: 29rem; margin-top: 11px;"></button>
                <div class="modal-body" style=" margin-bottom: 10px;">
                    <input type="hidden" id="deleteUser">
                    <h5 class="text-center"> Are you sure want to delete User <br> <span id="user_name" class="text-danger text-center"></span>? </h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" id="delete_btn">Delete</button>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<script src="{{ asset('js/ticket.js') }}"></script>
<script type="text/javascript">
    $(function() {
        var table = $('#ticket-table').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ajax: "{{ url('tickets') }}",
            dom: 'Bfrtip',
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            columns: [
                {
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'status',
                    name: 'status'
                },
                {
                    data: 'Sprint_goal',
                    name: 'Sprint_goal',
                },
                {
                    data: 'projects.name',
                    name: 'projects.name',
                },
                {
                    data: 'users.full_name',
                    name: 'users.full_name'
                },
                {
                    data: 'creator.full_name',
                    name: 'creator.full_name'
                },
                // {
                //     data: 'action',
                //     name: 'action',
                //     orderable: true,
                //     searchable: true,
                //     // render: function(data, type, row) {
                //     //     if (row.action === '') {
                //     //         return table.column(5).visible(false);
                //     //     } else {
                //     //         return data;
                //     //     }
                //     // },
                // }
            ]
        });
        $('#ticket-table').on('dblclick', 'tr', function() {

            console.log(this.id);

            window.location = `spend-time/${this.id}`

        });
    });
</script>
