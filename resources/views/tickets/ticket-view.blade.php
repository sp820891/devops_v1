<x-app-layout :assets="$assets ?? []">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <div class="header-title  d-flex justify-content-between">
                        @foreach ($ticket as $tickets)


                        <h4 class="card-title">
                            <label>Issue_no:</label>{{ $tickets->id }} <br>
                            <label>Project:</label> {{ $tickets->projects->name }}
                        </h4>
                        <button type="button" class="btn  btn-primary " id="submit-ticket">
                                        <i class="btn-inner">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                                viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                    d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                                            </svg>
                                        </i>
                                        <span>update</span>
                                    </button>
                        @endforeach
                    </div>
                    <div class="card-action">
                        @foreach ($ticket as $tickets)
                        <h3><span>Subject:</span>{{ $tickets->subject }}</h3>
                        <hr style="min-width:85%; background-color:#a1a1a1 !important; height:1px;"/>
                        <div class=" ms-3 ms-lg-0 ms-md-0" >
                            <div class="row">
                               <form id="update-ticket" class=" d-flex">
                                <input type="hidden" id="id" value="{{ $tickets->id }}">
                                <ul class="list-group col-8" >
                                    <li class="list-group-item form-control" ><label style="padding: 10px">Status: </label><input type="text" value="{{ $tickets->status }}" style="border: none" id="status"></li>
                                    <li class="list-group-item form-control" ><label style="padding: 10px">Estimate_time: </label><input type="text " value="@isset($spend_time){{ $tickets->estimate_time }}@endisset" style="border: none" id="estimate_time"></li>
                                    <li class="list-group-item form-control" ><label style="padding: 10px">Spend_time: </label><input type="text" value="@isset($spend_time){{ $spend_time->spend_time }}@endisset" style="border: none" id="spend_time"></li>
                                    <li class="list-group-item form-control" ><label style="padding: 10px">Assignee: </label><input type="text" value="{{ $tickets->users->full_name }}" style="border: none" id="assignee"></li>
                                </ul>
                                <ul class="list-group col-4">
                                    <li class="list-group-item form-control" id="sprint"><label style="padding: 10px">Sprint: </label>{{ $tickets->sprint_id }}</li>
                                    <li class="list-group-item form-control" ><label style="padding: 10px">Start_date: </label><input type="date" value="{{ $tickets->start_date }}" id="start_date"></li>
                                    <li class="list-group-item form-control" ><label style="padding: 10px">End_date: </label><input type="date" value="{{ $tickets->end_date }}" id="end_date"></li>
                                    <li class="list-group-item form-control" ><label style="padding: 10px">Done: </label><input type="range" value="@isset($spend_time){{ $spend_time->done }}@endisset" id="done"></li>
                                </ul>
                               </form>
                            </div>
                        </div>
                        <hr style="min-width:85%; background-color:#a1a1a1 !important; height:1px;"/>
                        @endforeach
                    </div>
                </div>
                <div class="card-body ">
                    <div class="table-responsive" style="overflow: hidden">
                        {{-- <table class="table table-striped table-hover " id="ticket-table">
                            <thead>
                                <tr>
                                    <th> Id</th>
                                    <th>Status</th>
                                    <th>sprint_goal</th>
                                    <th>project</th>
                                    <th>assginee</th>
                                    <th>created_by</th>
                                </tr>
                            </thead>
                        </table> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        </div>
    </div>
</x-app-layout>
<script src="{{ asset('js/ticket.js') }}"></script>
