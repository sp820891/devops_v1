<!-- Backend Bundle JavaScript -->
<script src="{{ asset('js/libs.min.js')}}"></script>
@if(in_array('data-table',$assets ?? []))
<script src="{{ asset('vendor/datatables/buttons.server-side.js')}}"></script>
@endif
@if(in_array('chart',$assets ?? []))
    <!-- apexchart JavaScript -->
    <script src="{{asset('js/charts/apexcharts.js') }}"></script>
    <!-- widgetchart JavaScript -->
    <script src="{{asset('js/charts/widgetcharts.js') }}"></script>
    <script src="{{asset('js/charts/dashboard.js') }}"></script>
@endif

<!-- mapchart JavaScript -->
<script src="{{asset('vendor/Leaflet/leaflet.js') }} "></script>
<script src="{{asset('js/charts/vectore-chart.js') }}"></script>


<!-- fslightbox JavaScript -->
<script src="{{asset('js/plugins/fslightbox.js')}}"></script>
<script src="{{asset('js/plugins/slider-tabs.js') }}"></script>
<script src="{{asset('js/plugins/form-wizard.js')}}"></script>

<!-- settings JavaScript -->
<script src="{{asset('js/plugins/setting.js')}}"></script>

<script src="{{asset('js/plugins/circle-progress.js') }}"></script>
@if(in_array('animation',$assets ?? []))
<!--aos javascript-->
<script src="{{asset('vendor/aos/dist/aos.js')}}"></script>
@endif

@if(in_array('calender',$assets ?? []))
<!-- Fullcalender Javascript -->
{{-- {{-- <script src="{{asset('vendor/fullcalendar/core/main.js')}}"></script>
<script src="{{asset('vendor/fullcalendar/daygrid/main.js')}}"></script>
<script src="{{asset('vendor/fullcalendar/timegrid/main.js')}}"></script>
<script src="{{asset('vendor/fullcalendar/list/main.js')}}"></script>
<script src="{{asset('vendor/fullcalendar/interaction/main.js')}}"></script> --}}
<script src="{{asset('vendor/moment.min.js')}}"></script>
<script src="{{asset('js/plugins/calender.js')}}"></script>
@endif

<script src="{{ asset('vendor/flatpickr/dist/flatpickr.min.js') }}"></script>
<script src="{{ asset('js/plugins/flatpickr.js') }}" defer></script>
{{-- <script src="{{asset('vendor/vanillajs-datepicker/dist/js/datepicker-full.js')}}"></script> --}}

@stack('scripts')

<script src="{{asset('js/plugins/prism.mini.js')}}"></script>

<!-- Custom JavaScript -->
<script src="{{asset('js/hope-ui.js') }}"></script>
@if (auth()->user())
    <script src="{{asset('js/custom.js')}}"></script>
@endif

<!-- Toastr -->
<script src="{{asset("js/toastr.min.js")}}"></script>
{{-- datatable --}}
<script src="https://cdn.datatables.net/buttons/2.3.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.3.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.3.6/js/buttons.print.min.js"></script>
{{-- Pusher Notification --}}
<script src="https://js.pusher.com/8.2.0/pusher.min.js"></script>
<script>

// Enable pusher logging - don't include this in production
Pusher.logToConsole = true;

var pusher = new Pusher('5924496c954ed4797127', {
    cluster: 'ap2'
});

var channel = pusher.subscribe('notification-channel');
channel.bind('notification-broadcast', function(data) {
   $.ajax({
        type: "get",
        url: "/application_notification",
        data: data,
        dataType: "json",
        success: function(response) {
            if(response.notifications_count == 0) {
                var html = `<h6 class="mt-2 mb-2 text-center" style="color:#6c757d;">No Records Found</h6>`;
                $("#application_notification").html(html);
            }
            else {
                $("#application_notification").html("");
            }
            var now = new Date();
            $.each(response.notifications , function (i, val) {
                var created_at = new Date(val.created_at);
                var diff_time = created_at.getTime() -  now.getTime();
                var diff_hours = Math.abs(created_at - now) / 36e5;
                var yesterday = new Date();
                yesterday.setDate(now.getDate() - 1);
                var hours = Math.trunc(diff_hours);
                var seconds = Math.abs(diff_time) / 1000;
                var minutes = Math.trunc(Math.abs(diff_time / 60000));
                if (seconds <= 60 && created_at.getDate() == now.getDate() && created_at.getMonth() == now.getMonth() && created_at.getFullYear() == now.getFullYear()) {
                    var dateTime = "Just Now";
                }
                else if (seconds > 60 && minutes == 1 && created_at.getDate() == now.getDate() && created_at.getMonth() == now.getMonth() && created_at.getFullYear() == now.getFullYear()) {
                    var dateTime = minutes+" Min Ago";
                }
                else if (minutes > 1 && minutes <= 60 && created_at.getDate() == now.getDate() && created_at.getMonth() == now.getMonth() && created_at.getFullYear() == now.getFullYear()) {
                    var dateTime = minutes+" Mins Ago";
                }
                else if(hours == 1 && created_at.getDate() == now.getDate() && created_at.getMonth() == now.getMonth() && created_at.getFullYear() == now.getFullYear()) {
                    var dateTime = hours+" Hr Ago";
                }
                else if(hours > 1 && hours <= 12 && created_at.getDate() == now.getDate() && created_at.getMonth() == now.getMonth() && created_at.getFullYear() == now.getFullYear()) {
                    var dateTime = hours+" Hrs Ago";
                }
                else if(hours > 12 && created_at.getDate() == now.getDate() && created_at.getMonth() == now.getMonth() && created_at.getFullYear() == now.getFullYear()) {
                    var dateTime = "Today";
                }
                else if (created_at.getDate() != now.getDate() && yesterday.getDate() == created_at.getDate() && created_at.getMonth() == yesterday.getMonth() && created_at.getFullYear() == yesterday.getFullYear()) {
                    var dateTime = "Yesterday";
                }
                else {
                    if(created_at.getDate() < 10) {
                        var date_format = "0"+created_at.getDate();
                    }
                    else {
                        var date_format = created_at.getDate();
                    }
                    if(created_at.getMonth() < 10) {
                        var month_format = "0"+(created_at.getMonth()+1);
                    }
                    else {
                        var month_format = (created_at.getMonth()+1);
                    }
                    var dateTime = date_format+"/"+month_format+"/"+created_at.getFullYear();
                }
                let notifications = `<a onclick="application_notification_markasread('${val.id}','${response.url}')" href="${response.url}/${val.data["url_name"]}/${val.data["notify_id"]}" class="iq-sub-card">
                                <div class="d-flex align-items-center">
                                <img class="avatar-40 rounded-pill bg-soft-primary p-1" src="${response.url}/images/shapes/01.png" alt="img">
                                <div class="ms-3 w-100">
                                    <h6 class="mb-0 ">${val.data["message"]}</h6>
                                    <div class="d-flex justify-content-between align-items-center">
                                    <p class="mb-0">${val.data["created_by"]}</p>
                                    <small class="float-right font-size-12">${dateTime}</small>
                                    </div>
                                </div>
                                </div>
                            </a>`;
                $("#application_notification").append(notifications);
            });
            if (response.notifications_count > 4) {
                $('#notification_read_more').css("display", "block");
            }
            if (response.notifications_count > 100) {
                var notifications_count = `100+`;
                $('#notifications_count').html(notifications_count);
            }
            else {
                $('#notifications_count').html(response.notifications_count);
            }
        }
    });
});
</script>
