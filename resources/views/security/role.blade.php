<x-app-layout :assets="$assets ?? []">
    <div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header ">
                        <div class="header-title d-flex justify-content-between">
                            <h5 class="card-title mb-0">
                                <span>
                                     <svg width="29" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.4"
                                            d="M12.0865 22C11.9627 22 11.8388 21.9716 11.7271 21.9137L8.12599 20.0496C7.10415 19.5201 6.30481 18.9259 5.68063 18.2336C4.31449 16.7195 3.5544 14.776 3.54232 12.7599L3.50004 6.12426C3.495 5.35842 3.98931 4.67103 4.72826 4.41215L11.3405 2.10679C11.7331 1.96656 12.1711 1.9646 12.5707 2.09992L19.2081 4.32684C19.9511 4.57493 20.4535 5.25742 20.4575 6.02228L20.4998 12.6628C20.5129 14.676 19.779 16.6274 18.434 18.1581C17.8168 18.8602 17.0245 19.4632 16.0128 20.0025L12.4439 21.9088C12.3331 21.9686 12.2103 21.999 12.0865 22Z"
                                            fill="currentColor"></path>
                                        <path
                                            d="M11.3194 14.3209C11.1261 14.3219 10.9328 14.2523 10.7838 14.1091L8.86695 12.2656C8.57097 11.9793 8.56795 11.5145 8.86091 11.2262C9.15387 10.9369 9.63207 10.934 9.92906 11.2193L11.3083 12.5451L14.6758 9.22479C14.9698 8.93552 15.448 8.93258 15.744 9.21793C16.041 9.50426 16.044 9.97004 15.751 10.2574L11.8519 14.1022C11.7049 14.2474 11.5127 14.3199 11.3194 14.3209Z"
                                            fill="currentColor"></path>
                                    </svg>
                                </span>Roles & Permissions</h5>

                            @can('can add roles')
                                <button type="button" class="btn  btn-primary btn-md" data-bs-toggle="modal"
                                    data-bs-target="#RoleModel">
                                    <i class="btn-inner">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                            viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                                        </svg>
                                    </i>
                                    <span>New Role</span>
                                </button>
                            @endcan
                        </div>



                    </div>
                    <div class="card-body">
                        <div class="card-body p-0">
                            <div class="table-responsive" style="overflow: hidden">
                                <table class="table table-hover mb-0" id="roles-table">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>RoleName</th>
                                            <th>GuardName</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- ADD ROLE MODEL START --}}
        <div class="modal fade" id="RoleModel" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5 text-secondary" id="exampleModalLabel">Add Role</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                            id="close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="roleForm">

                            <div class="form-group">
                                <label class="form-label">Role Title</label>
                                <input class="form-control text-secondary text-secondary" placeholder="Role Title"
                                    type="text" name="name" id="name" />
                                <small class="text-danger" id="role_small"></small>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-md"
                                    data-bs-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary btn-md" id="roleSave">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {{-- ADD ROLE MODEL END --}}


        <div class="modal fade" id="DeleteRole" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">


                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                        style="margin-left: 29rem; margin-top: 11px;"></button>

                    <div class="modal-body" style=" margin-bottom: 10px;">
                        <input type="hidden" id="deleteRole">
                        <h5 class="text-center"> Are you sure want to delete Role <br><span id="role_name"
                                class="text-danger"></span>?</h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary btn-md" data-bs-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-danger btn-md" id="delete_role">Delete</button>
                    </div>
                </div>
            </div>
        </div>
</x-app-layout>
<script src="{{ asset('js/security/role.js') }}"></script>
<script type="text/javascript">
    $(function() {

        var table = $('#roles-table').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            // dom: 'Bfrtip',
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: "{{ route('roles-permissions.index') }}",
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'guard_name',
                    name: 'guard_name'
                },

                {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true,
                    render: function(data, type, row) {
                        if (row.action === '') {
                            return table.column(3).visible(false);
                        } else {
                            return data;
                        }
                    },

                },
            ]
        });
        $('#roles-table').on('dblclick', 'tr', function() {
            if(this.id != 0) {
                window.location = `role-permissions/${this.id}`
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("No Role is Created To View Permission");
            }
        });
    });
</script>
