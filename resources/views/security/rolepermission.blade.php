@can('can access role-based-permissions')
<x-app-layout :assets="$assets ?? []">
    <div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                    <h5 class="card-title mb-0"> <span>
                        <img width="45" height="45" src="https://img.icons8.com/external-others-pike-picture/50/external-Permission-pet-travel-others-pike-picture-2.png"
                        alt="external-Permission-pet-travel-others-pike-picture-2"/></span>Role Based Permissions ({{$role->name}})</h5>
                    <a href="{{ url('roles-permissions') }}">
                        <img src="data:image/png;base64,
                        iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAoUlEQVR4nN3UsQpBcRTH8c+AFzAY
                        TAaDwWRUZl0PYVQ2u8UbsPECZh7AinKT3epBdOsqlOFef5Fv/frXGb7/TqdzeKSHuUB0sUX5v2UJZ1wQZ8wBM9Q80cApfbNQQJR2Nwwl
                        TShh+Qnp7lX7E/mIMBWQYjqsoMQ/LaxgE1I4xiCUrIl9Opi36eCI+q3QypE2+lhjher9D4scSY7CKOdWfZkr7qstZzIrX1EAAAAASUVOR
                        K5CYII=">

                    </a>


                    </div>
                    <div class="card-body">
                        <div class="stat-widget-one">
                            <div class="stat-content dib">
                                <input type="hidden" id="role_name" value="{{ $role->name }}">
                                <div class="row">
                                    <div class="col-md-4 pt-0 pb-0"  style="color: #232d42">
                                        <input style="cursor:pointer;" class="form-check-input" type="checkbox"
                                            id="permission_check_all">
                                        <label style="cursor:pointer;" class="form-check-label"
                                            for="permission_check_all">
                                            <p>can access all permissions</p>
                                        </label>
                                    </div>
                                    @foreach ($Permissions as $permission)

                                        <div class="col-md-4 pt-0 pb-0 " style="color: #232d42">
                                            <input style="cursor:pointer;" class="form-check-input" type="checkbox"
                                                value="{{ $permission->name }}"
                                                id="permission_check{{ $permission->id }}" name="permission_check">
                                            <label style="cursor:pointer;" class="form-check-label"
                                                for="permission_check{{ $permission->id }}">
                                                <p>{{ $permission->name }}</p>
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button onclick="update_permission('{{ $role->name }}')" id="update_btn"
                            class="btn btn-primary btn-md permission float-end">Update</button>
                    </div>
                </div>
            </div>
        </div>
</x-app-layout>

@endcan
<script src="{{ asset('js/security/rolepermission.js') }}"></script>
