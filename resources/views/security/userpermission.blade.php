
<x-app-layout :assets="$assets ?? []">
 <style>
    .pt-6 {
        padding-top: 5rem !important;
    }
 </style>
    <div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h5 class="card-title mb-0"> <span>
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAAsTAAALEw
                            EAmpwYAAABkElEQVR4nO2Wyy4EQRSGPyazkrgtrPEAZrwFG8Zt410s3B5ACJkJNgwyDyAssRZW7rGxkCAiLEQiSMmZ5GRSXdXT0
                            x0W/Sf/pk+dOn/VuVRDihRudADzwCXwAdwDW0Cfw2cV+AS+HXwDhj2x6QVuAzYwYiYD/F48wassuYJngFO1+AnYk5vQImw3YU62D
                            VQcXAO6XQJGVaADoF2+NwFTylYmISypILkamxFxJTZTE4lgQwlotdj3xfZusY2ESME60OMSMKsEjNfYOoFnsZ0lVYQ54EsWPoqINq
                            AfOFKbTFt8SyHa8BUYcgkwWPZsch2QntiQBVbUTWge+3IYJ3JSE5vAAlAAmh3ru6SNJxwclFmTCC5CFuFMMuHhPKQAWwH/Ii8PUNn
                            Ty4aL0veZOlMwYEtBFigGFJ2PJ3EUZTFCYM0bmRWRkFcnfwDGgBaPT0b8DuMorDm1iQle709LdTSbAoyEshLgO7kNu+ofIRIqSsBf
                            +POvBFQi8K5RATsNtmCV5hmOhIK80Y0GN7MkRQrqwQ+XIgiFdDVlvQAAAABJRU5ErkJggg=="></span>User Details</h5>
                    <a href="{{ url('users') }}">
                        <img src="data:image/png;base64,
                        iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAoUlEQVR4nN3UsQpBcRTH8c+AFzAY
                        TAaDwWRUZl0PYVQ2u8UbsPECZh7AinKT3epBdOsqlOFef5Fv/frXGb7/TqdzeKSHuUB0sUX5v2UJZ1wQZ8wBM9Q80cApfbNQQJR2Nwwl
                        TShh+Qnp7lX7E/mIMBWQYjqsoMQ/LaxgE1I4xiCUrIl9Opi36eCI+q3QypE2+lhjher9D4scSY7CKOdWfZkr7qstZzIrX1EAAAAASUVOR
                        K5CYII=">
                    </a>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 pl-0"><br>
                            @if ($user->profile_picture == NULL)
                                <img src="{{asset('images/avatars/01.png')}}" alt="profile-img" class="rounded-pill avatar-155 img-fluid">
                            @else
                                <img src="{{asset($user->profile_picture)}}" id="new-pic3" alt="profile_picture" class="rounded-pill avatar-155 img-fluid">
                            @endif
                            </div>
                            <div class="col-md-6 pt-6">
                                <label class="control-label">Full Name</label>
                                <h6>{{ $user->full_name }}</h6>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label">Email</label>
                                <h6>{{ $user->email }}</h6>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label">Gender</label>
                                <h6>{{ $user->gender }}</h6>
                            </div>
                            <div class="col-md-6"><br>
                                <div class="col-md-6">
                                    <label class="control-label">Role</label>
                                    <h6>{{ $user->roles->pluck('name')->first() }}</h6><br>
                                </div>
                                <label class="control-label">Role Based Permissions</label>
                                <div class="stat-widget-one">
                                    <div class="stat-content dib">
                                        <div class="row">
                                            <div class="col-md-12 p-0 text-dark" style="margin-left: 24px;">
                                                @forelse ($Permissions as $permission)
                                                <svg width="13" height="13" viewBox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <rect x="0.5" y="0.5" width="12" height="12" fill="white" stroke="black"/>
                                                    </svg>
                                                    {{ $permission->name }} <br>
                                                @empty
                                                    <div class="ps-4">
                                                        No Permissions is Assigned For This Role.
                                                    </div>
                                                @endforelse
                                            </div>
                                        </div>
                                    </div>
                                </div><br>
                                <input type="hidden" id="user_id" value="{{ $user->id }}">
                                <label class="control-label">User Based Permissions</label>
                                <div class="stat-widget-one">
                                    <div class="stat-content dib">
                                        <div class="row ps-4" id="userBasedPermissions"></div>
                                    </div>
                                </div>
                            </div>
                            @can('can access user-based-permissions')
                            <div class="col-md-6 pl-0"><br>
                                <label class="control-label">All Permissions</label>
                                <div class="stat-widget-one">
                                    <div class="stat-content dib">
                                        <div class="row">
                                            @foreach ($allPermissions as $permission)
                                                <div class="col-md-6 pt-0 pb-0">
                                                    <input style="cursor:pointer;" class="form-check-input"
                                                        type="checkbox" value="{{ $permission->name }}"
                                                        id="permission_check{{ $permission->id }}"
                                                        name="permission_check">
                                                    <label style="cursor:pointer;" class="form-check-label text-dark"
                                                        for="permission_check{{ $permission->id }}">
                                                        <p>{{ $permission->name }}</p>
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="button" onclick="update_permission('{{ $user->id }}')" id="update_btn"
                            class="btn btn-primary btn-md float-end">Update</button>
                    </div>
                    @endcan
                </div>
            </div>
        </div>
    </div>

</x-app-layout>


<script src="{{ asset('js/security/userpermission.js') }}"></script>
