<x-app-layout :assets="$assets ?? []">
    <div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div class="header-title  d-flex justify-content-between">
                            <h5 class="card-title">
                                <span>
                                    <svg width="25" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M11.9488 14.54C8.49884 14.54 5.58789 15.1038 5.58789 17.2795C5.58789 19.4562 8.51765 20.0001 11.9488 20.0001C15.3988 20.0001 18.3098 19.4364 18.3098 17.2606C18.3098 15.084 15.38 14.54 11.9488 14.54Z"
                                            fill="currentColor"></path>
                                        <path opacity="0.4"
                                            d="M11.949 12.467C14.2851 12.467 16.1583 10.5831 16.1583 8.23351C16.1583 5.88306 14.2851 4 11.949 4C9.61293 4 7.73975 5.88306 7.73975 8.23351C7.73975 10.5831 9.61293 12.467 11.949 12.467Z"
                                            fill="currentColor"></path>
                                        <path opacity="0.4"
                                            d="M21.0881 9.21923C21.6925 6.84176 19.9205 4.70654 17.664 4.70654C17.4187 4.70654 17.1841 4.73356 16.9549 4.77949C16.9244 4.78669 16.8904 4.802 16.8725 4.82902C16.8519 4.86324 16.8671 4.90917 16.8895 4.93889C17.5673 5.89528 17.9568 7.0597 17.9568 8.30967C17.9568 9.50741 17.5996 10.6241 16.9728 11.5508C16.9083 11.6462 16.9656 11.775 17.0793 11.7948C17.2369 11.8227 17.3981 11.8371 17.5629 11.8416C19.2059 11.8849 20.6807 10.8213 21.0881 9.21923Z"
                                            fill="currentColor"></path>
                                        <path
                                            d="M22.8094 14.817C22.5086 14.1722 21.7824 13.73 20.6783 13.513C20.1572 13.3851 18.747 13.205 17.4352 13.2293C17.4155 13.232 17.4048 13.2455 17.403 13.2545C17.4003 13.2671 17.4057 13.2887 17.4316 13.3022C18.0378 13.6039 20.3811 14.916 20.0865 17.6834C20.074 17.8032 20.1698 17.9068 20.2888 17.8888C20.8655 17.8059 22.3492 17.4853 22.8094 16.4866C23.0637 15.9589 23.0637 15.3456 22.8094 14.817Z"
                                            fill="currentColor"></path>
                                        <path opacity="0.4"
                                            d="M7.04459 4.77973C6.81626 4.7329 6.58077 4.70679 6.33543 4.70679C4.07901 4.70679 2.30701 6.84201 2.9123 9.21947C3.31882 10.8216 4.79355 11.8851 6.43661 11.8419C6.60136 11.8374 6.76343 11.8221 6.92013 11.7951C7.03384 11.7753 7.09115 11.6465 7.02668 11.551C6.3999 10.6234 6.04263 9.50765 6.04263 8.30991C6.04263 7.05904 6.43303 5.89462 7.11085 4.93913C7.13234 4.90941 7.14845 4.86348 7.12696 4.82926C7.10906 4.80135 7.07593 4.78694 7.04459 4.77973Z"
                                            fill="currentColor"></path>
                                        <path
                                            d="M3.32156 13.5127C2.21752 13.7297 1.49225 14.1719 1.19139 14.8167C0.936203 15.3453 0.936203 15.9586 1.19139 16.4872C1.65163 17.4851 3.13531 17.8066 3.71195 17.8885C3.83104 17.9065 3.92595 17.8038 3.91342 17.6832C3.61883 14.9167 5.9621 13.6046 6.56918 13.3029C6.59425 13.2885 6.59962 13.2677 6.59694 13.2542C6.59515 13.2452 6.5853 13.2317 6.5656 13.2299C5.25294 13.2047 3.84358 13.3848 3.32156 13.5127Z"
                                            fill="currentColor"></path>
                                    </svg></span> Users
                            </h5>


                            @can('can add users')
                                <button type="button" class="btn  btn-primary btn-md" data-bs-toggle="modal"
                                    data-bs-target="#userModel">
                                    <i class="btn-inner">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                            viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                                        </svg>
                                    </i>
                                    <span>New User</span>
                                </button>
                            @endcan
                        </div>
                    </div>


                    <div class="card-body ">
                        <div class="table-responsive" style="overflow: hidden">
                            <table class="table  table-hover " id="user-table">
                                <thead>
                                    <tr>
                                        <th>S.no</th>
                                        <th>Name</th>
                                        <th>Gender</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Created By</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- USER MODEl START --}}
    <div class="modal fade" id="userModel" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Add User</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                        id="close"></button>
                </div>
                <div class="modal-body">
                    <form id="userForm">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="form-label">First name <span class="text-danger">*</span> </label>
                                <input class="form-control text-secondary text-secondary" placeholder="Enter First name"
                                    type="text" name="first_name" id="first_name" />
                                <small class="text-danger" id="fname_small"></small>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label">Last name <span class="text-danger">*</span> </label>
                                <input class="form-control text-secondary" placeholder="Enter Last name" type="text"
                                    name="last_name" id="last_name" />
                                <small class="text-danger" id="lname_small"></small>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label">Gender <span class="text-danger">*</span></label><br>
                                <input type="radio" name="gender" class="form-check-input" value="Male"
                                    id="Male">
                                <label class="form-check-label" for="gender">
                                    Male
                                </label>&nbsp;
                                <input type="radio" class="form-check-input" name="gender" value="Female"
                                    id="Female">
                                <label class="form-label" for="gender">
                                    Female
                                </label><br>
                                <small class="text-danger" id="gender_small"></small>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label">Email <span class="text-danger">*</span> </label>
                                <input class="form-control text-secondary" placeholder="Enter email" type="email"
                                    name="email" id="email" />
                                <small class="text-danger" id="email_small"></small>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label">Password <span class="text-danger">*</span> </label>
                                <input class="form-control text-secondary form-white" placeholder="Enter Password"
                                    type="password" name="password" id="password" />
                                <small class="text-danger" id="password_small"></small>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label">Confirm password </label>
                                <input class="form-control text-secondary form-white" placeholder="Enter Password"
                                    type="password" name="password_confirmation" id="password_confirmation" />
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label">Role <span class="text-danger">*</span></label>
                                <select class="form-select shadow-none" data-placeholder="Choose a role..."
                                    name="role_as" id="role">
                                    <option selected value="">Choose a role</option>
                                    @forelse ($roles as $role)
                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                    @empty
                                        <option value="">No Roles Found</option>
                                    @endforelse
                                </select>
                                <small class="text-danger" id="role_small"></small>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-bs-dismiss="modal"
                        class="btn btn-danger"id="cancel">Close</button>
                    <button type="button" class="btn btn-primary" id="saveUser">Save </button>
                </div>
                </form>
            </div>
        </div>
    </div>
    {{-- USER MODEl END --}}
    {{-- edit User Start --}}
    <div class="modal fade" id="editModel" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5">Update User</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                        id="close"></button>
                </div>
                <div class="modal-body">
                    <form id="userForm">
                        <div class="row">
                            <input type="hidden" id="editUsers">
                            <div class="form-group col-md-6">
                                <label class="form-label">First name <span class="text-danger">*</span> </label>
                                <input class="form-control text-secondary" placeholder="Enter First name"
                                    type="text" name="first_name" id="first_name_edit" />
                                <small class="text-danger" id="fname_small"></small>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label">Last name <span class="text-danger">*</span> </label>
                                <input class="form-control text-secondary" placeholder="Enter Last name"
                                    type="text" name="last_name" id="last_name_edit" />
                                <small class="text-danger" id="lname_small"></small>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label">Gender <span class="text-danger">*</span></label><br>
                                <input type="radio" class="form-check-input" name="gender_edit"value="Male"
                                    id="Male_edit">
                                <label class="form-check-label" for="gender">
                                    Male
                                </label>&nbsp;
                                <input type="radio" class="form-check-input" name="gender_edit" value="Female"
                                    id="Female_edit">
                                <label class="form-label" for="gender_edit">
                                    Female
                                </label><br>
                                <small class="text-danger" id="gender_small"></small>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label">Email <span class="text-danger">*</span> </label>
                                <input class="form-control text-secondary" placeholder="Enter email" type="email"
                                    name="email_edit" id="email_edit" />
                                <small class="text-danger" id="email_small"></small>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label">Role</label>
                                <select class="form-select mb-3 shadow-none text-secondary"
                                    data-placeholder="Choose a role..." name="role_as_edit" id="role_as_edit">
                                    @forelse ($roles as $role)
                                        <option value="{{ $role->id }}" id="roles_edit{{ $role->id }}">
                                            {{ $role->name }}</option>
                                    @empty
                                        <option value="">No Roles Found</option>
                                    @endforelse
                                </select>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="UpdateUser">Update </button>
                </div>
                </form>
            </div>
        </div>
    </div>
    {{-- edit User end --}}
    <div class="modal fade" id="DeleteUser" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                    style="margin-left: 29rem; margin-top: 11px;"></button>
                <div class="modal-body" style=" margin-bottom: 10px;">
                    <input type="hidden" id="deleteUser">
                    <h5 class="text-center"> Are you sure want to delete User <br> <span id="user_name" class="text-danger text-center"></span>? </h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" id="delete_btn">Delete</button>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<script src="{{ asset('js/security/user.js') }}"></script>
<script type="text/javascript">
    $(function() {
        var i = 1;
        var table = $('#user-table').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ajax: "{{ url('users') }}",
            // dom: 'Bfrtip',
            // buttons: [
            // 'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            initComplete: function(settings, json) {
              $('body').find('.dataTables_scrollBody').addClass("scrollbar");
            },
            fnRowCallback : function(nRow, aData, iDisplayIndex){
                var index = iDisplayIndex +1;
                $("td:first", nRow).html(index);
               return nRow;
            },
            columns: [
                {
                    render: function(data, type, full, meta) {
                        return i++;
                    }
                },
                {
                    data: 'full_name',
                    name: 'full_name'
                },
                {
                    data: 'gender',
                    name: 'gender'
                },
                {
                    data: 'email',
                    name: 'email'
                },
                {
                    data: 'roles[0].name',
                    name: 'roles[0].name'
                },
                {
                    data: 'created_by',
                    name: 'created_by'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true,
                    render: function(data, type, row) {
                        if (row.action === '') {
                            return table.column(6).visible(false);
                        } else {
                            return data;
                        }
                    },
                }
            ]
        });
        $('#user-table').on('dblclick', 'tr', function() {
            if(this.id != 0) {
                window.location = `user_details/${this.id}`
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("No User is Created To View");
            }
        });
    });
</script>
