<x-app-layout :assets="$assets ?? []">
    <div class="card bg-light" style="height: 40em;">
        <div class="card-header bg-primary py-3">
            <div class="header-title">
                <h5 class="mb-0 text-white">All Notifications
                    <a href="{{url()->previous()}}" class="btn btn-danger btn-sm float-end">Back</a>
                </h5>
            </div>
        </div>
        <div class="card-body p-1 notifications_scroll" style="overflow-y: auto;">
            @php
                $now = Carbon\Carbon::now();
            @endphp
            @foreach (auth()->user()->unreadNotifications as $notification)
            <div class="card m-3">
                <div class="card-body">
                    <a onclick="application_notification_markasread('{{$notification->id}}','{{env('APP_URL')}}')" href="{{env('APP_URL')}}/{{$notification->data["url_name"]}}/{{$notification->data["notify_id"]}}" class="iq-sub-card">
                       <div class="d-flex align-items-center">
                        <img class="avatar-40 rounded-pill bg-soft-primary" src="{{asset('images/shapes/01.png')}}" alt="img">
                        <div class="ms-3">
                            <h6>{{$notification->data["message"]}}</h6>
                            <p class="mb-0">{{$notification->data["created_by"]}}</p>
                        </div>
                        @php
                            $created_at = $notification->created_at;
                        @endphp
                        </div>
                        <div class="position-absolute" style="right: 15px; top: 35px;">
                            @if ($created_at->diffInSeconds($now) <= 60)
                                <small>Just Now</small>
                            @elseif ($created_at->diffInMinutes($now) == 1)
                                <small>{{$created_at->diffInMinutes($now)}} Minute Ago</small>
                            @elseif ($created_at->diffInMinutes($now) > 1 && $created_at->diffInMinutes($now) <= 60)
                                <small>{{$created_at->diffInMinutes($now)}} Minutes Ago</small>
                            @elseif ($created_at->diffInHours($now) == 1)
                                <small>{{$created_at->diffInHours($now)}} Hour Ago</small>
                            @elseif ($created_at->diffInHours($now) > 1 && $created_at->diffInHours($now) <= 12)
                                <small>{{$created_at->diffInHours($now)}} Hours Ago</small>
                            @elseif ($created_at->diffInHours($now) > 12 && $created_at->isToday())
                                <small>Today</small>
                            @elseif ($created_at->isYesterday())
                                <small>Yesterday</small>
                            @elseif ($created_at->isYesterday() == false)
                                <small>{{$created_at->format('d/m/Y')}}</small>
                            @endif
                        </div>
                    </a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</x-app-layout>
