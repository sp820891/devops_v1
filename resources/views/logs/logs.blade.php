<x-app-layout :assets="$assets ?? []">
    {{-- <div class="col-sm-12 col-lg-6"> --}}
     @livewireStyles()

        <div class="card">
            <div class="card-header d-flex justify-content-between">
               <div class="header-title">
                  <h4 class="card-title">Release Logs </h4>
               </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                   <table id="release_log_table" class="table table-striped">
                      <thead>
                         <tr>
                            <th>S.No</th>
                            <th>Git Link</th>
                            <th>Status</th>
                            <th>Reason</th>
                            <th>Rejected by</th>
                            <th>action</th>
                         </tr>
                      </thead>
                      <tbody>
                      </tbody>
                   </table>
                </div>
             </div>
         </div>
           {{-- Delete MODEL start --}}
           <div class="modal fade" id="deleteReleaseLog" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                        style="margin-left: 29rem; margin-top: 11px;"></button>
                    <div class="modal-body" style=" margin-bottom: 10px;">
                        <input type="hidden" id="deleteReleaseLogId">
                        <h5 class="text-center"> Are you sure want to delete Release Log?</h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-danger" onclick="deleteReleaseLog(this)">Delete</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Delete MODEL END --}}
         @livewire('audit-log-component')
        @livewireScripts()
    </x-app-layout>
    <script type="text/javascript">
        $(function() {
            var i = 1;
            var table = $('#release_log_table').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: false,
                scrollX: true,
                ajax: "{{ url('logs') }}",
                initComplete: function(settings, json) {
                   $('body').find('.dataTables_scrollBody').addClass("scrollbar");
                },
                fnRowCallback : function(nRow, aData, iDisplayIndex){
                    var index = iDisplayIndex +1;
                    $("td:first", nRow).html(index);
                    return nRow;
                },
                columns: [ {
                        render: function(data, type, full, meta) {
                            return i++;
                        }
                    },
                    {
                        data: 'mergerequest',
                        name: 'mergerequest',
                        render: function(data, type, row) {
                            if (row.mergerequest.git_prod_link == null) {
                                return data.git_test_link;
                            } else {
                                return data.git_prod_link;
                            }
                        }
                    },
                    {
                        data: 'mergerequest.status.name',
                        name: 'mergerequest.status.name',
                    },
                    {
                        data: 'reason.name',
                        name: 'reason.name'
                    },
                    {
                        data: 'users.full_name',
                        name: 'users.full_name',

                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: true,
                    },
                ]
            });
        });
    </script>
    <script src="{{ asset('js/logs/logs.js') }}"></script>
