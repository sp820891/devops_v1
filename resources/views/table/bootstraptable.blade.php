<x-app-layout :assets="$assets ?? []">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Merge Request</h4>
                    </div>
                    <a href="#" class="mt-lg-0 mt-md-0 mt-3 btn btn-primary btn-icon" data-bs-toggle="tooltip"
                        data-modal-form="form" data-icon="person_add" data-size="small"
                        data--href="{{ route('permission.create') }}" data-app-title="Add new permission"
                        data-placement="top" title="New Support Request">
                        <i class="btn-inner">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24"
                                stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                            </svg>
                        </i>
                        <span>New Merge Request</span>
                    </a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable" class="table table-striped" data-toggle="data-table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Project</th>
                                    <th>Issue</th>
                                    <th>Git Test Link</th>
                                    <th>Git Prod Link</th>
                                    <th>Created By</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>GIS</td>
                                    <td>issue...</td>
                                    <td>xxx</td>
                                    <td>xxx</td>
                                    <td>Sankar</td>
                                    <td>Icon</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{-- INSERT MODEL START --}}
    <div class="modal fade none-border" id="addsupport">
        <div class="modal-dialog">
            <div class="modal-content merge-model">
                <div class="modal-header">
                    <h5 class="modal-title text-primary">Support Request</h5>
                    <button type="button" class="ti-close" data-dismiss="modal" aria-label="fa-sharp fa-solid fa-xmark"
                        style="border: none;
                background: none;">
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" enctype="multipart/form-data" name="support_send" id="support_send">
                        <div class="mb-3 row">
                            <div class="col-sm-6 text-primary">
                                <i class="ti-link"> <label for="issueurl">Issue Url </label> </i>
                                <input type="text" class="form-control text-secondary" id="issueurl"
                                    name="issueurl">
                            </div>
                            <div class="col-sm-6 text-primary">
                                <i class="ti-notepad"> <label for="supportnotes">Notes </label> </i>
                                <input type="text" class="form-control text-secondary" id="supportnotes"
                                    name="supportnotes">
                            </div>
                            <div class="col-sm-6 text-primary">
                                <i class="ti-check-box"> <label for="supportpriority"> Priority</label> </i>
                                <select class="form-control  text-secondary "id="supportpriority"
                                    name="supportpriority">
                                    <option>Medium </option>


                                </select>
                            </div>
                            <div class="col-sm-6  text-primary">
                                <i class="ti-clip"><label> Attachments</label></i>
                                <label for="formFile" class="form-control text-secondary "> <i class="ti-clip">Add

                                        Attachment</i></label>
                                <input class="form-control " type="file" id="formFile" name="file[]" multiple
                                    style="display: none">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"
                                id="merge-close">Close</button>
                            <button type="button" class="btn btn-primary" name="save-merge"
                                id="">Save</button>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
    </div>
    {{-- INSERT MODEL END --}}


</x-app-layout>
