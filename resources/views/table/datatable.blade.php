<x-app-layout :assets="$assets ?? []">
<style>
    .badge {
    padding: 5px 10px;
    font-weight: 400;
    border-radius: 5px;
  }
 .badge-success{
    background: rgb(83, 194, 83);
    color: #131413;
}
.badge-failed{
    background: rgb(235, 68, 62);
    color: #131413;
}
</style>
<div class="row">
   <div class="col-sm-12">
      <div class="card">
         <div class="card-header d-flex justify-content-between">
            <div class="header-title">
               <h4 class="card-title">Support Request</h4>
            </div>
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addsupport">
                 New Support Request
              </button>
         </div>
         <div class="card-body">
            <div class="table-responsive">
               <table id="datatable" class="table table-striped" data-toggle="data-table">
                  <thead>
                     <tr>
                        <th>ID</th>
                        <th>Notesd</th>
                        <th>Project</th>
                        <th>Status</th>
                        <th>Release_Env</th>
                        <th>Issue Url Link</th>
                        <th>Priority</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>1</td>
                        <td>welcome</td>
                        <td>GIS</td>
                        <td ><p class="badge badge-success">Success</p></td>
                        <td>00</td>
                        <td>xyz@xyz.com</td>
                        <td>Medium</td>
                        <td><svg width="65" height="21" viewBox="0 0 65 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M58.7059 4.07812H64.1951V6.07812H61.9995V19.0781C61.9995 19.6304 61.5079 20.0781 60.9016 20.0781H45.5318C44.9255 20.0781 44.434 19.6304 44.434 19.0781V6.07812H42.2383V4.07812H47.7275V1.07812C47.7275 0.525845 48.219 0.078125 48.8253 0.078125H57.6081C58.2144 0.078125 58.7059 0.525845 58.7059 1.07812V4.07812ZM59.8038 6.07812H46.6297V18.0781H59.8038V6.07812ZM49.9232 9.07812H52.1189V15.0781H49.9232V9.07812ZM54.3146 9.07812H56.5102V15.0781H54.3146V9.07812ZM49.9232 2.07812V4.07812H56.5102V2.07812H49.9232Z" fill="#FF4B4B"/>
                            <path d="M2.71571 15.9706H4.26829L14.4933 6.65686L12.9407 5.24264L2.71571 14.5564V15.9706ZM20.2812 17.9706H0.52002V13.728L15.2695 0.292897C15.6984 -0.0976325 16.3934 -0.0976325 16.8221 0.292897L19.9274 3.12132C20.3561 3.51185 20.3561 4.14501 19.9274 4.53554L7.37346 15.9706H20.2812V17.9706ZM14.4933 3.82843L16.0458 5.24264L17.5984 3.82843L16.0458 2.41422L14.4933 3.82843Z" fill="#4E7FFF"/>
                            </svg>
                            </td>
                     </tr>
                     <tr>
                        <td>2</td>
                        <td>welcome</td>
                        <td>Smart meter</td>
                        <td ><p class="badge badge-failed">Failed</p></td>
                        <td>00</td>
                        <td>xyz@xyz.com</td>
                        <td>Medium</td>
                        <td><svg width="65" height="21" viewBox="0 0 65 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M58.7059 4.07812H64.1951V6.07812H61.9995V19.0781C61.9995 19.6304 61.5079 20.0781 60.9016 20.0781H45.5318C44.9255 20.0781 44.434 19.6304 44.434 19.0781V6.07812H42.2383V4.07812H47.7275V1.07812C47.7275 0.525845 48.219 0.078125 48.8253 0.078125H57.6081C58.2144 0.078125 58.7059 0.525845 58.7059 1.07812V4.07812ZM59.8038 6.07812H46.6297V18.0781H59.8038V6.07812ZM49.9232 9.07812H52.1189V15.0781H49.9232V9.07812ZM54.3146 9.07812H56.5102V15.0781H54.3146V9.07812ZM49.9232 2.07812V4.07812H56.5102V2.07812H49.9232Z" fill="#FF4B4B"/>
                            <path d="M2.71571 15.9706H4.26829L14.4933 6.65686L12.9407 5.24264L2.71571 14.5564V15.9706ZM20.2812 17.9706H0.52002V13.728L15.2695 0.292897C15.6984 -0.0976325 16.3934 -0.0976325 16.8221 0.292897L19.9274 3.12132C20.3561 3.51185 20.3561 4.14501 19.9274 4.53554L7.37346 15.9706H20.2812V17.9706ZM14.4933 3.82843L16.0458 5.24264L17.5984 3.82843L16.0458 2.41422L14.4933 3.82843Z" fill="#4E7FFF"/>
                            </svg>
                            </td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>

    {{-- INSERT MODEL START --}}
    <div class="modal fade none-border" id="addsupport">
        <div class="modal-dialog">
            <div class="modal-content merge-model">
                <div class="modal-header">
                    <h5 class="modal-title text-primary">Support Request</h5>
                    <button type="button" class="ti-close" data-dismiss="modal"
                        aria-label="fa-sharp fa-solid fa-xmark"
                        style="border: none;
                background: none;">
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" enctype="multipart/form-data" name="support_send" id="support_send">
                        <div class="mb-3 row">
                            <div class="col-sm-6 text-primary">
                                <i class="ti-link"> <label for="issueurl">Issue Url </label> </i>
                                <input type="text" class="form-control text-secondary" id="issueurl"
                                    name="issueurl">
                            </div>
                            <div class="col-sm-6 text-primary">
                                <i class="ti-notepad"> <label for="supportnotes">Notes </label> </i>
                                <input type="text" class="form-control text-secondary" id="supportnotes"
                                    name="supportnotes">
                            </div>
                            <div class="col-sm-6 text-primary">
                                <i class="ti-check-box"> <label for="supportpriority"> Priority</label> </i>
                                <select class="form-control  text-secondary "id="supportpriority" name="supportpriority">
                                    <option>Medium </option>

                                </select>
                            </div>
                            <div class="col-sm-6  text-primary">
                                <i class="ti-clip"><label> Attachments</label></i>
                                <label for="formFile" class="form-control text-secondary "> <i class="ti-clip">Add
                                        Attachment</i></label>
                                <input class="form-control " type="file" id="formFile" name="file[]" multiple
                                    style="display: none">
                            </div>
                        </div>
                            <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Submit</button>
                            </div>
                        </form>

                        </div>
                </div>
            </div>
        </div>
    </div>
    {{-- INSERT MODEL END --}}

</x-app-layout>
