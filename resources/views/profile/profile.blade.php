<x-app-layout :assets="$assets ?? []">
    <div class="row">
       <div class="col-lg-12">
          <div class="card">
             <div class="card-body">
                <div class="d-flex flex-wrap align-items-center justify-content-between">
                   <div class="d-flex flex-wrap align-items-center">
                      <div class="profile-img position-relative me-3 mb-3 mb-lg-0">
                        @if(auth()->user()->profile_picture != NULL)
                        <img src="{{asset(auth()->user()->profile_picture)}}" id="new-pic2" alt="profile_picture" class="theme-color-default-img img-fluid avatar avatar-100 avatar-rounded">
                        @else
                          <img src="{{asset('images/avatars/01.png')}}" alt="User-Profile" class="theme-color-default-img img-fluid avatar avatar-100 avatar-rounded">
                        @endif
                      </div>
                      <div class="d-flex flex-wrap align-items-center mb-3 mb-sm-0">
                         <h4 class="me-2 h4">{{ $data->full_name }}</h4>
                         <span class="text-capitalize mt-1"> - &nbsp;{{ auth()->user()->roles->pluck('name')->first() }}</span>
                      </div>
                   </div>
                   <ul class="d-flex nav nav-pills mb-0 text-center profile-tab" data-toggle="slider-tab" id="profile-pills-tab" role="tablist">
                      <li class="nav-item">
                         <a class="nav-link active show" data-bs-toggle="tab" href="#profile-profile" role="tab" aria-selected="false">Profile</a>
                      </li>
                   </ul>
                </div>
             </div>
          </div>
       </div>
       <div class="col-lg-3">
          <div class="card">
          <div class="card-header">
             <div class="header-title">
                <h4 class="card-title">Role Based Permissions</h4>
             </div>
          </div>
          <div class="card-body">
             <ul class="list-inline m-0 p-0">
                <li class="d-flex mb-2">
                   <div class="news-icon me-3">
                      <svg width="20" viewBox="0 0 24 24">
                         <path fill="currentColor" d="M20,2H4A2,2 0 0,0 2,4V22L6,18H20A2,2 0 0,0 22,16V4C22,2.89 21.1,2 20,2Z" />
                      </svg>
                   </div>
                   <p class="news-detail mb-0">there is a meetup in your city on fryday at 19:00. <a href="#">see details</a></p>
                </li>
                <li class="d-flex">
                   <div class="news-icon me-3">
                      <svg width="20" viewBox="0 0 24 24">
                         <path fill="currentColor" d="M20,2H4A2,2 0 0,0 2,4V22L6,18H20A2,2 0 0,0 22,16V4C22,2.89 21.1,2 20,2Z" />
                      </svg>
                   </div>
                   <p class="news-detail mb-0">20% off coupon on selected items at pharmaprix </p>
                </li>
             </ul>
          </div>
          </div>
          <div class="card">
          <div class="card-header d-flex align-items-center justify-content-between">
             <div class="header-title">
                <h4 class="card-title"></h4>
             </div>
             <span>132 pics</span>
          </div>
          <div class="card-body">
             <div class="d-grid gap-card grid-cols-3">
                <a data-fslightbox="gallery" href="{{asset('images/icons/04.png')}}">
                   <img src="{{asset('images/icons/04.png')}}" class="img-fluid bg-soft-info rounded" alt="profile-image">
                </a>
                <a data-fslightbox="gallery" href="{{asset('images/shapes/02.png')}}">
                   <img src="{{asset('images/shapes/02.png')}}" class="img-fluid bg-soft-primary rounded" alt="profile-image">
                </a>
                <a data-fslightbox="gallery" href="{{asset('images/icons/08.png')}}">
                   <img src="{{asset('images/icons/08.png')}}" class="img-fluid bg-soft-info rounded" alt="profile-image">
                </a>
                <a data-fslightbox="gallery" href="{{asset('images/shapes/04.png')}}">
                   <img src="{{asset('images/shapes/04.png')}}" class="img-fluid bg-soft-primary rounded" alt="profile-image">
                </a>
                <a data-fslightbox="gallery" href="{{asset('images/icons/02.png')}}">
                   <img src="{{asset('images/icons/02.png')}}" class="img-fluid bg-soft-warning rounded" alt="profile-image">
                </a>
                <a data-fslightbox="gallery" href="{{asset('images/shapes/06.png')}}">
                   <img src="{{asset('images/shapes/06.png')}}" class="img-fluid bg-soft-primary rounded" alt="profile-image">
                </a>
                <a data-fslightbox="gallery" href="{{asset('images/icons/05.png')}}">
                   <img src="{{asset('images/icons/05.png')}}" class="img-fluid bg-soft-danger rounded" alt="profile-image">
                </a>
                <a data-fslightbox="gallery" href="{{asset('images/shapes/04.png')}}">
                   <img src="{{asset('images/shapes/04.png')}}" class="img-fluid bg-soft-primary rounded" alt="profile-image">
                </a>
                <a data-fslightbox="gallery" href="{{asset('images/icons/01.png')}}">
                   <img src="{{asset('images/icons/01.png')}}" class="img-fluid bg-soft-success rounded" alt="profile-image">
                </a>
             </div>
          </div>
          </div>
          <div class="card">
          <div class="card-header">
             <div class="header-title">
                <h4 class="card-title">Twitter Feeds</h4>
             </div>
          </div>
          <div class="card-body">
             <div class="twit-feed">
                <div class="d-flex align-items-center mb-2">
                   <img class="rounded-pill img-fluid avatar-50 me-3 p-1 bg-soft-danger ps-2" src="{{asset('images/icons/03.png')}}" alt="">
                   <div class="media-support-info">
                      <h6 class="mb-0">Figma Community</h6>
                      <p class="mb-0">@figma20
                         <span class="text-primary">
                            <svg width="15" viewBox="0 0 24 24">
                               <path fill="currentColor" d="M10,17L5,12L6.41,10.58L10,14.17L17.59,6.58L19,8M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z" />
                            </svg>
                         </span>
                      </p>
                   </div>
                </div>
                <div class="media-support-body">
                   <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                   <div class="d-flex flex-wrap">
                      <a href="#" class="twit-meta-tag pe-2">#Html</a>
                      <a href="#" class="twit-meta-tag pe-2">#Bootstrap</a>
                   </div>
                   <div class="twit-date">07 Jan 2021</div>
                </div>
             </div>
             <hr class="my-4">
             <div class="twit-feed">
                <div class="d-flex align-items-center mb-2">
                   <img class="rounded-pill img-fluid avatar-50 me-3 p-1 bg-soft-primary" src="{{asset('images/icons/04.png')}}" alt="">
                   <div class="media-support-info">
                      <h6 class="mb-0">Flutter</h6>
                      <p class="mb-0">@jane59
                         <span class="text-primary">
                            <svg width="15" viewBox="0 0 24 24">
                               <path fill="currentColor" d="M10,17L5,12L6.41,10.58L10,14.17L17.59,6.58L19,8M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z" />
                            </svg>
                         </span>
                      </p>
                   </div>
                </div>
                <div class="media-support-body">
                   <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                   <div class="d-flex flex-wrap">
                      <a href="#" class="twit-meta-tag pe-2">#Js</a>
                      <a href="#" class="twit-meta-tag pe-2">#Bootstrap</a>
                   </div>
                   <div class="twit-date">18 Feb 2021</div>
                </div>
             </div>
             <hr class="my-4">
             <div class="twit-feed">
                <div class="d-flex align-items-center mb-2">
                      <img class="rounded-pill img-fluid avatar-50 me-3 p-1 bg-soft-warning pt-2" src="{{asset('images/icons/02.png')}}" alt="">
                   <div class="mt-2">
                      <h6 class="mb-0">Blender</h6>
                      <p class="mb-0">@blender59
                         <span class="text-primary">
                            <svg width="15" viewBox="0 0 24 24">
                               <path fill="currentColor" d="M10,17L5,12L6.41,10.58L10,14.17L17.59,6.58L19,8M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z" />
                            </svg>
                         </span>
                      </p>
                   </div>
                </div>
                <div class="media-support-body">
                   <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                   <div class="d-flex flex-wrap">
                      <a href="#" class="twit-meta-tag pe-2">#Html</a>
                      <a href="#" class="twit-meta-tag pe-2">#CSS</a>
                   </div>
                   <div class="twit-date">15 Mar 2021</div>
                </div>
             </div>
          </div>
       </div>
       </div>
       <div class="col-lg-6">
          <div class="profile-content tab-content">
          <div id="profile-profile" class="tab-pane active show">
             <div class="card">
                <div class="card-header">
                   <div class="header-title">
                      <h4 class="card-title">Profile Information</h4>
                      <p>Update your account's profile picture.</p>
                   </div>
                </div>
                <div class="card-body">
                   <div class="text-center">
                            <form id="form_profile_update" enctype="multipart/form-data">
                                <input type="file" name="profile_picture" id="profile_upload" style="display: none;">
                            </form>
                            <div id="profile_select" class="col-md-3" style="margin-left: 180px;">
                               <div class="file_open_profile">
                                <div class="layer"></div>
                                <span class="hoverable_profile">
                                    <svg width="24" height="24" viewBox="0 0 52 39" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M0.287598 10.1974C0.287598 8.311 1.82291 6.7818 3.71322 6.7818H48.1622C50.0542 6.7818 51.5879 8.30763 51.5879 10.1974V35.0014C51.5879 36.8878 50.0525 38.417 48.1622 38.417H3.71322C1.8213 38.417 0.287598 36.8911 0.287598 35.0014V10.1974Z" fill="#6601eb"/>
                                        <path d="M25.9377 34.9969C32.5486 34.9969 37.9078 29.6378 37.9078 23.0269C37.9078 16.416 32.5486 11.0568 25.9377 11.0568C19.3268 11.0568 13.9677 16.416 13.9677 23.0269C13.9677 29.6378 19.3268 34.9969 25.9377 34.9969Z" fill="#D8D8D8"/>
                                        <path d="M25.9378 32.4319C31.132 32.4319 35.3428 28.2211 35.3428 23.0269C35.3428 17.8326 31.132 13.6218 25.9378 13.6218C20.7435 13.6218 16.5327 17.8326 16.5327 23.0269C16.5327 28.2211 20.7435 32.4319 25.9378 32.4319Z" fill="black"/>
                                        <path d="M23.3728 21.3169C24.7894 21.3169 25.9378 20.1685 25.9378 18.7518C25.9378 17.3352 24.7894 16.1868 23.3728 16.1868C21.9561 16.1868 20.8077 17.3352 20.8077 18.7518C20.8077 20.1685 21.9561 21.3169 23.3728 21.3169Z" fill="white"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M15.6777 0.796753H36.1978L37.9078 10.2018H13.9677L15.6777 0.796753Z" fill="#6601eb"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M5.4176 3.36176H11.4026V5.92677H5.4176V3.36176Z" fill="#6601eb"/>
                                    </svg>
                                </span>
                                <span class="hoverable_profile_text">Change Profile</span>
                              </div>
                                <div class="user-profile">
                                    @if (auth()->user()->profile_picture == NULL)
                                        <img src="{{asset('images/avatars/01.png')}}" alt="profile-img" class="rounded-pill avatar-130 img-fluid">
                                    @else
                                       <img src="{{asset(auth()->user()->profile_picture)}}" id="new-pic3" alt="profile_picture" class="rounded-pill avatar-130 img-fluid">
                                    @endif
                               </div>
                            </div>
                      <div class="mt-3">
                         <h3 class="d-inline-block">{{ auth()->user()->full_name }}</h3>
                         <p class="d-inline-block pl-3"> - {{ auth()->user()->roles->pluck('name')->first() }}</p>
                         <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                      </div>
                   </div>
                </div>
             </div>
             <div class="card">
                <div class="card-header">
                   <div class="header-title">
                      <h4 class="card-title">About User</h4>
                   </div>
                </div>
                <div class="card-body">
                   <div class="user-bio">
                      <p>Tart I love sugar plum I love oat cake. Sweet roll caramels I love jujubes. Topping cake wafer.</p>
                   </div>
                   <div class="mt-2">
                   <h6 class="mb-1">Joined:</h6>
                   <p>Feb 15, 2021</p>
                   </div>
                   <div class="mt-2">
                   <h6 class="mb-1">Lives:</h6>
                   <p>United States of America</p>
                   </div>
                   <div class="mt-2">
                   <h6 class="mb-1">Email:</h6>
                   <p><a href="#" class="text-body"> austin@gmail.com</a></p>
                   </div>
                   <div class="mt-2">
                   <h6 class="mb-1">Url:</h6>
                   <p><a href="#" class="text-body" target="_blank"> www.bootstrap.com </a></p>
                   </div>
                   <div class="mt-2">
                   <h6 class="mb-1">Contact:</h6>
                   <p><a href="#" class="text-body">(001) 4544 565 456</a></p>
                   </div>
                </div>
             </div>
          </div>
       </div>
       </div>
       <div class="col-lg-3">
          <div class="card">
          <div class="card-header">
             <div class="header-title">
                <h4 class="card-title">About</h4>
             </div>
          </div>
          <div class="card-body">
             <p>Lorem ipsum dolor sit amet, contur adipiscing elit.</p>
             <div class="mb-1">Email: <a href="#" class="ms-3">nikjone@demoo.com</a></div>
             <div class="mb-1">Phone: <a href="#" class="ms-3">001 2351 256 12</a></div>
             <div>Location: <span class="ms-3">USA</span></div>
          </div>
          </div>
          <div class="card">
          <div class="card-header">
             <div class="header-title">
                <h4 class="card-title">Stories</h4>
             </div>
          </div>
          <div class="card-body">
             <ul class="list-inline m-0 p-0">
                <li class="d-flex mb-4 align-items-center active">
                   <img src="{{asset('images/icons/06.png')}}" alt="story-img" class="rounded-pill avatar-70 p-1 border bg-soft-light img-fluid">
                   <div class="ms-3">
                      <h5>Web Design</h5>
                      <p class="mb-0">1 hour ago</p>
                   </div>
                </li>
                <li class="d-flex mb-4 align-items-center">
                   <img src="{{asset('images/icons/03.png')}}" alt="story-img" class="rounded-pill avatar-70 p-1 border img-fluid bg-soft-danger">
                   <div class="ms-3">
                      <h5>App Design</h5>
                      <p class="mb-0">4 hour ago</p>
                   </div>
                </li>
                <li class="d-flex align-items-center">
                   <img src="{{asset('images/icons/07.png')}}" alt="story-img" class="rounded-pill avatar-70 p-1 border bg-soft-primary img-fluid">
                   <div class="ms-3">
                      <h5>Abstract Design</h5>
                      <p class="mb-0">9 hour ago</p>
                   </div>
                </li>
             </ul>
          </div>
          </div>
          <div class="card">
          <div class="card-header">
             <div class="header-title">
                <h4 class="card-title">Suggestions</h4>
             </div>
          </div>
          <div class="card-body">
             <ul class="list-inline m-0 p-0">
                <li class="d-flex mb-4 align-items-center">
                   <div class="img-fluid bg-soft-warning rounded-pill"><img src="{{asset('images/icons/05.png')}}" alt="story-img" class="rounded-pill avatar-40"></div>
                   <div class="ms-3 flex-grow-1">
                      <h6>Paul Molive</h6>
                      <p class="mb-0">4 mutual friends</p>
                   </div>
                   <a href="javascript:void(0);" class="btn btn-outline-primary rounded-circle btn-icon btn-sm p-2">
                      <span class="btn-inner">
                         <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" currentColor="#3a57e8">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M9.87651 15.2063C6.03251 15.2063 2.74951 15.7873 2.74951 18.1153C2.74951 20.4433 6.01251 21.0453 9.87651 21.0453C13.7215 21.0453 17.0035 20.4633 17.0035 18.1363C17.0035 15.8093 13.7415 15.2063 9.87651 15.2063Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M9.8766 11.886C12.3996 11.886 14.4446 9.841 14.4446 7.318C14.4446 4.795 12.3996 2.75 9.8766 2.75C7.3546 2.75 5.3096 4.795 5.3096 7.318C5.3006 9.832 7.3306 11.877 9.8456 11.886H9.8766Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M19.2036 8.66919V12.6792" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M21.2497 10.6741H17.1597" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                         </svg>
                      </span>
                   </a>
                </li>
                <li class="d-flex mb-4 align-items-center">
                   <div class="img-fluid bg-soft-danger rounded-pill"><img src="{{asset('images/icons/03.png')}}" alt="story-img" class="rounded-pill avatar-40"></div>
                   <div class="ms-3 flex-grow-1">
                      <h6>Robert Fox</h6>
                      <p class="mb-0">4 mutual friends</p>
                   </div>
                   <a href="javascript:void(0);" class="btn btn-outline-primary rounded-circle btn-icon btn-sm p-2">
                      <span class="btn-inner">
                         <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" currentColor="#3a57e8">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M9.87651 15.2063C6.03251 15.2063 2.74951 15.7873 2.74951 18.1153C2.74951 20.4433 6.01251 21.0453 9.87651 21.0453C13.7215 21.0453 17.0035 20.4633 17.0035 18.1363C17.0035 15.8093 13.7415 15.2063 9.87651 15.2063Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M9.8766 11.886C12.3996 11.886 14.4446 9.841 14.4446 7.318C14.4446 4.795 12.3996 2.75 9.8766 2.75C7.3546 2.75 5.3096 4.795 5.3096 7.318C5.3006 9.832 7.3306 11.877 9.8456 11.886H9.8766Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M19.2036 8.66919V12.6792" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M21.2497 10.6741H17.1597" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                         </svg>
                      </span>
                   </a>
                </li>
                <li class="d-flex mb-4 align-items-center">
                   <div class="img-fluid bg-soft-dark rounded-pill"><img src="{{asset('images/icons/06.png')}}" alt="story-img" class="rounded-pill avatar-40"></div>
                   <div class="ms-3 flex-grow-1">
                      <h6>Jenny Wilson</h6>
                      <p class="mb-0">6 mutual friends</p>
                   </div>
                   <a href="javascript:void(0);" class="btn btn-outline-primary rounded-circle btn-icon btn-sm p-2">
                      <span class="btn-inner">
                         <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" currentColor="#3a57e8">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M9.87651 15.2063C6.03251 15.2063 2.74951 15.7873 2.74951 18.1153C2.74951 20.4433 6.01251 21.0453 9.87651 21.0453C13.7215 21.0453 17.0035 20.4633 17.0035 18.1363C17.0035 15.8093 13.7415 15.2063 9.87651 15.2063Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M9.8766 11.886C12.3996 11.886 14.4446 9.841 14.4446 7.318C14.4446 4.795 12.3996 2.75 9.8766 2.75C7.3546 2.75 5.3096 4.795 5.3096 7.318C5.3006 9.832 7.3306 11.877 9.8456 11.886H9.8766Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M19.2036 8.66919V12.6792" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M21.2497 10.6741H17.1597" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                         </svg>
                      </span>
                   </a>
                </li>
                <li class="d-flex mb-4 align-items-center">
                   <div class="img-fluid bg-soft-primary rounded-pill"><img src="{{asset('images/icons/07.png')}}" alt="story-img" class="rounded-pill avatar-40"></div>
                   <div class="ms-3 flex-grow-1">
                      <h6>Cody Fisher</h6>
                      <p class="mb-0">8 mutual friends</p>
                   </div>
                   <a href="javascript:void(0);" class="btn btn-outline-primary rounded-circle btn-icon btn-sm p-2">
                      <span class="btn-inner">
                         <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" currentColor="#3a57e8">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M9.87651 15.2063C6.03251 15.2063 2.74951 15.7873 2.74951 18.1153C2.74951 20.4433 6.01251 21.0453 9.87651 21.0453C13.7215 21.0453 17.0035 20.4633 17.0035 18.1363C17.0035 15.8093 13.7415 15.2063 9.87651 15.2063Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M9.8766 11.886C12.3996 11.886 14.4446 9.841 14.4446 7.318C14.4446 4.795 12.3996 2.75 9.8766 2.75C7.3546 2.75 5.3096 4.795 5.3096 7.318C5.3006 9.832 7.3306 11.877 9.8456 11.886H9.8766Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M19.2036 8.66919V12.6792" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M21.2497 10.6741H17.1597" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                         </svg>
                      </span>
                   </a>
                </li>
                <li class="d-flex mb-4 align-items-center">
                   <div class="img-fluid bg-soft-info rounded-pill"><img src="{{asset('images/icons/04.png')}}" alt="story-img" class="rounded-pill avatar-40"></div>
                   <div class="ms-3 flex-grow-1">
                      <h6>Bessie Cooper</h6>
                      <p class="mb-0">1 mutual friends</p>
                   </div>
                   <a href="javascript:void(0);" class="btn btn-outline-primary rounded-circle btn-icon btn-sm p-2">
                      <span class="btn-inner">
                         <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" currentColor="#3a57e8">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M9.87651 15.2063C6.03251 15.2063 2.74951 15.7873 2.74951 18.1153C2.74951 20.4433 6.01251 21.0453 9.87651 21.0453C13.7215 21.0453 17.0035 20.4633 17.0035 18.1363C17.0035 15.8093 13.7415 15.2063 9.87651 15.2063Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M9.8766 11.886C12.3996 11.886 14.4446 9.841 14.4446 7.318C14.4446 4.795 12.3996 2.75 9.8766 2.75C7.3546 2.75 5.3096 4.795 5.3096 7.318C5.3006 9.832 7.3306 11.877 9.8456 11.886H9.8766Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M19.2036 8.66919V12.6792" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M21.2497 10.6741H17.1597" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                         </svg>
                      </span>
                   </a>
                </li>
                <li class="d-flex mb-4 align-items-center">
                   <div class="img-fluid bg-soft-warning rounded-pill"><img src="{{asset('images/icons/02.png')}}" alt="story-img" class="rounded-pill avatar-40"></div>
                   <div class="ms-3 flex-grow-1">
                      <h6>Wade Warren</h6>
                      <p class="mb-0">3 mutual friends</p>
                   </div>
                   <a href="javascript:void(0);" class="btn btn-outline-primary rounded-circle btn-icon btn-sm p-2">
                      <span class="btn-inner">
                         <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" currentColor="#3a57e8">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M9.87651 15.2063C6.03251 15.2063 2.74951 15.7873 2.74951 18.1153C2.74951 20.4433 6.01251 21.0453 9.87651 21.0453C13.7215 21.0453 17.0035 20.4633 17.0035 18.1363C17.0035 15.8093 13.7415 15.2063 9.87651 15.2063Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M9.8766 11.886C12.3996 11.886 14.4446 9.841 14.4446 7.318C14.4446 4.795 12.3996 2.75 9.8766 2.75C7.3546 2.75 5.3096 4.795 5.3096 7.318C5.3006 9.832 7.3306 11.877 9.8456 11.886H9.8766Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M19.2036 8.66919V12.6792" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M21.2497 10.6741H17.1597" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                         </svg>
                      </span>
                   </a>
                </li>
                <li class="d-flex mb-4 align-items-center">
                   <div class="img-fluid bg-soft-success rounded-pill"><img src="{{asset('images/icons/01.png')}}" alt="story-img" class="rounded-pill avatar-40"></div>
                   <div class="ms-3 flex-grow-1">
                      <h6>Guy Hawkins</h6>
                      <p class="mb-0">12 mutual friends</p>
                   </div>
                   <a href="javascript:void(0);" class="btn btn-outline-primary rounded-circle btn-icon btn-sm p-2">
                      <span class="btn-inner">
                         <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" currentColor="#3a57e8">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M9.87651 15.2063C6.03251 15.2063 2.74951 15.7873 2.74951 18.1153C2.74951 20.4433 6.01251 21.0453 9.87651 21.0453C13.7215 21.0453 17.0035 20.4633 17.0035 18.1363C17.0035 15.8093 13.7415 15.2063 9.87651 15.2063Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M9.8766 11.886C12.3996 11.886 14.4446 9.841 14.4446 7.318C14.4446 4.795 12.3996 2.75 9.8766 2.75C7.3546 2.75 5.3096 4.795 5.3096 7.318C5.3006 9.832 7.3306 11.877 9.8456 11.886H9.8766Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M19.2036 8.66919V12.6792" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M21.2497 10.6741H17.1597" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                         </svg>
                      </span>
                   </a>
                </li>
                <li class="d-flex align-items-center">
                   <div class="img-fluid bg-soft-info rounded-pill"><img src="{{asset('images/icons/08.png')}}" alt="story-img" class="rounded-pill avatar-40"></div>
                   <div class="ms-3 flex-grow-1">
                      <h6>Floyd Miles</h6>
                      <p class="mb-0">2 mutual friends</p>
                   </div>
                   <a href="javascript:void(0);" class="btn btn-outline-primary rounded-circle btn-icon btn-sm p-2">
                      <span class="btn-inner">
                         <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" currentColor="#3a57e8">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M9.87651 15.2063C6.03251 15.2063 2.74951 15.7873 2.74951 18.1153C2.74951 20.4433 6.01251 21.0453 9.87651 21.0453C13.7215 21.0453 17.0035 20.4633 17.0035 18.1363C17.0035 15.8093 13.7415 15.2063 9.87651 15.2063Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M9.8766 11.886C12.3996 11.886 14.4446 9.841 14.4446 7.318C14.4446 4.795 12.3996 2.75 9.8766 2.75C7.3546 2.75 5.3096 4.795 5.3096 7.318C5.3006 9.832 7.3306 11.877 9.8456 11.886H9.8766Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M19.2036 8.66919V12.6792" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M21.2497 10.6741H17.1597" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                         </svg>
                      </span>
                   </a>
                </li>
             </ul>
          </div>
          </div>
       </div>
    </div>

    @include('partials.components.share-offcanvas')
 </x-app-layout>
