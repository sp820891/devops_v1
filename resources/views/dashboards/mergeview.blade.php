<x-app-layout :assets="$assets ?? []">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

    <div class="d-flex mergecomment">
        <div id="fullScreen" class="col-lg-12 m-2">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    {{-- <div class="d-flex "> --}}
                    <a href="{{ url('mergerequests') }}">
                        <h4 class="card-title mb-0 ">Merge Request</h4>
                    </a>
                    {{-- <a href="{{ url('mergerequests') }}">
                        <svg width="32" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M4.25 12.2744L19.25 12.2744" stroke="currentColor" stroke-width="1.5"
                                stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M10.2998 18.2988L4.2498 12.2748L10.2998 6.24976" stroke="currentColor"
                                stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                        </svg>
                    </a> --}}
                    {{-- </div> --}}
                    <div class="openCommentBox">
                        <button id="open">&#9776;</button>
                    </div>
                </div>
                <div class="row p-5" id="refresh">
                    <input type="hidden" id="merge_id" value="{{ $merge->id }}">
                    <div class="col-md-6">
                        <label class="control-label">Sprint</label>
                        <p class="text-dark">{{ $sprint->name }}
                            ({{ \Carbon\Carbon::parse($sprint->start_date)->isoFormat('MMM  D') }} -
                            {{ \Carbon\Carbon::parse($sprint->end_date)->isoFormat('MMM  D') }})</p>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Project</label>
                        <p class="text-dark">{{ $project->name }}</p>

                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Git Test Link</label>
                        @if ($merge->git_test_link != null)
                            <p><a href="{{ $merge->git_test_link }}" class="text-dark">{{ $merge->git_test_link }}</a>
                            </p>
                        @else
                            <p class="text-dark">Not Updated Yet</p>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Git Prod Link</label>
                        @if ($merge->git_prod_link != null)
                            <p><a href="{{ $merge->git_prod_link }}" class="text-dark">{{ $merge->git_prod_link }}</a>
                            </p>
                        @else
                            <p class="text-dark">Not Updated Yet</p>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">SQL Update Link</label>
                        @if ($merge->sql_update_link != null)
                            <p><a href="{{ $merge->sql_update_link }}"
                                    class="text-dark">{{ $merge->sql_update_link }}</a>
                            </p>
                        @else
                            <p class="text-dark">Not Updated Yet</p>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Env Update Link</label>
                        @if ($merge->env_update_link != null)
                            <p><a href="{{ $merge->env_update_link }}">{{ $merge->env_update_link }}</a></p>
                        @else
                            <p class="text-dark">Not Updated Yet</p>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Priority</label>
                        @if ($merge->priority_id != null)
                            <p class="text-dark">{{ $priority->name }}</p>
                        @else
                            <p class="text-dark">Not Updated Yet</p>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Release Env</label>
                        <p class="text-dark">{{ $env->name }}</p>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Quality Score</label>
                        @if ($merge->quality_score != null)
                            <p class="text-dark">{{ $merge->quality_score }}</p>
                        @else
                            <p class="text-dark">Not Updated Yet</p>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Artisan Command</label>
                        @if ($merge->artisan_command != null)
                            <p class="text-dark">{{ $merge->artisan_command }}</p>
                        @else
                            <p class="text-dark">Not Updated Yet</p>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Issue Number</label>
                        <p class="text-dark">{{ $merge->issue_url }}</p>
                        {{-- <p><a href="{{ $merge->issue_url }}">{{ $merge->issue_url }}</a></p> --}}
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Status</label><br>
                        @if ($merge->status_id == 1)
                            <p class="badge badge-test">{{ $status->name }}</p>
                        @elseif ($merge->status_id == 2)
                            <p class="badge badge-prod">{{ $status->name }}</p>
                        @elseif ($merge->status_id == 3)
                            <p class="badge badge-mergedone">{{ $status->name }}</p>
                        @elseif ($merge->status_id == 4)
                            <p class="badge badge-testdone">{{ $status->name }}</p>
                        @elseif ($merge->status_id == 5)
                            <p class="badge badge-done">{{ $status->name }}</p>
                        @elseif ($merge->status_id == 6)
                            <p class="badge badge-testreject">{{ $status->name }}</p>
                        @elseif ($merge->status_id == 7)
                            <p class="badge badge-prodreject">{{ $status->name }}</p>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Test Release Date</label>
                        @if ($merge->test_release_date != null)
                            <p class="text-dark">{{ $merge->test_release_date }}</p>
                        @else
                            <p class="text-dark">Not Updated Yet</p>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Prod Release Date</label>
                        @if ($merge->prod_release_date != null)
                            <p class="text-dark">{{ $merge->prod_release_date }}</p>
                        @else
                            <p class="text-dark">Not Updated Yet</p>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Test Release Count</label>
                        @if ($merge->test_release_done_count != null)
                            <p class="text-dark">{{ $merge->test_release_done_count }}</p>
                        @else
                            <p class="text-dark">Not Updated Yet</p>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Prod Release Count</label>
                        @if ($merge->prod_release_done_count != null)
                            <p class="text-dark">{{ $merge->prod_release_done_count }}</p>
                        @else
                            <p class="text-dark">Not Updated Yet</p>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Test Reject Count</label>
                        @if ($merge->test_reject_count != null)
                            <p class="text-dark">{{ $merge->test_reject_count }}</p>
                        @else
                            <p class="text-dark">Not Updated Yet</p>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Prod Reject Count</label>
                        @if ($merge->prod_reject_count != null)
                            <p class="text-dark">{{ $merge->prod_reject_count }}</p>
                        @else
                            <p class="text-dark">Not Updated Yet</p>
                        @endif
                    </div>

                </div>
                {{-- merge done merge approve --}}
                @if (auth()->user()->roles->pluck('name')->first() == 'DevOps Admin' && $merge->status_id == 1)
                    <div class="card-footer float-end" id="mergedonebutton">
                        <button type="button" class="mergeviewapprove btn  btn-primary" data-bs-toggle="modal"
                            data-bs-target="#mergeApprove">
                            <span>Approve</span>
                        </button>
                        <button type="button" class="btn btn-danger" data-bs-toggle="modal"
                            data-bs-target="#mergereject">Reject</button>
                    </div>
                @endif

                {{-- production Approve --}}
                @if (auth()->user()->roles->pluck('name')->first() == 'DevOps Engineer' && $merge->status_id == 2)
                    <div class="card-footer" id="prodApprovebutton">
                        <button type="button" class="mergeviewapprove btn  btn-primary" data-bs-toggle="modal"
                            data-bs-target="#ProdApprove">
                            <span>Approve</span>
                        </button>
                        <button type="button" class="btn btn-danger " data-bs-toggle="modal"
                            data-bs-target="#prodreject">Reject</button>
                    </div>
                @endif

                {{-- test approve --}}
                @if (auth()->user()->roles->pluck('name')->first() == 'DevOps QA' && $merge->status_id == 3)
                    <div class="card-footer float-end" id="testApprovebutton">
                        <button type="button" class="QaApprove btn  btn-primary" data-bs-toggle="modal"
                            data-bs-target="#QaApprove">
                            <span>Approve</span>
                        </button>
                        <button type="button" class="btn btn-danger" data-bs-toggle="modal"
                            data-bs-target="#QaReject">Reject</button>
                    </div>
                @endif
            </div>
        </div>
        {{-- comments --}}
        <div id="chat" class="col-lg-3 m-2 comment-boxx">
            <div class="card ">
                <div class="card-header d-flex justify-content-between closeCommentButton">
                    <h5>Comments</h5>
                    <button type="button" id="close">&times;</button>
                </div>
                <div class="card-footer text_box">
                    <form class="comment-text">
                        <div class="d-flex area ">
                            <textarea class="form-control form-white comment-box" placeholder="Enter Comment" type="text " name=""
                                id="casual_comment" style="min-height: calc(5rem + 1rem + 2px);"></textarea>
                            <button type="submit" class="comment-snd primary" id="up-comment"><i
                                    class="fa-solid fa-paper-plane"></i></button>
                        </div>
                    </form>
                </div>
                <div class="row m-2 overflow" id="comments_view">

                </div>
            </div>
        </div>
        {{-- comments --}}
    </div>

    {{-- Merge Approve model --}}
    <div class="modal fade none-border" id="mergeApprove">
        <div class="modal-dialog">
            <div class="modal-content merge-model">
                <div class="modal-header">
                    <h5 class="modal-title text-primary">Merge Request</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body">
                    <form id="approve_view_merge">
                        <div class="mb-3">
                            <div class="col-md-12">
                                <input type="hidden" id="viewmerge_approve_id" value="{{ $id }}">
                                <label for="quality_score">Quality Score</label> </i>
                                <input type="number" name="quality_score" id="quality_score"
                                    class="form-control  text-secondary " placeholder="Enter Quality Score">

                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Approve</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Merge Approve model end --}}
    {{-- Qa approve model --}}
    <div class="modal fade none-border" id="QaApprove">
        <div class="modal-dialog">
            <div class="modal-content merge-model">
                <div class="modal-header">
                    <h5 class="modal-title text-primary">Qa Approve</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body">
                    <form id="approve_view_qa">
                        <div class="mb-3">
                            <div class="col-md-12">
                                <input type="hidden" id="viewqa_approve_id" value="{{ $id }}">
                                <h5>Sure You Want To Approve {{ $merge->issue_url }}</h5>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-primary">Approve</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Qa approve model --}}
    {{-- ProdApprove approve model --}}
    <div class="modal fade none-border" id="ProdApprove">
        <div class="modal-dialog">
            <div class="modal-content merge-model">
                <div class="modal-header">
                    <h5 class="modal-title text-primary">Production Approve</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body">
                    <form id="ProdApprove_form">
                        <div class="mb-3">
                            <div class="col-md-12">
                                <input type="hidden" id="viewprod_approv_id" value="{{ $id }}">
                                <h5>Sure You Want To Approve {{ $merge->issue_url }}</h5>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="prod_approve">Approve</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    {{-- ProdApprove approve model --}}

    {{-- Test Reject --}}
    <div class="modal fade none-border" id="QaReject">
        <div class="modal-dialog">
            <div class="modal-content merge-model">
                <div class="modal-header">
                    <h5 class="modal-title text-primary">Test Reject</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body">
                    <form id="QaReject_form">
                        <div class="mb-3">
                            <div class="col-md-12">
                                <input type="hidden" id="QaReject_id" value="{{ $id }}">

                                <label for="status">Reject Reasons</label>
                                <select class="form-control  text-secondary "id="reject" name="reject_reason">
                                    <option selected value="">_</option>
                                    @foreach ($reject as $rejects)
                                        <option value="{{ $rejects->id }}" id="rejects_option{{ $rejects->id }}">
                                            {{ $rejects->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-12">
                                <label for="status">Comment</label><br>
                                <textarea name="comment" id="comment" class="form-control" cols="49" rows="3"></textarea>
                            </div>
                        </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">Reject</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Test Reject End --}}

    {{-- Merge Reject Start --}}
    <div class="modal fade none-border" id="mergereject">
        <div class="modal-dialog">
            <div class="modal-content merge-model">
                <div class="modal-header">
                    <h5 class="modal-title text-primary">Test Reject</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body">
                    <form id="MergeReject_form">
                        <div class="mb-3">
                            <div class="col-md-12">
                                <input type="hidden" id="MergeReject_id" value="{{ $id }}">

                                <label for="status">Reject Reasons</label>
                                <select class="form-control  text-secondary "id="reject" name="reject_reason">
                                    <option selected value="">_</option>
                                    @foreach ($reject as $rejects)
                                        <option value="{{ $rejects->id }}" id="rejects_option{{ $rejects->id }}">
                                            {{ $rejects->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-12">
                                <label for="status">Comment</label><br>
                                <textarea name="comment" id="comment" class="form-control" cols="49" rows="3"></textarea>
                            </div>
                        </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">Reject</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Merge Reject End --}}

    {{-- prod Reject Start --}}
    <div class="modal fade none-border" id="prodreject">
        <div class="modal-dialog">
            <div class="modal-content merge-model">
                <div class="modal-header">
                    <h5 class="modal-title text-primary">Prod Reject</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body">
                    <form id="prodReject_form">
                        <div class="mb-3">
                            <div class="col-md-12">
                                <input type="hidden" id="ProdReject_id" value="{{ $id }}">

                                <label for="status">Reject Reasons</label>
                                <select class="form-control  text-secondary "id="reject" name="reject_reason">
                                    <option selected value="">_</option>
                                    @foreach ($reject as $rejects)
                                        <option value="{{ $rejects->id }}" id="rejects_option{{ $rejects->id }}">
                                            {{ $rejects->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-12">
                                <label for="status">Comment</label><br>
                                <textarea name="comment" id="comment" class="form-control" cols="49" rows="3"></textarea>
                            </div>
                        </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">Reject</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    {{-- prod Reject End --}}

</x-app-layout>

<script src="{{ asset('js/main/merge.js') }}"></script>
