<x-app-layout :assets="$assets ?? []">
    <div class="row">
        <div class="col-sm-12">

            {{-- <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Merge Requests</h4>
                    </div>
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#add_merge">
                        <i class="btn-inner">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24"
                                stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                            </svg>
                        </i> New Merge Request</button>
                </div> --}}
            <div class="card">
                <div class="card-header ">
                    <div class="header-title d-flex justify-content-between">
                        <h5 class="card-title"> <span>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <g opacity="0.4">
                                <path d="M19 16V6.5C19 5.4 18.1 4.5 17 4.5H12.5" stroke="currentColor" stroke-width="1.5"
                                    stroke-linecap="round" stroke-linejoin="round" />
                                <path d="M15 2L12 4.5L15 7" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                                    stroke-linejoin="round" />
                            </g>
                            <path opacity="0.4" d="M5 9V16" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path
                                d="M5.25 8.5C6.11195 8.5 6.9386 8.15759 7.5481 7.5481C8.15759 6.9386 8.5 6.11195 8.5 5.25C8.5 4.38805 8.15759 3.5614 7.5481 2.9519C6.9386 2.34241 6.11195 2 5.25 2C4.38805 2 3.5614 2.34241 2.9519 2.9519C2.34241 3.5614 2 4.38805 2 5.25C2 6.11195 2.34241 6.9386 2.9519 7.5481C3.5614 8.15759 4.38805 8.5 5.25 8.5ZM5 22C5.79565 22 6.55871 21.6839 7.12132 21.1213C7.68393 20.5587 8 19.7956 8 19C8 18.2044 7.68393 17.4413 7.12132 16.8787C6.55871 16.3161 5.79565 16 5 16C4.20435 16 3.44129 16.3161 2.87868 16.8787C2.31607 17.4413 2 18.2044 2 19C2 19.7956 2.31607 20.5587 2.87868 21.1213C3.44129 21.6839 4.20435 22 5 22ZM19 22C19.7956 22 20.5587 21.6839 21.1213 21.1213C21.6839 20.5587 22 19.7956 22 19C22 18.2044 21.6839 17.4413 21.1213 16.8787C20.5587 16.3161 19.7956 16 19 16C18.2044 16 17.4413 16.3161 16.8787 16.8787C16.3161 17.4413 16 18.2044 16 19C16 19.7956 16.3161 20.5587 16.8787 21.1213C17.4413 21.6839 18.2044 22 19 22Z"
                                stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                        </svg></span> Merge Requests</h5>

                        <button type="button" class="btn btn-primary btn-md" data-bs-toggle="modal" data-bs-target="#add_merge">
                            <i class="btn-inner">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                    viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                        d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                                </svg>
                            </i> New Merge Request</button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="merge_table" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>S.no</th>
                                    <th>Project</th>
                                    <th>Status</th>
                                    <th>Issue</th>
                                    <th>Git Test Link</th>
                                    <th>Git Prod Link</th>
                                    <th>Created By</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- INSERT MODEL START --}}
    <div class="modal fade none-border" id="add_merge">
        <div class="modal-dialog">
            <div class="modal-content merge-model">
                <div class="modal-header">
                    <h5 class="modal-title text-primary">Merge Request</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="post" enctype="multipart/form-data" name="save_merge" id="save_merge">
                        <div class="row">
                            <div class="col-sm-6 p-3">
                                <label for="issueurl">Issue Number </label>
                                <input type="number" class="form-control text-secondary" id="issue_url" name="issue_no"
                                    placeholder="Issue No">
                                <span class="text-danger" id="issue_no_small_merge"></span>
                            </div>
                            <br>
                            <div class="col-sm-6 p-3">
                                <label for="issueurl">Git test link</label>
                                <input type="text" class="form-control text-secondary" id="git_test_link"
                                    name="git_test_link">
                            </div>
                            <br>


                            {{--  <div class="col-sm-6 p-3">
                                <label for="issueurl">SQL Update Link</label>
                                <input type="text" class="form-control text-secondary" id="sql_update_link"
                                    name="sql_update_link">
                            </div>
                            <br>
                            <div class="col-sm-6 p-3">
                                <label for="issueurl">Env Update Link</label>
                                <input type="text" class="form-control text-secondary" id="env_update_link"
                                    name="env_update_link">
                            </div>
                            <br>
                            <div class="col-sm-6 p-3">
                                <label for="issueurl">Release Env</label>
                                <select class="form-control  text-secondary "id="release_env"
                                    name="release_env">
                                    <option value="1">Medium </option>
                                    <option value="2">High </option>
                                    <option value="3">Low </option>
                                </select>
                            </div>
                            <div class="col-sm-6 p-3 ">
                                <label for="supportnotes">Priority</label>
                                <select class="form-control  text-secondary "id="merge_priority"
                                    name="priority">
                                    <option value="1">Medium </option>
                                    <option value="2">High </option>
                                    <option value="3">Low </option>
                                </select>
                            </div> --}}
                            <br>
                            <div class="col-sm-6 p-3">
                                <label> Attachments</label>
                                <label for="formFileMerge" class="form-control text-secondary "> <svg width="20"
                                        viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M14.7366 2.76175H8.08455C6.00455 2.75375 4.29955 4.41075 4.25055 6.49075V17.3397C4.21555 19.3897 5.84855 21.0807 7.89955 21.1167C7.96055 21.1167 8.02255 21.1167 8.08455 21.1147H16.0726C18.1416 21.0937 19.8056 19.4087 19.8026 17.3397V8.03975L14.7366 2.76175Z"
                                            stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                                            stroke-linejoin="round"></path>
                                        <path d="M14.4741 2.75V5.659C14.4741 7.079 15.6231 8.23 17.0431 8.234H19.7971"
                                            stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                                            stroke-linejoin="round"></path>
                                        <path d="M14.2936 12.9141H9.39355" stroke="currentColor" stroke-width="1.5"
                                            stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M11.8442 15.3639V10.4639" stroke="currentColor" stroke-width="1.5"
                                            stroke-linecap="round" stroke-linejoin="round"></path>
                                    </svg> Attachment </label>
                                <input class="form-control" type="file" id="formFileMerge" name="file[]" multiple
                                    style="display: none">
                                <span class="text-success" id="mergeFile"></span>
                            </div>
                            <br>
                            <div class="col-sm-6 p-3">
                                <label for="supportpriority">Artisan Command</label>
                                <textarea class="form-control text-secondary" id="artisan" name="artisan_command" cols="30" rows="3"></textarea>
                            </div>
                            <br>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal"
                                id="close">Close</button>
                            <button type="submit" class="btn btn-primary" id="save_merge_button"
                                name="save_merge">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
    {{-- INSERT MODEL END --}}
    {{-- Edit MODEL START --}}
    <div class="modal fade none-border" id="edit_merge">
        <div class="modal-dialog">
            <div class="modal-content merge-model">
                <input type="hidden" id="editUsers">
                <div class="modal-header">
                    <h5 class="modal-title text-primary">Merge Request</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form enctype="multipart/form-data" name="edit_merge" id="update_merge">
                        <div class="row">
                            <div class="col-sm-6 p-3">
                                <label for="issueurl">Issue Number </label>
                                <input type="number" class="form-control text-secondary" id="issue_url_edit"
                                    name="issue_url">
                            </div>
                            <br>
                            <div class="col-sm-6 p-3">
                                <label for="status">Status</label>
                                <select class="form-control  text-secondary "id="status_edit" name="status_id">
                                    {{-- <option selected value=""></option> --}}
                                    @foreach ($statuses as $status)
                                        <option value="{{ $status->id }}" id="status_option{{ $status->id }}">
                                            {{ $status->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <div class="col-sm-6 p-3">
                                <label for="issueurl">Git test link</label>
                                <input type="text" class="form-control text-secondary" id="git_test_link_edit"
                                    name="git_test_link_edit">
                            </div>
                            <div class="col-sm-6 p-3">
                                <label for="issueurl">Git prod link</label>
                                <input type="text" class="form-control text-secondary" id="git_prod_link"
                                    name="git_prod_link">
                            </div>

                            <div class="col-sm-6 p-3">
                                <label for="quality_score">Quality Score </label>
                                <input type="number" class="form-control text-secondary" id="quality_score_edit"
                                    name="quality_score">
                            </div>
                            <br>
                            <div class="col-sm-6 p-3">
                                <label for="issueurl">Release Env</label>
                                <select class="form-control  text-secondary "id="release_env_edit"
                                    name="release_env">
                                    @foreach ($releaseenvs as $releaseenv)
                                        <option value="{{ $releaseenv->id }}" id="env_option{{ $releaseenv->id }}">
                                            {{ $releaseenv->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-6 p-3 ">
                                <label for="merge_priority">Priority</label>
                                <select class="form-control  text-secondary "id="merge_priority_edit"
                                    name="priority_id">
                                    @foreach ($priorities as $priority)
                                        <option value="{{ $priority->id }}" id="priority_option{{ $priority->id }}">
                                            {{ $priority->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <div class="col-sm-6 p-3">
                                <label for="supportpriority">Artisan Commed</label>
                                <textarea class="form-control text-secondary" id="artisan_edit" name="artisan_command" cols="30"
                                    rows="1"></textarea>
                            </div>
                            <br>
                            <div class="col-sm-6 p-3">
                                <label> Attachments</label>
                                <label for="formFile" class="form-control text-secondary ">
                                    <svg width="20" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M14.7366 2.76175H8.08455C6.00455 2.75375 4.29955 4.41075 4.25055 6.49075V17.3397C4.21555 19.3897 5.84855 21.0807 7.89955 21.1167C7.96055 21.1167 8.02255 21.1167 8.08455 21.1147H16.0726C18.1416 21.0937 19.8056 19.4087 19.8026 17.3397V8.03975L14.7366 2.76175Z"
                                            stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                                            stroke-linejoin="round"></path>
                                        <path d="M14.4741 2.75V5.659C14.4741 7.079 15.6231 8.23 17.0431 8.234H19.7971"
                                            stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                                            stroke-linejoin="round"></path>
                                        <path d="M14.2936 12.9141H9.39355" stroke="currentColor" stroke-width="1.5"
                                            stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M11.8442 15.3639V10.4639" stroke="currentColor" stroke-width="1.5"
                                            stroke-linecap="round" stroke-linejoin="round"></path>
                                    </svg> Attachment </label>
                                <input class="form-control " type="file" id="formFile_edit" name="file[]"
                                    multiple style="display: none">
                            </div>
                            <br>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" id="edit_merge_button"
                                name="save_merge">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
    {{-- Edit MODEL END --}}
    {{-- Delete Model --}}
    <div class="modal fade" id="delete_merge_model" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <input type="hidden" id="deleteUsers">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                    style="margin-left: 94.5%; margin-top: 13px; font-size:12px;"></button>
                <div class="modal-body" style=" margin-bottom: 30px;">
                    <h5 class="text-center"> Are you sure want to delete this <span id="issue_num_model"
                            class="text-danger text-center">Merge Request</span>?
                    </h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" id="delete_merge">Delete</button>
                </div>
            </div>
        </div>
    </div>
    {{-- Delete Model End --}}

</x-app-layout>
<script type="text/javascript">
    $(function() {
        var i = 1;
        var table = $('#merge_table').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            scrollX: true,
            // dom: 'Bfrtip',
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: "{{ url('fetchMerge') }}",
            initComplete: function(settings, json) {
              $('body').find('.dataTables_scrollBody').addClass("scrollbar");
            },
            fnRowCallback : function(nRow, aData, iDisplayIndex){
                var index = iDisplayIndex +1;
                $("td:first", nRow).html(index);
               return nRow;
            },
            columns: [ {
                    render: function(data, type, full, meta) {
                        return i++;
                    }
                },
                {
                    data: 'projects.name',
                    name: 'projects.name',
                    render: function(data, type, row) {
                        if (row.project_id === null) {
                            return '';
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'status.name',
                    name: 'status.name',
                    render: function(data, type, row) {
                        if (row.status_id === 1) {
                            $testrequest = `<span class=" badge badge-test">${data}</span>`
                            return $testrequest;
                        } else if (row.status_id === 2) {
                            $prodrequest = `<span class=" badge badge-prod">${data}</span>`
                            return $prodrequest;
                        } else if (row.status_id === 3) {
                            $mergedone = `<span class=" badge badge-mergedone">${data}</span>`
                            return $mergedone;
                        } else if (row.status_id === 4) {
                            $testdone = `<span class=" badge badge-testdone">${data}</span>`
                            return $testdone;
                        } else if (row.status_id === 5) {
                            $proddone = `<span class=" badge badge-done">${data}</span>`
                            return $proddone;
                        } else if (row.status_id === 6) {
                            $testreject = `<span class=" badge badge-testreject">${data}</span>`
                            return $testreject;
                        } else if (row.status_id === 7) {
                            $prodreject = `<span class=" badge badge-prodreject">${data}</span>`
                            return $prodreject;
                        }

                    }
                },
                {
                    data: 'issue_url',
                    name: 'issue_url'
                },
                {
                    data: 'git_test_link',
                    name: 'git_test_link',
                    render: function(data, type, row) {
                        if (row.git_test_link === null) {
                            return '';
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'git_prod_link',
                    name: 'git_prod_link',
                    render: function(data, type, row) {
                        if (row.git_prod_link === null) {
                            return 'Yet To Update';
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'user.full_name',
                    name: 'user.full_name'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true,
                    render: function(data, type, row) {
                        if (row.action === '') {
                            return table.column(7).visible(false);
                        } else {
                            return data;
                        }
                    },
                },
            ]
        });
        $('#merge_table').on('dblclick', 'tr', function() {
            if(this.id != 0) {
                window.location = `mergerequest_view/${this.id}`
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("No Merge Request is Created To View");
            }
        });
    });
</script>
<script src="{{ asset('js/main/merge.js') }}"></script>
