<x-app-layout :assets="$assets ?? []">
    <style>
        .control-label {
            color: #3a57e8 !important;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <div class="col-lg-12">
        <div class="card">
            <div class="row p-4" id="refresh_support">
                <div class="col-md-12">
                    <a href="{{ url('supportrequests') }}" style="cursor: pointer; font-size:18px; color:gray;"><i
                            class="fa-solid fa-backward"></i></a>
                </div><br><br>
                <h4 class="card-title mb-0">SupportRequests</h4><br><br>
                <div class="col-md-6">
                    <label class="control-label">Notes</label>
                    <p class="text-secondary">{{ $supportrequest->notes }}</p>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Issue Number</label>
                    <p><a class="text-secondary"
                            href="{{ $supportrequest->issue_url }}">{{ $supportrequest->issue_url }}</a></p>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Project</label>
                    <p class="text-secondary">{{ $supportrequest->Project->name }}</p>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Priority</label>
                    <p class="text-secondary">{{ $supportrequest->priority->name }}</p>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Release Env</label>
                    <p class="text-secondary">{{ $supportrequest->Releaseenv->name }}</p>
                </div>

                <div class="col-md-6">
                    <label class="control-label">Status</label>
                    <br>
                    {{-- <p class="text-secondary">{{ $supportrequest->status->name }}</p> --}}
                    @if ($supportrequest->status_id==1)
                    <p class="badge badge-warning">{{ $supportrequest->status->name }}</p>
                    @elseif ($supportrequest->status_id==2)
                    <p class="badge badge-prod " style=" background:green; color:#fff;">{{ $supportrequest->status->name }}</p>
                    @elseif ($supportrequest->status_id==3)
                    <p class="badge badge-testreject">{{ $supportrequest->status->name}}</p>
                    @endif
                </div>
                <div class="col-md-6">
                    <label class="control-label">Attachments</label>
                   @forelse ($supportdocument as $supports)
                     <input type="hidden" value="{{$supports->id}}" id="supportDocsID">
                     <p class="support_docs_delete"><a class="text-dark" target="_blank" href="{{asset("support-uploads/".$supports->Filepath)}}">{{$supports->Filepath}}</a>
                     <a type="button" href ="{{url("docdeleted/$supports->id")}}" id="document_deleted"><svg class="hover_support_docs" width="15" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" stroke="#d9534f">
                        <path d="M19.3248 9.46826C19.3248 9.46826 18.7818 16.2033 18.4668 19.0403C18.3168 20.3953 17.4798 21.1893 16.1088 21.2143C13.4998 21.2613 10.8878 21.2643 8.27979 21.2093C6.96079 21.1823 6.13779 20.3783 5.99079 19.0473C5.67379 16.1853 5.13379 9.46826 5.13379 9.46826" stroke="#d9534f" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                        <path d="M20.708 6.23975H3.75" stroke="#d9534f" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                        <path d="M17.4406 6.23973C16.6556 6.23973 15.9796 5.68473 15.8256 4.91573L15.5826 3.69973C15.4326 3.13873 14.9246 2.75073 14.3456 2.75073H10.1126C9.53358 2.75073 9.02558 3.13873 8.87558 3.69973L8.63258 4.91573C8.47858 5.68473 7.80258 6.23973 7.01758 6.23973" stroke="#d9534f" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                    </svg></a></p>
                    @empty
                      <p class="text-dark">Null</p>
                    @endforelse

                </div>
                @if (auth()->user()->roles->pluck('name')->first() == 'DevOps Engineer' && $supportrequest->status_id != 3 && $supportrequest->status_id != 2)
                <div class="card-footer">
                   <button type="button" class=" btn  btn-primary" data-bs-toggle="modal"
                       data-bs-target="#SupportApprove">
                       <span>Approve</span>
                   </button>
                   <button type="button" class="btn btn-danger" data-bs-toggle="modal"
                       data-bs-target="#SupportReject">Reject</button>
               </div>
           @endif
            </div>
        </div>
        {{-- <div class="col-lg-3 m-2">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <h5>Comments</h5>
                </div>
                <div class="row">
                </div>
                <div class="card-footer text_box">
                    <form class="comment-text">
                        <textarea class="form-control form-white comment-box" placeholder="Enter Commend" type="text " name=""
                            id="casual_comment"></textarea>
                        <button type="submit" class="comment-snd" id="up-comment" style="color: rgb(10, 14, 117);"><i
                                class="fa-solid fa-paper-plane"></i></button>
                    </form>
                </div>
            </div>
        </div> --}}
    </div>
{{-- Support Approve model --}}
<div class="modal fade none-border" id="SupportApprove">
    <div class="modal-dialog">
        <div class="modal-content merge-model">
            <div class="modal-header">
                <h5 class="modal-title text-primary">Support Request</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="approve_view_support">
                    <div>
                        <div class="col-md-12">
                            <input type="hidden" id="viewsupport_approve_id" value="{{ $supportrequest->id }}">
                                <h5>Sure You Want To Approve </h5>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Approve</button>
            </div>
            </form>
        </div>
    </div>
</div>
{{-- Support Approve model end --}}

{{-- Support Reject Modal --}}
<div class="modal fade none-border" id="SupportReject">
    <div class="modal-dialog">
        <div class="modal-content merge-model">
            <div class="modal-header">
                <h5 class="modal-title text-primary">Support Reject</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <form id="SupportReject_form">
                    <div>
                        <div class="col-md-12">
                            <input type="hidden" id="support_reject" value="{{ $supportrequest->id}}">
                            <h5>Sure You Want To Reject </h5>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Reject</button>
            </div>
            </form>
        </div>
    </div>
</div>
{{-- Support Reject model end --}}
</x-app-layout>
<script src="{{ asset('js/main/support.js')}}"></script>

