<x-app-layout :assets="$assets ?? []">
    <style>
        .badge {
        padding: 5px 10px;
        font-weight: 400;
        border-radius: 5px;
      }
     .badge-success{
        background: rgb(83, 194, 83);
        color: #131413;
    }
    .badge-failed{
        background: rgb(235, 68, 62);
        color: #131413;
    }
    </style>
    <div class="row">
       <div class="col-sm-12">
          <div class="card">
             <div class="card-header d-flex justify-content-between">
                    <h5 class="card-title"> <span>
                    <svg width="24" height="24" viewBox="0 0 1266 1500" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" fill-opacity="0.5" clip-rule="evenodd"
                        d="M0 1186.29C0 1129.11 45.8478 1077.25 102.525 1070.44L616.685 1008.63L1130.84 1070.44C1187.47 1077.25 1233.37 1128.94 1233.37 1186.29V1499.39H0V1186.29Z"
                        fill="currentColor" />
                    <path fill-opacity="0.25" fill-rule="evenodd" clip-rule="evenodd"
                        d="M456.423 968.547C329.752 878.562 242.154 695.543 242.154 546.038C242.154 339.19 409.837 171.507 616.685 171.507C823.533 171.507 991.216 339.19 991.216 546.038C991.216 695.543 903.617 878.562 776.947 968.547V1160.08C776.947 1160.08 668.305 1209.41 616.685 1209.41C565.065 1209.41 456.423 1160.08 456.423 1160.08V968.547Z"
                        fill="currentColor" />
                    <path
                        d="M180.808 594.254V206.58C180.808 106.848 246.458 26 327.306 26H912.521C993.43 26 1059.02 106.813 1059.02 206.58V594.254"
                        stroke="currentColor" stroke-width="51.6595" stroke-linecap="round" />
                    <path d="M774.892 1059.19C1031.67 1059.19 1239.83 851.031 1239.83 594.254" stroke="currentColor"
                        stroke-width="51.6595" stroke-linecap="round" />
                    <path
                        d="M749.062 1033.36H568.254C553.989 1033.36 542.424 1044.92 542.424 1059.19C542.424 1073.45 553.989 1085.02 568.254 1085.02H749.062C763.328 1085.02 774.892 1073.45 774.892 1059.19C774.892 1044.92 763.328 1033.36 749.062 1033.36Z"
                        stroke="currentColor" stroke-width="51.6595" />
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M1214 490.935H1239.83V620.084H1214V490.935Z"
                        stroke="currentColor" stroke-width="51.6595" />
                    <path fill-opacity="0.5" fill-rule="evenodd" clip-rule="evenodd"
                        d="M203.409 271.383C203.409 178.658 278.584 103.489 371.172 103.489H862.198C954.851 103.489 1029.96 178.846 1029.96 271.383V439.276L616.685 286.343L203.409 439.276V271.383Z"
                        fill="currentColor" />
                    <path
                        d="M1136.51 335.957H1033.19C1004.66 335.957 981.53 359.086 981.53 387.616V723.403C981.53 751.934 1004.66 775.062 1033.19 775.062H1136.51C1165.04 775.062 1188.17 751.934 1188.17 723.403V387.616C1188.17 359.086 1165.04 335.957 1136.51 335.957Z"
                        fill="currentColor" />
                    <path
                        d="M206.638 335.957H103.319C74.7882 335.957 51.6594 359.086 51.6594 387.616V723.403C51.6594 751.934 74.7882 775.062 103.319 775.062H206.638C235.169 775.062 258.297 751.934 258.297 723.403V387.616C258.297 359.086 235.169 335.957 206.638 335.957Z"
                        fill="currentColor" />
                </svg>
                  </span> Support Requests</h5>
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addsupport">
                    <i class="btn-inner">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                        </svg>
                    </i>
                     New Support Request
                  </button>
             </div>
             <div class="card-body">
                <div class="table-responsive">
                   <table id="support_table" class="table table-hover">
                      <thead>
                         <tr>
                            <th>Id</th>
                            <th>Project</th>
                            <th>Release_Env</th>
                            <th>Priority</th>
                            <th>Status</th>
                            <th>Action</th>
                         </tr>
                      </thead>
                      <tbody>
                      </tbody>
                   </table>
                </div>
             </div>
          </div>
       </div>
    </div>
        {{-- INSERT MODEL START --}}
        <div class="modal fade none-border" id="addsupport">
            <div class="modal-dialog">
                <div class="modal-content merge-model" style="margin: 100px 0 0 -10px;">
                    <div class="modal-header">
                        <h5 class="modal-title text-primary">Support Request</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form enctype="multipart/form-data" id="support_send">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="issueurl">Issue Number </label>
                                    <input type="Number" class="form-control text-secondary" id="issueurl"
                                        name="issue_no">
                                    <small class="text-danger" id="issue_no_small"></small>
                                </div>
                                <div class="col-sm-6 ">
                                   <label for="supportnotes">Notes </label>
                                    <input type="text" class="form-control text-secondary" id="supportnotes"
                                        name="supportnotes">
                                </div>
                                <div class="col-sm-6  ">
                                <label> Attachments</label>
                                    <label for="formFileSupport" class="form-control text-secondary ">
                                        <svg width="20"
                                        viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M14.7366 2.76175H8.08455C6.00455 2.75375 4.29955 4.41075 4.25055 6.49075V17.3397C4.21555 19.3897 5.84855 21.0807 7.89955 21.1167C7.96055 21.1167 8.02255 21.1167 8.08455 21.1147H16.0726C18.1416 21.0937 19.8056 19.4087 19.8026 17.3397V8.03975L14.7366 2.76175Z"
                                            stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                                            stroke-linejoin="round"></path>
                                        <path d="M14.4741 2.75V5.659C14.4741 7.079 15.6231 8.23 17.0431 8.234H19.7971"
                                            stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                                            stroke-linejoin="round"></path>
                                        <path d="M14.2936 12.9141H9.39355" stroke="currentColor" stroke-width="1.5"
                                            stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M11.8442 15.3639V10.4639" stroke="currentColor" stroke-width="1.5"
                                            stroke-linecap="round" stroke-linejoin="round"></path>
                                    </svg>
                                            Attachment</i></label>
                                    <input class="form-control " type="file" id="formFileSupport" name="file[]" multiple
                                        style="display: none">
                                    <small class="text-success" id="supportFile"></small>
                                </div>
                            </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary" id="save-merge">Save</button>
                                </div>
                            </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- INSERT MODEL END --}}
         {{-- Edit MODEL START --}}
         <div class="modal fade none-border" id="editModel">
            <div class="modal-dialog">
                <div class="modal-content merge-model"style="margin: 100px 0 0 -110px; width:150%;">
                    <input type="hidden" id="edit_support">
                    <div class="modal-header">
                        <h5 class="modal-title text-primary">Support Request</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form  enctype="multipart/form-data" id="updated-support">
                        <div class="row">
                            <div class="col-md-4">
                                <label class="control-label" name="notes">Notes </label>
                                <input class="form-control form-white "
                                placeholder="Enter Value Here" type="text"
                                id="notes-edit" name="notes_edit" />
                            </div>
                            <div class="col-md-4">
                                <label class="control-label" >Project</label>
                                <select class="form-control form-white "
                                    data-placeholder="Choose a color..."
                                    id="project-edit" name="project_edit">
                                    <option selected disabled>__</option>
                                    @foreach ($project as $projects  )
                                    <option value="{{$projects->id}}"  id="project-edit{{$projects->id}}"> {{$projects->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label" >Release_Env</label>
                                <select class="form-control form-white "
                                    data-placeholder="Choose a color..."
                                    id="releaseenv-edit" name="releaseenv_edit">
                                    <option selected disabled>__</option>
                                    @foreach ($env as $envs  )
                                    <option value="{{ $envs->id }}"id="releaseenv-edit{{$envs->id}}">{{$envs->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Issue Number</label>
                                <input class="form-control form-white "
                                    placeholder="Enter URL" type="text"
                                    id="issueurl-edit"  name="issueurl_edit"/>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Priority</label>
                                <select class="form-control form-white "
                                    data-placeholder="Choose a color..."
                                    id="priority-edit" name="priority_edit">
                                    <option selected disabled id="priority-edit">__</option>
                                    @foreach ($priority as $prioritys  )
                                    <option value="{{$prioritys->id }}" id="priority-edit{{$prioritys->id}}">{{$prioritys->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-6 p-2">
                                <label class="control-label" >Attachments</label>
                                <input type="file" class="form-control form-white "
                                    id="file-edit" name="file_edit[]" multiple><br>
                            </div><br>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary" name="save-merge"
                                    id="support_edit_button">Update</button>
                            </div>
                            </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Edit MODEL END --}}
           {{-- Delete MODEL start --}}
    <div class="modal fade" id="DeleteSupport" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                    style="margin-left: 29rem; margin-top: 11px;"></button>
                <div class="modal-body" style=" margin-bottom: 10px;">
                    <input type="hidden" id="deleteSupportId">
                    <h5 class="text-center"> Are you sure want to delete  <span id="user_name" class="text-danger text-center">Support Request</span>?</h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" id="delete_support">Delete</button>
                </div>
            </div>
        </div>
    </div>
              {{-- Delete MODEL END --}}
    </x-app-layout>
    <script src="{{ asset('js/main/support.js') }}"></script>
    <script>
          $(function () {
        var table = $('#support_table').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ajax:"{{ url('fetchSupports') }}",
            // dom: 'Bfrtip',
            // buttons: [
            //  'copy', 'csv', 'excel', 'pdf', 'print'
            //  ],
             initComplete: function(settings, json) {
              $('body').find('.dataTables_scrollBody').addClass("scrollbar");
              },
             columns: [{
                    data: 'id',
                    name: 'id'
                    },
                    {
                    data: 'project.name',
                    name: 'project.name',
                    render:  function (data, type, row) {
                        if ( row.project_id === null ) {
                            return 'Null';
                        }
                        else {
                            return data;
                        }
                    }
                    },
                    {
                    data: 'releaseenv.name',
                    name: 'releaseenv.name',
                    render:  function (data, type, row) {
                        if ( row.releaseenv_id === null ) {
                            return 'Null';
                        }
                        else {
                            return data;
                        }
                    }
                    },
                    {
                    data: 'priority.name',
                    name: 'priority.name',
                    render:  function (data, type, row) {
                        if ( row.priority_id === null ) {
                            return 'Null';
                        }
                        else {
                            return data;
                        }
                    }
                    },
                    {
                    data: 'status.name',
                    name: 'status.name',
                    render:  function (data, type, row) {
                        if(row.status_id === 1) {
                            $supporttest = `<span class="badge badge-warning">${data}</span>`
                            return  $supporttest;
                        }
                        else if(row.status_id === 2) {
                            $prodrequest = `<span class="badge badge-success" style="color:#fff;">${data}</span>`
                            return  $prodrequest;
                        }

                        else if(row.status_id === 3) {
                            $supportmergedone = `<span class="badge badge-testreject ">${data}</span>`
                            return  $supportmergedone;
                        }
                    }
                    },
                    {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true
                    },
                    ]
                    });
                    $('#support_table').on('dblclick', 'tr', function() {
                        if(this.id != 0) {
                            window.location = `supportrequest_view/${this.id}`
                        }
                        else {
                            toastr.options = {
                                "positionClass": "toast-bottom-right",
                            }
                            toastr.error("No Support Request is Created To View");
                        }
                    });
            });
</script>

