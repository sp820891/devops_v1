<x-app-layout :assets="$assets ?? []">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
               <div class="header-title">
                  <h4 class="card-title">Sprints </h4>
               </div>
               @can('can create sprint')


               <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addSprint">
                <i class="btn-inner">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                        viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                    </svg>
                </i>
                <span>Create Sprint</span>
              </button>
              @endcan
            </div>
            <div class="card-body">

               <div class="table-responsive">
                  <table id="sprint_table" class="table table-striped">
                     <thead>
                        <tr>
                           <th>Id</th>
                           <th>Sprint</th>
                           <th>Start date</th>
                           <th>End date</th>
                           <th>Goal</th>
                           <th>Created By</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                  </table>
               </div>
            </div>
         </div>
                {{--  model --}}
                <div class="modal fade" id="addSprint" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header header-title">
                          <h5 class="modal-title card-title" id="exampleModalLabel">Add Sprint</h5>
                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <p>Create a Sprint with a Goal and some date of it.</p>
                            <form>
                                <div class="row">
                                  <div class="col">
                                    <div class="form-group">
                                        <label class="form-label" for="name">Name</label>
                                        <input type="Text" class="form-control" id="name">
                                        <small class="text-danger" id="name_small"></small>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="end_date">End date</label>
                                        <input type="date" class="form-control" id="end_date">
                                        <small class="text-danger" id="end_small"></small>
                                    </div>
                                  </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label class="form-label" for="start_date">Start date</label>
                                            <input type="date" class="form-control" id="start_date">
                                            <small class="text-danger" id="start_small"></small>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="goal">Goal</label>
                                            <input type="text" class="form-control" id="goal">
                                            <small class="text-danger" id="goal_small"></small>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal" id="close">cancel</button>
                                    <button type="button" class="btn btn-primary" id="save-sprint">Save</button>
                                </div>
                            </form>
                        </div>
                      </div>
                    </div>
                  </div>
            {{-- Delete MODEL start --}}
            <div class="modal fade" id="deleteSprint" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                            style="margin-left: 29rem; margin-top: 11px;"></button>
                        <div class="modal-body" style=" margin-bottom: 10px;">
                            <input type="hidden" id="deleteSprintId">
                            <h5 class="text-center"> Are you sure want to delete sprint <br> <span id="delete_name" class="text-danger text-center"></span>?</h5>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-danger" onclick="deleteSprint(this)">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Delete MODEL END --}}
    </x-app-layout>
    <script src="{{asset("js/configuration/sprint.js")}}"></script>
    <script>
        $(function () {
      var table = $('#sprint_table').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          ajax: "{{ url('sprints') }}",
           columns: [{
                  data: 'id',
                  name: 'id'
                  },
                  {
                  data: 'name',
                  name: 'name',
                  },
                  {
                  data: 'start_date',
                  name: 'start_date',
                  },
                  {
                  data: 'end_date',
                  name: 'end_date',
                  },
                  {
                  data: 'goal',
                  name: 'goal',
                  },
                  {
                  data: 'users.full_name',
                  name: 'users.full_name',
                  },
                  {
                  data: 'action',
                  name: 'action',
                  orderable: true,
                  searchable: true
                  },
                  ]
                  });
          });
    </script>
