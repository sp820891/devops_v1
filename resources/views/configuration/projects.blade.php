<x-app-layout :assets="$assets ?? []">
    <div class="card">
        <div class="card-header d-flex justify-content-between">
           <div class="header-title">
              <h4 class="card-title">Projects </h4>
           </div>
           @can('can create a project')
           <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#add-project">

            <i class="btn-inner">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                    viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                </svg>
            </i>
            <span>Create Project</span>
          </button>
          @endcan
        </div>
        <div class="card-body">
           <div class="table-responsive">
              <table id="project_table" class="table table-striped">
                 <thead>
                    <tr>
                       <th>Id</th>
                       <th>Title</th>
                       <th>Description</th>
                       <th>Created By</th>
                       <th>Action</th>
                    </tr>
                 </thead>
                 <tbody>
              </table>
           </div>
        </div>
     </div>
            {{--  model --}}
            <div class="modal fade" id="add-project" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header header-title">
                      <h5 class="modal-title card-title" id="exampleModalLabel">Add Project</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Create a project with a Title and some Desription about it.</p>
                        <form>
                            <div class="form-group">
                                <label class="form-label" for="project_name">Title</label>
                                <input type="Text" class="form-control" id="project_name">
                                <small class="text-danger" id="small_project_name"></small>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="project_des">Description</label>
                                <input type="text" class="form-control" id="project_des">
                                <small class="text-danger" id="small_project_des"></small>
                            </div>
                            <br>
                           <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-bs-dismiss="modal" id="#close">cancel</button>
                                <button type="button" class="btn btn-primary" id="save-project">Save</button>
                           </div>
                        </form>
                    </div>
                  </div>
                </div>
              </div>
              {{-- Delete MODEL start --}}
                <div class="modal fade" id="deleteProject" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                                style="margin-left: 29rem; margin-top: 11px;"></button>
                            <div class="modal-body" style=" margin-bottom: 10px;">
                                <input type="hidden" id="deleteProjectId">
                                <h5 class="text-center"> Are you sure want to delete project <br> <span id="delete_name" class="text-danger text-center"></span>?</h5>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-danger" onclick="deleteProject(this)">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
              {{-- Delete MODEL END --}}
</x-app-layout>
<script src="{{asset("js/configuration/project.js")}}"></script>
<script>
    $(function () {
  var table = $('#project_table').DataTable({
      processing: true,
      serverSide: true,
      autoWidth: false,
      ajax: "{{ url('projects') }}",
       columns: [{
              data: 'id',
              name: 'id'
              },
              {
              data: 'name',
              name: 'name',
              },
              {
              data: 'description',
              name: 'description',
              },
              {
              data: 'users.full_name',
              name: 'users.full_name',
              },
              {
              data: 'action',
              name: 'action',
              orderable: true,
              searchable: true
              },
              ]
              });
      });
</script>
