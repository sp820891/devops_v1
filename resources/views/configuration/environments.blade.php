<x-app-layout :assets="$assets ?? []">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
               <div class="header-title">
                  <h4 class="card-title">Environments </h4>
               </div>
               @can('can create environment')
               <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addEnv">
                <i class="btn-inner">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                        viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                    </svg>
                </i>
                <span> Create Environment</span>

              </button>
              @endcan
            </div>
            <div class="card-body">
                <div class="table-responsive">
                   <table id="env_table" class="table table-striped">
                      <thead>
                         <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>created by</th>
                            <th>action</th>
                         </tr>
                      </thead>
                      <tbody>
                      </tbody>
                   </table>
                </div>
             </div>
         </div>
                {{--  model --}}
                <div class="modal fade" id="addEnv" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header header-title">
                          <h5 class="modal-title card-title" id="exampleModalLabel">Create Envirovement</h5>
                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <p>Create an envirovement with a Name and some Desription about it.</p>
                            <form>
                                <div class="form-group">
                                    <label class="form-label" for="name">Name</label>
                                    <input type="Text" class="form-control" id="name">
                                    <small class="text-danger" id="small_env_name"></small>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="description">Description</label>
                                    <input type="text" class="form-control" id="description">
                                    <small class="text-danger" id="small_env_des"></small>
                                </div>
                                <br>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal" id="close">Cancel</button>
                                    <button type="button" class="btn btn-primary" id="save-env">Save</button>
                                </div>
                            </form>
                        </div>
                      </div>
                    </div>
                  </div>
                {{-- Delete MODEL start --}}
                <div class="modal fade" id="deleteEnv" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                                style="margin-left: 29rem; margin-top: 11px;"></button>
                            <div class="modal-body" style=" margin-bottom: 10px;">
                                <input type="hidden" id="deleteEnvId">
                                <h5 class="text-center"> Are you sure want to delete envirovement <br> <span id="delete_name" class="text-danger text-center"></span>?</h5>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-danger" onclick="deleteEnv(this)">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- Delete MODEL END --}}
    </x-app-layout>
    <script src="{{asset("js/configuration/releaseenv.js")}}"></script>
    <script>
        $(function () {
      var table = $('#env_table').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          ajax: "{{ url('environments') }}",
           columns: [{
                  data: 'id',
                  name: 'id'
                  },
                  {
                  data: 'name',
                  name: 'name',
                  },
                  {
                  data: 'description',
                  name: 'description',
                  },
                  {
                  data: 'users.full_name',
                  name: 'users.full_name',
                  },
                  {
                  data: 'action',
                  name: 'action',
                  orderable: true,
                  searchable: true
                  },
                  ]
                  });
          });
    </script>
