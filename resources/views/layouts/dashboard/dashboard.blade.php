@props(['dir'])
<!DOCTYPE html>
<meta name="csrf-token" content="{{ csrf_token() }}">
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="{{$dir ? 'rtl' : 'ltr'}}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{env('APP_NAME')}}</title>

    @include('partials.dashboard._head')
</head>
<body id="app_mode">
@include('partials.dashboard._body')
@if(session('message'))
{!! session('message') !!}
@endif
</body>
</html>
