<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->string('subject');
            $table->string('status');
            $table->string('description')->nullable();
            $table->string('issue_no');
            $table->foreignId('project_id')->constrained('projects');
            $table->foreignId('sprint_id')->constrained('sprints');
            $table->foreignId('assignee')->constrained('users');
            $table->string('created_by');
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('done')->nullable()->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tickets');
    }
};
