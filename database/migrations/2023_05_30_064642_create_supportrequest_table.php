<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supportrequest', function (Blueprint $table) {
            $table->id();
            $table->string('notes')->nullable();
            $table->foreignId('status_id')->nullable()->constrained('support_request_statuses');
            $table->foreignId('project_id')->nullable()->constrained('projects');
            $table->foreignId('releaseenv_id')->nullable()->constrained('releaseenvs');
            $table->string('issue_url')->nullable();
            $table->string('engineer_report')->nullable();
            $table->foreignId('priority_id')->nullable()->constrained('priorities');
            $table->string('created_by')->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supportrequest');
    }
};
