<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sprint_retrospective', function (Blueprint $table) {
            $table->id();
            $table->foreignId('sprint_id')->constrained('sprints');
            $table->foreignId('user_id')->constrained('users');
            $table->string('pp_rule_id');
            $table->string('power_points');
            $table->string('power_points_breakdown')->nullable();
            $table->string('assessment_score_comments')->nullable();
            $table->string('improvement_score_comments')->nullable();
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sprint_retrospective');
    }
};
