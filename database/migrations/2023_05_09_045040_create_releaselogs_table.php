<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('releaselogs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('mergerequest_id')->constrained('mergerequests');
            $table->foreignId('rejectreason_id')->nullable()->constrained('reject_reasons');
            $table->foreignId('rejected_by')->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('releaselogs');
    }
};
