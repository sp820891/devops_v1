<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_doucuments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('supportrequest_id')->constrained('supportrequest')->onDelete('cascade');
            $table->string('Filepath');
            $table->enum('status',['1','0'])->default('1');
            $table->foreignId('created_by')->constrained('users');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_doucuments');
    }
};
