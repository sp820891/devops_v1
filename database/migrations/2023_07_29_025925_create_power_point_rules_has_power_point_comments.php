<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('power_point_rules_has_pp_comments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pp_rule_id')->constrained('power_point_rules');
            $table->foreignId('pp_Comment_id')->constrained('power_point_comments');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('power_point_rules_has_power_point_comments');
    }
};
