<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('mergerequests', function (Blueprint $table) {
            $table->id();
            $table->foreignId('project_id')->constrained('projects');
            $table->foreignId('user_id')->constrained('users');
            $table->string('git_test_link')->nullable();
            $table->string('git_prod_link')->nullable();
            $table->string('sql_update_link')->nullable();
            $table->string('env_update_link')->nullable();
            $table->foreignId('status_id')->constrained('statuses');
            $table->foreignId('priority_id')->nullable()->constrained('priorities');
            $table->foreignId('releaseenv_id')->constrained('releaseenvs');
            $table->foreignId('sprint_id')->constrained('sprints');
            $table->string('artisan_command')->nullable();
            $table->integer('quality_score')->nullable();
            $table->string('issue_url');
            $table->date('test_release_date')->nullable();
            $table->date('prod_release_date')->nullable();
            $table->integer('test_release_done_count')->default(0);
            $table->integer('prod_release_done_count')->default(0);
            $table->integer('test_release_request_count')->default(0);
            $table->integer('prod_release_request_count')->default(0);
            $table->integer('test_reject_count')->default(0);
            $table->integer('prod_reject_count')->default(0);
            $table->string('quality_report')->nullable();
            $table->string('engineer_report')->nullable();
            $table->foreignId('release_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mergerequests');
    }
};
