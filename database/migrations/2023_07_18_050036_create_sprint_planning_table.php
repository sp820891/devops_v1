<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sprint_planning', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users');
            $table->string('project_name');
            $table->integer('issue_no');
            $table->string('sprint_goals');
            $table->string('day_1');
            $table->string('day_2');
            $table->string('day_3');
            $table->string('day_4');
            $table->string('day_5');
            $table->time('estimated_time');
            $table->time('spend_time');
            $table->string('allocation');
            $table->enum('retrospective',['0','1'])->default(0);
            $table->foreignId('sprint_owner')->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sprint_planning');
    }
};
