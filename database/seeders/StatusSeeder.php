<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::select("INSERT INTO `statuses` (`id`, `name`, `description`, `status`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
        (1, 'Test Release Request', 'Done', '1', 1, '2023-06-02 02:55:52', '2023-06-02 02:55:52', NULL),
        (2, 'Prod Release Request', 'Done', '1', 1, '2023-06-02 02:56:29', '2023-06-02 02:56:29', NULL),
        (3, 'Merge Request Done', 'Done', '1', 1, '2023-06-02 02:56:29', '2023-06-02 02:56:29', NULL),
        (4, 'Test Release Done', 'Done', '1', 1, '2023-06-02 02:54:46', '2023-06-02 02:54:46', NULL),
        (5, 'Prod Release Done', 'Done', '1', 1, '2023-06-02 02:55:17', '2023-06-02 02:55:17', NULL),
        (6, 'Test Release Rejected', 'Done', '1', 1, '2023-06-02 02:55:52', '2023-06-02 02:55:52', NULL),
        (7, 'Prod Release Rejected', 'Done', '1', 1, '2023-06-02 02:55:52', '2023-06-02 02:55:52', NULL)");
    }
}
