<?php

namespace Database\Seeders;

use App\Models\Sprint;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SprintSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sprint = new Sprint();
        $sprint->name = "1";
        $sprint->start_date = fake()->date();
        $sprint->end_date = fake()->date();
        $sprint->goal = "This is a goal";
        $sprint->owner = "1";
        $sprint->status = "1";
        $sprint->save();
    }
}
