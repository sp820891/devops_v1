<?php

namespace Database\Seeders;

use App\Models\Document;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DocumentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $document = new Document();
        $document->mergerequest_id = "1";
        $document->name = "Document Name";
        $document->description = "Description";
        $document->path = "File Path";
        $document->created_by = "1";
        $document->save();
    }
}
