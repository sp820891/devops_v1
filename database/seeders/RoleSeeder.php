<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'DevOps Admin']);
        Role::create(['name' => 'DevOps Engineer']);
        Role::create(['name' => 'DevOps Team']);
        Role::create(['name' => 'DevOps Manager']);
        Role::create(['name' => 'DevOps Developer']);
        Role::create(['name' => 'DevOps QA']);
        Role::create(['name' => 'DevOps Support']);
    }
}
