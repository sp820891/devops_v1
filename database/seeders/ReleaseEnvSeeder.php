<?php

namespace Database\Seeders;

use App\Models\Releaseenv;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ReleaseEnvSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $release_env = new Releaseenv();
        $release_env->name = 'Testing';
        $release_env->description = 'Description';
        $release_env->status = '1';
        $release_env->created_by = '1';
        $release_env->save();
    }
}
