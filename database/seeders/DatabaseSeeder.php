<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //For User Default user Roles and Permissions
        $this->call([
            UserSeeder::class,
            RoleSeeder::class,
            RoleAssignSeeder::class,
            PermissionSeeder::class,
            PermissionAssignSeeder::class,
            StatusSeeder::class,
            supportrequeststatusSeeder::class,
            // Power_pointSeeder::class,
        ]);
        // For Configuration and Merge requests
        // $this->call([
        //     ProjectSeeder::class,
        //     SprintSeeder::class,
        //     ReleaseEnvSeeder::class,
        //     PrioritySeeder::class,
        // ]);
    }
}
