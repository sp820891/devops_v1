<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class supportrequeststatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        DB::select("INSERT INTO `support_request_statuses` (`id`, `name`, `description`, `status`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
        (1, 'InProgress', 'Done', '1', 1, '2023-06-02 02:55:52', '2023-06-02 02:55:52', NULL),
        (2, 'Done', 'Done', '1', 1, '2023-06-02 02:56:29', '2023-06-02 02:56:29', NULL),
        (3, 'Reject', 'Done', '1', 1, '2023-06-02 02:56:29', '2023-06-02 02:56:29', NULL)");
    }
}
