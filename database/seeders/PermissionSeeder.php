<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::select("INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
        (1, 'can access dashboard', 'web', '2023-05-22 00:54:55', '2023-05-22 03:54:55'),
        (2, 'can access merge requests', 'web', '2023-05-24 00:56:57', '2023-05-24 00:56:57'),
        (3, 'can create merge requests', 'web', '2023-05-24 00:58:59', '2023-05-24 00:58:59'),
        (4, 'can view merge requests', 'web', '2023-05-24 01:00:00', '2023-05-24 01:00:00'),
        (5, 'can edit merge requests', 'web', '2023-05-24 01:00:11', '2023-05-24 01:00:11'),
        (6, 'can delete merge requests', 'web', '2023-05-24 01:00:29', '2023-05-24 01:00:29'),
        (7, 'can access support requests', 'web', '2023-05-24 01:03:55', '2023-05-24 01:03:55'),
        (8, 'can create support requests', 'web', '2023-05-24 01:04:55', '2023-05-24 01:04:55'),
        (9, 'can view support requests', 'web', '2023-05-24 01:05:55', '2023-05-24 01:05:55'),
        (10, 'can edit support requests', 'web', '2023-05-24 01:06:11', '2023-05-24 01:06:11'),
        (11, 'can delete support requests', 'web', '2023-05-24 01:07:29', '2023-05-24 01:07:29'),
        (12, 'can access configuration', 'web', '2023-05-22 01:08:04', '2023-05-22 01:08:04'),
        (13, 'can access a project', 'web', '2023-05-22 01:09:52', '2023-05-22 01:09:52'),
        (14, 'can create a project', 'web', '2023-05-22 01:10:55', '2023-05-22 01:10:55'),
        (15, 'can delete a project', 'web', '2023-05-22 01:11:12', '2023-05-22 01:11:12'),
        (16, 'can access sprint', 'web', '2023-05-22 02:12:56', '2023-05-22 02:12:56'),
        (17, 'can create sprint', 'web', '2023-05-22 02:13:29', '2023-05-22 02:13:29'),
        (18, 'can delete sprint', 'web', '2023-05-24 02:14:43', '2023-05-24 02:14:43'),
        (19, 'can access environment', 'web', '2023-05-24 02:15:22', '2023-05-24 02:15:22'),
        (20, 'can create environment', 'web', '2023-05-24 03:41:09', '2023-05-24 03:41:09'),
        (21, 'can delete environment', 'web', '2023-05-24 03:44:31', '2023-05-24 03:44:31'),
        (22, 'can access priority', 'web', '2023-05-24 03:45:07', '2023-05-24 03:45:07'),
        (23, 'can create priority', 'web', '2023-05-24 04:45:30', '2023-05-24 04:45:30'),
        (24, 'can delete priority', 'web', '2023-05-24 04:45:43', '2023-05-24 04:45:43'),
        (25, 'can access status', 'web', '2023-05-24 05:48:11', '2023-05-24 05:48:11'),
        (26, 'can create status', 'web', '2023-05-24 05:48:24', '2023-05-24 05:48:24'),
        (27, 'can delete status', 'web', '2023-05-24 06:48:35', '2023-05-24 06:48:35'),
        (28, 'can access departments', 'web', '2023-05-24 05:48:11', '2023-05-24 05:48:11'),
        (29, 'can create departments', 'web', '2023-05-24 05:48:24', '2023-05-24 05:48:24'),
        (30, 'can delete departments', 'web', '2023-05-24 06:48:35', '2023-05-24 06:48:35'),
        (31, 'can access power point rules', 'web', '2023-05-24 05:48:11', '2023-05-24 05:48:11'),
        (32, 'can create power point rules', 'web', '2023-05-24 05:48:24', '2023-05-24 05:48:24'),
        (33, 'can delete power point rules', 'web', '2023-05-24 06:48:35', '2023-05-24 06:48:35'),
        (34, 'can access power point system', 'web', '2023-05-24 05:48:11', '2023-05-24 05:48:11'),
        (35, 'can access a security', 'web', '2023-05-22 06:57:34', '2023-05-22 06:57:34'),
        (36, 'can access users', 'web', '2023-05-23 07:16:33', '2023-05-23 07:16:33'),
        (37, 'can add users', 'web', '2023-05-23 07:17:09', '2023-05-23 07:17:09'),
        (38, 'can edit users', 'web', '2023-05-23 08:16:51', '2023-05-23 08:16:51'),
        (39, 'can delete users', 'web', '2023-05-23 09:17:29', '2023-05-23 09:17:29'),
        (40, 'can access a roles', 'web', '2023-05-22 10:58:26', '2023-05-22 10:58:26'),
        (41, 'can add roles', 'web', '2023-05-23 10:19:58', '2023-05-23 10:19:58'),
        (42, 'can delete roles', 'web', '2023-05-23 11:20:20', '2023-05-23 11:20:20'),
        (43, 'can access reject reasons', 'web', '2023-05-24 12:48:11', '2023-05-24 12:48:11'),
        (44, 'can create reject reasons', 'web', '2023-05-24 13:48:24', '2023-05-24 13:48:24'),
        (45, 'can delete reject reasons', 'web', '2023-05-24 13:48:35', '2023-05-24 13:48:35'),
        (46, 'can access user-based-permissions', 'web', '2023-05-23 14:58:39', '2023-05-23 14:58:39'),
        (47, 'can access role-based-permissions', 'web', '2023-05-23 15:57:31', '2023-05-23 15:57:31'),
        (48, 'can access logs', 'web', '2023-05-23 16:55:59', '2023-05-23 16:55:59'),
        (49, 'can access sprint retrospective', 'web', '2023-05-23 16:55:59', '2023-05-23 16:55:59'),
        (50, 'can create sprint retrospective', 'web', '2023-05-23 16:55:59', '2023-05-23 16:55:59'),
        (51, 'can view sprint retrospective', 'web', '2023-05-23 16:55:59', '2023-05-23 16:55:59'),
        (52, 'can edit sprint retrospective', 'web', '2023-05-23 16:55:59', '2023-05-23 16:55:59')");
    }
}
