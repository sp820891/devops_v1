<?php

namespace Database\Seeders;

use App\Models\priority;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PrioritySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $priority = new priority;
        $priority->name = 'Medium';
        $priority->description = 'Description';
        $priority->status = '1';
        $priority->created_by = '1';
        $priority->save();
    }
}
