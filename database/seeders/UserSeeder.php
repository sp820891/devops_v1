<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->first_name = "Super";
        $user->last_name = "Admin";
        $user->gender = "Male";
        $user->email = "admin123@devops.com";
        $user->password = Hash::make('admin@123');
        $user->created_by = "SystemDefault";
        $user->save();
    }
}
