//add
$("#support_send").submit(function(e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var formData = new FormData(this);
        console.log(formData);
        $.ajax({
            type: "POST",
            url: "/addSupport",
            data: formData,
            dataType: "json",
            contentType: false,
            cache: false,
            processData: false,
            beforeSend : function () {
                $("#save-merge").html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
                $("#save-merge").attr('disabled', 'disabled');
            },
            success: function (response) {
                $('#addsupport').modal('hide');
                $('#support_table').DataTable().ajax.reload();
                $("#issueurl").val('');
                $("#supportnotes").val('');
                $("#supportpriority").val('');
                $("#supportFile").html("");
                $("#issue_no_small").html("");
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Support Request is Created Successfully");
                $("#save-merge").html('Save');
                $("#save-merge").removeAttr('disabled', 'disabled');
                // Application Notification
                let notify = {
                    notify_id: response.support.id,
                    message: "New Support Request is Created",
                    url_name: "supportrequest_view",
                    created_by: response.support.user.full_name,
                    notification_from: "SupportRequest_Create",
                    created_id: response.created_id,
                };
                $.ajax({
                    type: "get",
                    url: "/notification_channel",
                    data: notify,
                    dataType: "json",
                });
            },
            error: function(error) {
                if(error.status == 422) {
                    $("#issue_no_small").html(error.responseJSON.errors.issue_no);
                    setTimeout(() => {
                        $("#issue_no_small").html("");
                      }, 5000);
                  }
                else {
                    toastr.options = {
                        "positionClass": "toast-bottom-right",
                    }
                    toastr.error("Please Complete Configuration To Create Support Request");
                }
                $("#save-merge").html('Save');
                $("#save-merge").removeAttr('disabled', 'disabled');
            }
        });
    });
//edit
    $(document).on('click', '.editSupport ', function (e) {
        e.preventDefault();
        var id = $(this).attr('id');
        $.ajax({
            url: "/editSupport/" + id,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "json",
            success: function (data) {
                console.log(data);
                $("#edit_support").val(data.support.id);
                $("#notes-edit").val(data.support.notes);
                $("#issueurl-edit").val(data.support.issue_url);
                $.each(data.statuses, function (i, val) {
                    if (data.support.status_id  == val.id) {
                       $("#status-edit"+val.id).prop('selected', true);
                    }
                });
                $.each(data.projects, function (i, val) {
                    if (data.support.project_id  == val.id) {
                       $("#project-edit"+val.id).prop('selected', true);
                    }
                });
                $.each(data.releaseenv, function (i, val) {
                    if (data.support.releaseenv_id == val.id) {
                       $("#releaseenv-edit"+val.id).prop('selected', true);
                    }
                });
                $.each(data.priority, function (i, val) {
                    if (data.support.priority_id == val.id) {
                       $("#priority-edit"+val.id).prop('selected', true);
                    }
                });
                $('#editModel').modal('show');
                $("body").removeAttr("style");
            },
            error: function (data) {
                var errors = data.responseJSON;
                console.log(errors)
            }
        });
    });
 //update
 $('#updated-support').submit(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var formData = new FormData(this);
    var id = $('#edit_support').val();
    $.ajax({
        type: "post",
        url: "/updateSupport/"+id,
        data: formData,
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        beforeSend : function () {
            $("#support_edit_button").html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $("#support_edit_button").attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#editModel').modal('hide');
            $('#support_table').DataTable().ajax.reload();
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("Support Request is Updated Successfully");
            $("#support_edit_button").html('Update');
            $("#support_edit_button").removeAttr('disabled', 'disabled');
             // Application Notification
             let notify = {
                notify_id: response.support.id,
                message: "Support Request is Edited",
                url_name: "supportrequest_view",
                created_by: response.support.user.full_name,
                notification_from: "SupportRequest_Edit",
                created_id: response.created_id,
            };
            $.ajax({
                type: "get",
                url: "/notification_channel",
                data: notify,
                dataType: "json",
            });
        },
    });
});
// For Delete
$(document).on('click', '.deleteSupport ', function (e) {
    e.preventDefault();
    var id = $(this).attr('id');
    $.ajax({
        url: "/editSupport/" + id,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: "json",
        success: function (data) {
            $('#DeleteSupport').modal('show');
            $("body").removeAttr("style");
            $("#deleteSupportId").val(id);
        },
    });
});
$('#delete_support').click(function (e) {
    e.preventDefault();
    var id = $("#deleteSupportId").val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "post",
        url: "/deleteSupport/"+id,
        beforeSend : function () {
            $(e.target).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e.target).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#DeleteSupport').modal('hide');
            $('#support_table').DataTable().ajax.reload();
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("Support Request is Deleted Successfully");
            $(e.target).html('Delete');
            $(e.target).removeAttr('disabled', 'disabled');
        }
    });
});
$('#close').click(function (e) {
    e.preventDefault();
    $("#issueurl").val('');
    $("#supportnotes").val('');
});
$('.btn-close').click(function (e) {
    e.preventDefault();
    $("#issueurl").val('');
    $("#supportnotes").val('');
});

$("#approve_view_support").submit(function (e) {
    e.preventDefault();
    var id = $("#viewsupport_approve_id").val();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "post",
        url: "/supportApprove/" + id,
        cache: false,
        processData: false,
        success: function (response) {
            $('#SupportApprove').modal('hide');
            $('#refresh_support').load(' #refresh_support');
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("Support Request is Approved By DevOps Engineer");
             // Application Notification
             let notify = {
                notify_id: response.support.id,
                message: "Support Request is Approved By DevOps Engineer",
                url_name: "supportrequest_view",
                created_by: response.approved_by,
                notification_from: "SupportRequest_Engineer",
                created_id: response.created_id,
            };
            $.ajax({
                type: "get",
                url: "/notification_channel",
                data: notify,
                dataType: "json",
            });
        }
    });
});
// support Reject
$("#SupportReject_form").submit(function (e) {
    e.preventDefault();
    var id = $("#support_reject").val();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "post",
        url: "/supportReject/" + id,
        cache: false,
        processData: false,
        success: function (response) {
            $('#SupportReject').modal('hide');
            $('#refresh_support').load(' #refresh_support');
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("Support Request is Rejected By DevOps Engineer");
             // Application Notification
             let notify = {
                notify_id: response.support.id,
                message: "Support Request is Rejected By DevOps Engineer",
                url_name: "supportrequest_view",
                created_by: response.rejected_by,
                notification_from: "SupportRequest_Engineer",
                created_id: response.created_id,
            };
            $.ajax({
                type: "get",
                url: "/notification_channel",
                data: notify,
                dataType: "json",
            });
        }
    });
});
//Supportdocument delete
$(".support_docs_delete").hover(function () {
    $('.hover_support_docs').addClass('hover_support_docs_del');
 }, function () {
     $('.hover_support_docs').removeClass('hover_support_docs_del');
 }
);
// //deleted_document
// $('#document_deleted').click(function (e) {
//     e.preventDefault();
//     var id = $("#supportDocsID").val();
//     $.ajax({
//         type: "get",
//         url: "/docdeleted/"+id,
//         success: function (response) {
//             $('#refresh_support').load(' #refresh_support');
//             toastr.options = {
//                 "positionClass": "toast-bottom-right",
//             }
//             toastr.success("Support Request documentation  successfully deleted");
//         }
//     });
// });
