// Create Mergerequestname
$("#save_merge").submit(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var formData = new FormData(this);
    $.ajax({
        type: "post",
        url: "/addmerge",
        data: formData,
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function () {
            $("#save_merge_button").html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $("#save_merge_button").attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#add_merge').modal('hide');
            $('#merge_table').DataTable().ajax.reload();
            $("#email").val('');
            $("#git_test_link").val('');
            $("#git_prod_link").val('');
            $("#sql_update_link").val('');
            $("#env_update_link").val('');
            $('#merge_priority').val('');
            $('#artisan').val('');
            $('#issue_url').val('');
            $('#release_env').val('');
            $('#attach').val('');
            $('#merge-table').html("");
            $("#issue_no_small_merge").html("");
            $("#mergeFile").html("");
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("Merge Request is Created Successfully");
            $("#save_merge_button").html('Save');
            $("#save_merge_button").removeAttr('disabled', 'disabled');
             // Application Notification
             let notify = {
                notify_id: response.merge.id,
                message: "New Merge Request is Created",
                url_name: "mergerequest_view",
                created_by: response.merge.user.full_name,
                notification_from: "MergeRequest_Create",
                created_id: response.created_id,
            };
            $.ajax({
                type: "get",
                url: "/notification_channel",
                data: notify,
                dataType: "json",
            });
        },
        error: function (error) {
            if (error.status == 422) {
                $("#issue_no_small_merge").html(error.responseJSON.errors.issue_no);
                setTimeout(() => {
                    $("#issue_no_small_merge").html("");
                }, 5000);
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Please Complete Configuration To Create Merge Request");
            }
            $("#save_merge_button").html('Save');
            $("#save_merge_button").removeAttr('disabled', 'disabled');
        }
    });
});
//edit merge
$(document).on('click', '.edit ', function (e) {
    e.preventDefault();
    var id = $(this).attr('id');
    // alert(id);
    $.ajax({
        url: "/editMerge/" + id,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: "json",
        success: function (response) {
            //console.log('success:' + response.merge.issue_url);
            if(response.merge.status_id == 3) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("You Cannot Edit Merge Request Till Status is Merge Done");
            } else if(response.merge.status_id == 1) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("You Cannot Edit Merge Request Till Status is Test Release Request");
            }else if(response.merge.status_id == 1) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("You Cannot Edit Merge Request Till Status is Test Release Request");
            }else if(response.merge.status_id == 2) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("You Cannot Edit Merge Request Till Status is Prod Release Request");
            }
            else {
                $("#issue_url_edit").val(response.merge.issue_url);
                $("#status").val(response.merge.status_id);
                $("#quality_score_edit").val(response.merge.quality_score);
                $.each(response.statuses, function (i, val) {
                    if (response.merge.status_id == val.id) {
                        console.log(val.id);
                        $("#status_option" + val.id).prop('selected', true);
                    }
                });
                $.each(response.priorities, function (i, val) {
                    if (response.merge.priority_id == val.id) {
                        $("#priority_option" + val.id).prop('selected', true);
                    }
                });
                $.each(response.releaseenvs, function (i, val) {
                    if (response.merge.releaseenv_id == val.id) {
                        $("#env_option" + val.id).prop('selected', true);
                    }
                });
                $("#artisan_edit").val(response.merge.artisan_command);
                $("#git_test_link_edit").val(response.merge.git_test_link);
                $("#git_prod_link").val(response.merge.git_prod_link);
                $('#editUsers').val(id);
                $('#edit_merge').modal('show');
                $("body").removeAttr("style");
            }
        },
        error: function (response) {
            var errors = response.responseJSON;
            console.log(errors)
        }
    });
});

//update merge
$("#update_merge").submit(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var formData = new FormData(this);
    var id = $("#editUsers").val();
    $.ajax({
        type: "post",
        url: "/updateMerge/" + id,
        data: formData,
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function () {
            $("#edit_merge_button").html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $("#edit_merge_button").attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#edit_merge').modal('hide');
            $('#merge_table').DataTable().ajax.reload();
            $('#merge_priority').val('');
            $('#artisan').val('');
            $('#issue_url').val('');
            $('#release_env').val('');
            $('#attach').val('');
            $('#merge-table').html("");
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("Merge Request is Updated Successfully");
            $("#edit_merge_button").html('Update');
            $("#edit_merge_button").removeAttr('disabled', 'disabled');
             // Application Notification
             let notify = {
                notify_id: response.merge.id,
                message: "Merge Request is Edited",
                url_name: "mergerequest_view",
                created_by: response.merge.user.full_name,
                notification_from: "MergeRequest_Edit",
                created_id: response.created_id,
            };
            $.ajax({
                type: "get",
                url: "/notification_channel",
                data: notify,
                dataType: "json",
            });
        }
    });
});

//Delete Merge

$(document).on('click', '.delete ', function (e) {
    e.preventDefault();
    var id = $(this).attr('id');
    $.ajax({
        type: "get",
        url: "/editMerge/" + id,
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        success: function (response) {
            $('#deleteUsers').val(id);
            $('#delete_merge_model').modal('show');
            $("body").removeAttr("style");
        }
    });
});


$('#delete_merge').click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var id = $("#deleteUsers").val();
    $.ajax({
        type: "post",
        url: "/deleteMerge/" + id,
        beforeSend: function () {
            $(e.target).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e.target).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#delete_merge_model').modal('hide');
            $('#merge_table').DataTable().ajax.reload();
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("Merge Request is Deleted Successfully");
            $(e.target).html('Delete');
            $(e.target).removeAttr('disabled', 'disabled');
        }
    });
});
$('#close').click(function (e) {
    e.preventDefault();
    $('#artisan').val('');
    $('#issue_url').val('');
});
$('.btn-close').click(function (e) {
    e.preventDefault();
    $('#artisan').val('');
    $('#issue_url').val('');
});


// approve Qa test start //
$("#approve_view_qa").submit(function (e) {
    e.preventDefault();
    var id = $("#viewqa_approve_id").val();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "post",
        url: "/QaApprove/" + id,
        cache: false,
        processData: false,
        success: function (response) {
            $('#QaApprove').modal('hide');
            $('#testApprovebutton').hide();
            $('#refresh').load(' #refresh')

            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("Merge Request is Approved By DevOps QA");
            // Application Notification
            let notify = {
                notify_id: response.merge.id,
                message: "Merge Request is Approved By DevOps QA",
                url_name: "mergerequest_view",
                created_by: response.approved_by,
                notification_from: "MergeRequest_QA",
                created_id: response.created_id,
            };
            $.ajax({
                type: "get",
                url: "/notification_channel",
                data: notify,
                dataType: "json",
            });
        }
    });

});
// approve Qa test end //

// Merge approve start //
$("#approve_view_merge").submit(function (e) {
    e.preventDefault();
    var formData = new FormData(this);
    var id = $("#viewmerge_approve_id").val();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "post",
        url: "/mergedone/" + id,
        data: formData,
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        success: function (response) {
            $("#quality_score").val(response.merge.quality_score);
            $('#mergeApprove').modal('hide');
            $('#mergedonebutton').hide();
            $('#refresh').load(' #refresh')
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("Merge Request is Merged To Test Branch");
             // Application Notification
             let notify = {
                notify_id: response.merge.id,
                message: "Merge Request is Merged To Test Branch",
                url_name: "mergerequest_view",
                created_by: response.approved_by,
                notification_from: "MergeRequest_Engineer",
                created_id: response.created_id,
            };
            $.ajax({
                type: "get",
                url: "/notification_channel",
                data: notify,
                dataType: "json",
            });
        }
    });
});
// Merge approve end //

// production approve start //
$("#ProdApprove_form").submit(function (e) {
    e.preventDefault();
    var id = $("#viewprod_approv_id").val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "post",
        url: "/mergedone/" + id,
        cache: false,
        processData: false,
        success: function (response) {
            $('#ProdApprove').modal('hide');
            $('#prodApprovebutton').hide();
            $('#refresh').load(' #refresh')
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("Production Released Successfully");
             // Application Notification
             let notify = {
                notify_id: response.merge.id,
                message: "Merge Request is Approved By DevOps Engineer",
                url_name: "mergerequest_view",
                created_by: response.approved_by,
                notification_from: "MergeRequest_Engineer",
                created_id: response.created_id,
            };
            $.ajax({
                type: "get",
                url: "/notification_channel",
                data: notify,
                dataType: "json",
            });
        }
    });
});
// production approve End //



// $(document).on('click', '.QaReject ', function (e) {
//     e.preventDefault();
//     var id = $(this).attr('id');
//     $.ajax({
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         },
//         type: "post",
//         url: "/QaReject/" + id,
//         cache: false,
//         processData: false,
//         success: function (response) {
//             $('#merge_table').DataTable().ajax.reload();
//             toastr.options = {
//                 "positionClass": "toast-bottom-right",
//             }
//             toastr.success("Merge Request is Rejected Successfully");
//         }
//     });

// });

$("#QaReject_form").submit(function (e) {
    e.preventDefault();
    var formData = new FormData(this);
    var id = $("#QaReject_id").val();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "post",
        url: "/QaReject/" + id,
        data: formData,
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        success: function (response) {
            $('#QaReject').modal('hide');
            $('#testApprovebutton').hide();
            $('#refresh').load(' #refresh')
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("Merge Request is Rejected By DevOps QA");
              // Application Notification
              let notify = {
                notify_id: response.merge.id,
                message: "Merge Request is Rejected By DevOps QA",
                url_name: "mergerequest_view",
                created_by: response.rejected_by,
                notification_from: "MergeRequest_QA",
                created_id: response.created_id,
            };
            $.ajax({
                type: "get",
                url: "/notification_channel",
                data: notify,
                dataType: "json",
            });

        }
    });
});

$("#MergeReject_form").submit(function (e) {
    e.preventDefault();
    var formData = new FormData(this);
    var id = $("#MergeReject_id").val();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "post",
        url: "/MergeReject/" + id,
        data: formData,
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        success: function (response) {
            $('#mergereject').modal('hide');
            $('#mergedonebutton').hide();
            $('#refresh').load(' #refresh')
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("Merge Request is Rejected DevOps Team");
            // Application Notification
            let notify = {
            notify_id: response.merge.id,
            message: "Merge Request is Rejected By DevOps Team",
            url_name: "mergerequest_view",
            created_by: response.rejected_by,
            notification_from: "MergeRequest_Engineer",
            created_id: response.created_id,
            };
            $.ajax({
                type: "get",
                url: "/notification_channel",
                data: notify,
                dataType: "json",
            });

        }
    });
});

$("#prodReject_form").submit(function (e) {
    e.preventDefault();
    var formData = new FormData(this);
    var id = $("#ProdReject_id").val();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "post",
        url: "/MergeReject/" + id,
        data: formData,
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        success: function (response) {
            $('#prodreject').modal('hide');
            $('#prodApprovebutton').hide();
            $('#refresh').load(' #refresh')
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("Merge Request is Rejected By DevOps Engineer");
            // Application Notification
            let notify = {
            notify_id: response.merge.id,
            message: "Merge Request is Rejected By DevOps Engineer",
            url_name: "mergerequest_view",
            created_by: response.rejected_by,
            notification_from: "MergeRequest_Engineer",
            created_id: response.created_id,
            };
            $.ajax({
                type: "get",
                url: "/notification_channel",
                data: notify,
                dataType: "json",
            });

        }
    });
});

// comment-box
$(document).ready(function () {

    $('#open').click(function () {

        $("#fullScreen").removeClass('col-lg-12').addClass('col-lg-9');
        $('#chat').removeClass('comment-boxx');
        $('#open').hide();
    });

    $('#close').click(function () {

        $("#fullScreen").removeClass('col-lg-9').addClass('col-lg-12');
        $('#chat').addClass('comment-boxx');
        $('#open').show();

    });
})

var fetchComment = () => {
    var id = $('#merge_id').val();
    var name = $('#user_name').val();
    $.ajax({
        type: "get",
        url: "/showComment/" + id,
        success: function (response) {
            $.each(response.comment, function (i, val) {
                var dt = new Date(val.created_at);
                var dd = dt.toDateString() + " " + dt.getHours() + ":" + dt.getMinutes();
                if (response.auth == val.users.first_name) {
                    var html = `<div class="d-flex"><h6 style="color: #3a57e8";>You&nbsp</h6><small>${dd}</small></div><p class="text-wrap" style="color: rgb(0, 0, 0);">${val.comment}</p>`;
                    //var html = `<p class="col-lg-6">You</p>`
                }
                else {
                    var html = `<div class="d-flex"><h6 style="color:#3a57e8";>${val.users.first_name}&nbsp</h6><small>${dd}</small></div><p class="text-wrap" style="color: rgb(0, 0, 0);">${val.comment}</p> `;
                }
                $("#comments_view").append(html);
            });
        }

    });
}
fetchComment();

$('#up-comment').click(function (e) {
    e.preventDefault();
    if ($("#casual_comment").val() == "") {
        toastr.options = {
            "positionClass": "toast-bottom-right",
        }
        toastr.error("Please Enter Comment");
    }
    else {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var data = {
            comment: $("#casual_comment").val(),
        };
        var id = $('#merge_id').val();
        $.ajax({
            type: "post",
            url: "/updateComment/" + id,
            data: data,
            dataType: "json",
            success: function (response) {
                $("#comments_view").html("");
                fetchComment();
                $("#casual_comment").val("");

            },
        });
    }
})
