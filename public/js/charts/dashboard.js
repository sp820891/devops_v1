(function (jQuery) {
  "use strict";
// Release Request
$.ajax({
    type: "get",
    url: "fetchDashboardReleaseRequests",
    success: function (response) {
        if (document.querySelectorAll('#myChart').length) {
            const options = {
              series: [response.test_release , response.prod_release],
              chart: {
              height: 270,
              type: 'radialBar',
            },
            colors: ["#3a57e8","#4bc7d2"],
            plotOptions: {
              radialBar: {
                hollow: {
                    margin: 10,
                    size: "50%",
                },
                track: {
                    margin: 10,
                    strokeWidth: '50%',
                },
                dataLabels: {
                    show: false,
                }
              }
            },
            };
            if(ApexCharts !== undefined) {
              const chart = new ApexCharts(document.querySelector("#myChart"), options);
              chart.render();
              document.addEventListener('ColorChange', (e) => {
                  const newOpt = {colors: [e.detail.detail2, e.detail.detail1],}
                  chart.updateOptions(newOpt)

              })
            }
          }
    }
});
// Support Request
$.ajax({
    type: "get",
    url: "/fetchDashboardSupportRequests",
    success: function (response) {
        let supportrequest= [0,0,0,0,0,0,0,0,0,0,0,0];
        $.each(response.supportrequest_count, function (i, val) {
            supportrequest[val.month-1] = val.count;
        });
        if (document.querySelectorAll('#d-activity').length) {
            const options = {
              series: [{
                name: 'Support Request Count',
                data: supportrequest
              }],
              chart: {
                type: 'bar',
                height: 230,
                stacked: true,
                toolbar: {
                    show:false
                  }
              },
              colors: ["#4bc7d2"],
              plotOptions: {
                bar: {
                  horizontal: false,
                  columnWidth: '28%',
                  endingShape: 'rounded',
                  borderRadius: 5,
                },
              },
              legend: {
                show: false
              },
              dataLabels: {
                enabled: false
              },
              stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
              },
              xaxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct','Nov','Dec'],
                labels: {
                  minHeight:20,
                  maxHeight:40,
                  style: {
                    colors: "#8A92A6",
                  },
                }
              },
              yaxis: {
                title: {
                  text: ''
                },
                labels: {
                    minWizth: 19,
                    maxWidth: 19,
                    style: {
                      colors: "#8A92A6",
                    },
                }
              },
              fill: {
                opacity: 1
              },
              tooltip: {
                enabled: true,
              }
            };

            const chart = new ApexCharts(document.querySelector("#d-activity"), options);
            chart.render();
            document.addEventListener('ColorChange', (e) => {
            const newOpt = {colors: [e.detail.detail1, e.detail.detail2],}
            chart.updateOptions(newOpt)
            })
          }
    }
});
// Mergerequests
$.ajax({
    type: "get",
    url: "fetchDasboardMergeRequests",
    success: function (response) {
        let test_release = [0,0,0,0,0,0,0,0,0,0,0,0];
        let prod_release = [0,0,0,0,0,0,0,0,0,0,0,0];
        $.each(response.test_release, function (i, val) {
            test_release[val.month-1] = val.count;
        });
        $.each(response.prod_release, function (i, val) {
            prod_release[val.month-1] = val.count;
        });
        if (document.querySelectorAll('#d-main').length) {
            const options = {
                series: [{
                    name: 'Test Release',
                    data: test_release
                }, {
                    name: 'Production Release',
                    data: prod_release
                }],
                chart: {
                    fontFamily: '"Inter", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"',
                    height: 250,
                    type: 'bar',
                    toolbar: {
                        show: false
                    },
                    sparkline: {
                        enabled: false,
                    },
                },
                colors: ["#3a57e8", "#4bc7d2"],
                plotOptions: {
                    bar: {
                      columnWidth: '45%',
                    },
                  },
                dataLabels: {
                    enabled: false
                },
                stroke: {
                    width: 3,
                    show: true,
                },
                yaxis: {
                  show: true,
                  labels: {
                    show: true,
                    minWidth: 19,
                    maxWidth: 19,
                    style: {
                      colors: "#8A92A6",
                    },
                    offsetX: -5,
                  },
                },
                legend: {
                    show: false,
                },
                xaxis: {
                    labels: {
                        minHeight:22,
                        maxHeight:32,
                        show: true,
                        style: {
                          colors: "#8A92A6",
                        },
                    },
                    lines: {
                        show: false  //or just here to disable only x axis grids
                    },
                    categories: ["Jan", "Feb", "Mar", "Apr", "Jun", "Jul", "Aug","Sep","Oct","Nov","Dec"]
                },
                grid: {
                    show: true,
                },
                fill: {
                    opacity:1,
                },
                tooltip: {
                  enabled: true,
                },
            };

            const chart = new ApexCharts(document.querySelector("#d-main"), options);
            chart.render();
            document.addEventListener('ColorChange', (e) => {
              console.log(e)
              const newOpt = {
                colors: [e.detail.detail1, e.detail.detail2],
                fill: {
                  type: 'gradient',
                  gradient: {
                      shade: 'dark',
                      type: "vertical",
                      shadeIntensity: 0,
                      gradientToColors: [e.detail.detail1, e.detail.detail2], // optional, if not defined - uses the shades of same color in series
                      inverseColors: true,
                      opacityFrom: .4,
                      opacityTo: .1,
                      stops: [0, 50, 60],
                      colors: [e.detail.detail1, e.detail.detail2],
                  }
              },
             }
              chart.updateOptions(newOpt)
            })
          }
    }
});
// Counts Bar
if ($('.d-slider1').length > 0) {
    const options = {
        centeredSlides: false,
        loop: false,
        slidesPerView: 4,
        autoplay:true,
        spaceBetween: 32,
        breakpoints: {
            320: { slidesPerView: 1 },
            550: { slidesPerView: 2 },
            991: { slidesPerView: 3 },
            1400: { slidesPerView: 3 },
            1500: { slidesPerView: 4 },
            1700: { slidesPerView: 6 },
            2040: { slidesPerView: 7 },
            2440: { slidesPerView: 8 }
        },
        pagination: {
            el: '.swiper-pagination'
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        },

        // And if we need scrollbar
        scrollbar: {
            el: '.swiper-scrollbar'
        }
    }
    let swiper = new Swiper('.d-slider1',options);

    document.addEventListener('ChangeMode', (e) => {
      if (e.detail.rtl === 'rtl' || e.detail.rtl === 'ltr') {
        swiper.destroy(true, true)
        setTimeout(() => {
            swiper = new Swiper('.d-slider1',options);
        }, 500);
      }
    })
}

})(jQuery)
