$(document).ready(function () {
    $('#saveticket').click(function (e) {
        e.preventDefault();
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/ticket-store',
            type: "post",
            dataType: "json",
            data: $('#ticketForm').serialize(),
            beforeSend : function () {
                $(e.target).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
                $(e.target).attr('disabled', 'disabled');
            },
            success: function (response) {
                $('#ticketModel').modal('hide');
                $('#ticket-table').DataTable().ajax.reload();
                $('#ticketForm')[0].reset();
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("User is Added Successfully");
                $(e.target).html('Save');
                $(e.target).removeAttr('disabled', 'disabled');
            },
            error: function (error) {
                $(e.target).html('Save');
                $(e.target).removeAttr('disabled', 'disabled');
            }

        });
    });

// MODEL CLOSE RESET DATA START

    $('#close').click(function (e) {
        e.preventDefault();
        $('#userForm')[0].reset();
    })

    $('#cancel').click(function (e) {
        e.preventDefault();
        $('#userForm')[0].reset();
    })
// MODEL CLOSE RESET DATA END


$('#submit-ticket').click(function (e) {
    alert('success');
    e.preventDefault();
    $.ajax({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: '/update-ticket',
        type: "post",
        dataType: "json",
        data: {
                id:$('#id').val(),
                sprint:$('#sprint').val(),
                status:$('#status').val(),
                assignee:$('#assignee').val(),
                estimate_time:$('#estimate_time').val(),
                spend_time:$('#spend_time').val(),
                start_date:$('#start_date').val(),
                end_date:$('#end_date').val(),
                done:$('#done').val(),
        },
        // beforeSend : function () {
        //     $(e.target).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
        //     $(e.target).attr('disabled', 'disabled');
        // },
        success: function (response) {
            $('#userModel').modal('hide');
            // $('#user-table').DataTable().ajax.reload();
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("User is Added Successfully");
            //  $(e.target).html('Save');
            // $(e.target).removeAttr('disabled', 'disabled');
            // Application Notification
            // let notify = {
            //     notify_id: response.user.id,
            //     message: "New User is Created",
            //     url_name: "user_details",
            //     created_by: response.user.created_by,
            //     created_at: response.user.created_at
            // };
            // $.ajax({
            //     type: "get",
            //     url: "/notification_channel",
            //     data: notify,
            //     dataType: "json",
            // });
        },
        // error: function (error) {
        //     $('#fname_small').html(error.responseJSON.errors.first_name);
        //     $('#lname_small').html(error.responseJSON.errors.last_name);
        //     $('#gender_small').html(error.responseJSON.errors.gender);
        //     $('#role_small').html(error.responseJSON.errors.role_as);
        //     $('#password_small').html(error.responseJSON.errors.password);
        //     $('#email_small').html(error.responseJSON.errors.email);
        //     setTimeout(() => {
        //         $('#fname_small').html('');
        //         $('#lname_small').html('');
        //         $('#gender_small').html('');
        //         $('#role_small').html('');
        //         $('#password_small').html('');
        //         $('#email_small').html('');
        //     }, 5000);
        //     $(e.target).html('Save');
        //     $(e.target).removeAttr('disabled', 'disabled');
        // }

    });
});



});

$('#cancel').click(function (e) {
    e.preventDefault();
    $('#userForm')[0].reset();
})
