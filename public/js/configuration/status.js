// For Merge Request Status
$(document).on('click', '.deleteStatus ', function (e) {
    e.preventDefault();
    var id = $(this).attr('id');
    $.ajax({
        url: "/deleteStatusId/" + id,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: "json",
        success: function (data) {
            $('#deleteStatus').modal('show');
            $("body").removeAttr("style");
            $("#deleteStatusId").val(id);
            $("#delete_name").html(data.statuses.name);
        },
    });
});
// for deleting the status
let deleteStatus = (e) => {
    var id = $('#deleteStatusId').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "delete",
        url: "delete-status/" + id,
        beforeSend : function () {
            $(e).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#deleteStatus').modal('hide');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Status For Merge Request is Deleted Successfully");
                $('#status_table').DataTable().ajax.reload();
            }
            $(e).html('Delete');
            $(e).removeAttr('disabled', 'disabled');
        }
    });
}
// for storing the status
$('#save-status').click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = {
        status_name: $('#name').val(),
        status_description: $('#description').val(),
    }
    $.ajax({
        type: "post",
        url: "/store-status",
        data: data,
        dataType: "json",
        beforeSend : function () {
            $(e.target).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e.target).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#addStatus').modal('hide');
            $('#name').val(''),
            $('#description').val(''),
            $("#small_status_name").html("");
            $("#small_status_des").html("");
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Status For Merge Request is Created Successfully");
                $('#status_table').DataTable().ajax.reload();
            }
            $(e.target).html('Save');
            $(e.target).removeAttr('disabled', 'disabled');
        },
        error: function(error) {
            $("#small_status_name").html(error.responseJSON.errors.status_name);
            $("#small_status_des").html(error.responseJSON.errors.status_description);
            setTimeout(() => {
                $("#small_status_name").html("");
                $("#small_status_des").html("");
            }, 5000);
            $(e.target).html('Save');
            $(e.target).removeAttr('disabled', 'disabled');
        }
    });
});
$('#close').click(function (e) {
    e.preventDefault();
    $('#name').val(''),
    $('#description').val(''),
    $("#small_status_name").html("");
    $("#small_status_des").html("");
});
$('.btn-close').click(function (e) {
    e.preventDefault();
    $('#name').val(''),
    $('#description').val(''),
    $("#small_status_name").html("");
    $("#small_status_des").html("");
});
// For Support Request Status
$(document).on('click', '.deleteSupportStatus ', function (e) {
    e.preventDefault();
    var id = $(this).attr('id');
    $.ajax({
        url: "/deleteSupportStatusId/" + id,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: "json",
        success: function (data) {
            $('#deleteSupportStatus').modal('show');
            $("body").removeAttr("style");
            $("#deleteSupportStatusId").val(id);
            $("#delete_support_name").html(data.statuses.name);
        },
    });
});
// for deleting the status
let deleteSupportStatus = (e) => {
    var id = $('#deleteSupportStatusId').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "delete",
        url: "delete-support-status/" + id,
        beforeSend : function () {
            $(e).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#deleteSupportStatus').modal('hide');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Status For Support Request is Deleted Successfully");
                $('#Support_status_table').DataTable().ajax.reload();
            }
            $(e).html('Delete');
            $(e).removeAttr('disabled', 'disabled');
        }
    });
}
// for storing the status
$('#save-support-status').click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = {
        status_name: $('#support_name').val(),
        status_description: $('#support_description').val(),
    }
    $.ajax({
        type: "post",
        url: "/store-support-status",
        data: data,
        dataType: "json",
        beforeSend : function () {
            $(e.target).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e.target).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#addSupportStatus').modal('hide');
            $('#support_name').val(''),
            $('#support_description').val(''),
            $("#small_support_status_name").html("");
            $("#small_support_status_des").html("");
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Status For Support Request is Created Successfully");
                $('#Support_status_table').DataTable().ajax.reload();
            }
            $(e.target).html('Save');
            $(e.target).removeAttr('disabled', 'disabled');
        },
        error: function(error) {
            $("#small_support_status_name").html(error.responseJSON.errors.status_name);
            $("#small_support_status_des").html(error.responseJSON.errors.status_description);
            setTimeout(() => {
                $("#small_support_status_name").html("");
                $("#small_support_status_des").html("");
            }, 5000);
            $(e.target).html('Save');
            $(e.target).removeAttr('disabled', 'disabled');
        }
    });
});
$('#close-support').click(function (e) {
    e.preventDefault();
    $('#support_name').val('');
    $('#support_description').val('');
    $("#small_support_status_name").html("");
    $("#small_support_status_des").html("");
});
$('.btn-close-support').click(function (e) {
    e.preventDefault();
    $('#support_name').val('');
    $('#support_description').val('');
    $("#small_support_status_name").html("");
    $("#small_support_status_des").html("");
});
