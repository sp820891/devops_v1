$(document).on('click', '.deleteDept ', function (e) {
    e.preventDefault();
    var id = $(this).attr('id');
    $.ajax({
        url: "/deleteDeptId/" + id,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: "json",
        success: function (data) {
            $('#deleteDept').modal('show');
            $("body").removeAttr("style");
            $("#deleteDeptId").val(id);
            $("#delete_dept_name").html(data.dept.name);
        },
    });
});
// for deleting the dept
let deleteDept = (e) => {
    var id = $('#deleteDeptId').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "delete",
        url: "/delete-dept/" + id,
        beforeSend : function () {
            $(e).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#deleteDept').modal('hide');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Department is Deleted Successfully");
                $('#departments_table').DataTable().ajax.reload();
            }
            $(e).html('Delete');
            $(e).removeAttr('disabled', 'disabled');
        }
    });
}
// for storing the dept
$('#save-dept').click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = {
        department_name: $('#name').val(),
        role : $("#role").val(),
    }
    $.ajax({
        type: "post",
        url: "/store-dept",
        data: data,
        dataType: "json",
        beforeSend : function () {
            $(e.target).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e.target).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#addDept').modal('hide');
            $('#name').val(''),
            $('#role').val(''),
            $("#small_dept_name").html("");
            $("#role_small").html("");
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-botton-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-botton-right",
                }
                toastr.success("Department is Created Successfully");
                $('#departments_table').DataTable().ajax.reload();
            }
            $(e.target).html('Save');
            $(e.target).removeAttr('disabled', 'disabled');
        },
        error: function(error) {
            $("#small_dept_name").html(error.responseJSON.errors.department_name);
            $("#role_small").html(error.responseJSON.errors.role);
            setTimeout(() => {
                $("#small_dept_name").html("");
                $("#role_small").html("");
            }, 5000);
            $(e.target).html('Save');
            $(e.target).removeAttr('disabled', 'disabled');
        }
    });
});
$('#close').click(function (e) {
    e.preventDefault();
    $('#name').val(''),
    $("#small_dept_name").html("");
    $("#role_small").html("");
});
$('.btn-close').click(function (e) {
    e.preventDefault();
    $('#name').val(''),
    $("#small_dept_name").html("");
    $("#role_small").html("");
});
