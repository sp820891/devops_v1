$(document).on('click', '.deleteRejectReason ', function (e) {
    e.preventDefault();
    var id = $(this).attr('id');
    $.ajax({
        url: "/deleteRejectReasonId/" + id,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: "json",
        success: function (data) {
            $('#deleteRejectReason').modal('show');
            $("body").removeAttr("style");
            $("#deleteRejectReasonId").val(id);
            $("#delete_name").html(data.reject_reason.name);
        },
    });
});
// for deleting the release reject reason
let deleteRejectReason = (e) => {
    var id = $('#deleteRejectReasonId').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "delete",
        url: "/delete-reject-reason/" + id,
        beforeSend : function () {
            $(e).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#deleteRejectReason').modal('hide');
            if(response.reject_reason == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Reject Reason is Deleted Successfully");
                $('#reject_reason_table').DataTable().ajax.reload();
            }
            $(e).html('Delete');
            $(e).removeAttr('disabled', 'disabled');
        }
    });
}
// for storing the reject reason
$('#save-reject-reason').click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = {
        reject_reason_name: $('#name').val(),
        reject_reason_description: $('#description').val(),
    }
    $.ajax({
        type: "post",
        url: "/store-reject-reason",
        data: data,
        dataType: "json",
        beforeSend : function () {
            $(e.target).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e.target).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#addRejectReason').modal('hide');
            $('#name').val(''),
            $('#description').val(''),
            $("#small_reject_reason_name").html("");
            $("#small_reject_reason_des").html("");
            if(response.reject_reason == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Reject Reason is Created Successfully");
                $('#reject_reason_table').DataTable().ajax.reload();
            }
            $(e.target).html('Save');
            $(e.target).removeAttr('disabled', 'disabled');
        },
        error: function(error) {
            $("#small_reject_reason_name").html(error.responseJSON.errors.reject_reason_name);
            $("#small_reject_reason_des").html(error.responseJSON.errors.reject_reason_description);
            setTimeout(() => {
                $("#small_reject_reason_name").html("");
                $("#small_reject_reason_des").html("");
            }, 5000);
            $(e.target).html('Save');
            $(e.target).removeAttr('disabled', 'disabled');
        }
    });
});
$('#close').click(function (e) {
    e.preventDefault();
    $('#name').val(''),
    $('#description').val(''),
    $("#small_reject_reason_name").html("");
    $("#small_reject_reason_des").html("");
});
$('.btn-close').click(function (e) {
    e.preventDefault();
    $('#name').val(''),
    $('#description').val(''),
    $("#small_reject_reason_name").html("");
    $("#small_reject_reason_des").html("");
});
