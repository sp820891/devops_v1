$(document).on('click', '.deletePriority ', function (e) {
    e.preventDefault();
    var id = $(this).attr('id');
    $.ajax({
        url: "/deletePriorityId/" + id,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: "json",
        success: function (data) {
            $('#deletePriority').modal('show');
            $("body").removeAttr("style");
            $("#deletePriorityId").val(id);
            $("#delete_name").html(data.priority.name);
        },
    });
});
// for deleting the priority
let deletePriority = (e) => {
    var id = $('#deletePriorityId').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "delete",
        url: "/delete-priority/" + id,
        beforeSend : function () {
            $(e).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#deletePriority').modal('hide');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Priority is Deleted Successfully");
                $('#priority_table').DataTable().ajax.reload();
            }
            $(e).html('Delete');
            $(e).removeAttr('disabled', 'disabled');
        }
    });
}
// for storing the priority
$('#save-priority').click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = {
       priority_name: $('#name').val(),
       priority_description: $('#description').val(),
    }
    $.ajax({
        type: "post",
        url: "/store-priority",
        data: data,
        dataType: "json",
        beforeSend : function () {
            $(e.target).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e.target).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#addPriority').modal('hide');
            $('#name').val(''),
            $('#description').val(''),
            $("#small_priority_name").html("");
            $("#small_priority_des").html("");
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-botton-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-botton-right",
                }
                toastr.success("Priority is Created Successfully");
                $('#priority_table').DataTable().ajax.reload();
            }
            $(e.target).html('Save');
            $(e.target).removeAttr('disabled', 'disabled');
        },
        error: function(error) {
            $("#small_priority_name").html(error.responseJSON.errors.priority_name);
            $("#small_priority_des").html(error.responseJSON.errors.priority_description);
            setTimeout(() => {
                $("#small_priority_name").html("");
                $("#small_priority_des").html("");
            }, 5000);
            $(e.target).html('Save');
            $(e.target).removeAttr('disabled', 'disabled');
        }
    });
});
$('#close').click(function (e) {
    e.preventDefault();
    $('#name').val(''),
    $('#description').val(''),
    $("#small_priority_name").html("");
    $("#small_priority_des").html("");
});
$('.btn-close').click(function (e) {
    e.preventDefault();
    $('#name').val(''),
    $('#description').val(''),
    $("#small_priority_name").html("");
    $("#small_priority_des").html("");
});
