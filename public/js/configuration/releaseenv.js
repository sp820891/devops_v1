$(document).on('click', '.deleteEnv ', function (e) {
    e.preventDefault();
    var id = $(this).attr('id');
    $.ajax({
        url: "/deleteEnvId/" + id,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: "json",
        success: function (data) {
            $('#deleteEnv').modal('show');
            $("body").removeAttr("style");
            $("#deleteEnvId").val(id);
            $("#delete_name").html(data.env.name);
        },
    });
});
// for deleting the release env
let deleteEnv = (e) => {
    var id = $('#deleteEnvId').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "delete",
        url: "/delete-env/" + id,
        beforeSend : function () {
            $(e).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#deleteEnv').modal('hide');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Release Env is Deleted Successfully");
                $('#env_table').DataTable().ajax.reload();
            }
            $(e).html('Delete');
            $(e).removeAttr('disabled', 'disabled');
        }
    });
}
// for storing the release env
$('#save-env').click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = {
        release_env_name: $('#name').val(),
        release_env_description: $('#description').val(),
    }
    $.ajax({
        type: "post",
        url: "/store-env",
        data: data,
        dataType: "json",
        beforeSend : function () {
            $(e.target).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e.target).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#addEnv').modal('hide');
            $('#name').val(''),
            $('#description').val(''),
            $("#small_env_name").html("");
            $("#small_env_des").html("");
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Release Env is Created Successfully");
                $('#env_table').DataTable().ajax.reload();
            }
            $(e.target).html('Save');
            $(e.target).removeAttr('disabled', 'disabled');
        },
        error: function(error) {
            $("#small_env_name").html(error.responseJSON.errors.release_env_name);
            $("#small_env_des").html(error.responseJSON.errors.release_env_description);
            setTimeout(() => {
                $("#small_env_name").html("");
                $("#small_env_des").html("");
            }, 5000);
            $(e.target).html('Save');
            $(e.target).removeAttr('disabled', 'disabled');
        }
    });
});
$('#close').click(function (e) {
    e.preventDefault();
    $('#name').val(''),
    $('#description').val(''),
    $("#small_env_name").html("");
    $("#small_env_des").html("");
});
$('.btn-close').click(function (e) {
    e.preventDefault();
    $('#name').val(''),
    $('#description').val(''),
    $("#small_env_name").html("");
    $("#small_env_des").html("");
});
