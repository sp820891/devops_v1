$(document).on('click', '.deleteProject ', function (e) {
    e.preventDefault();
    var id = $(this).attr('id');
    $.ajax({
        url: "/deleteProjectId/" + id,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: "json",
        success: function (data) {
            $('#deleteProject').modal('show');
            $("body").removeAttr("style");
            $("#deleteProjectId").val(id);
            $("#delete_name").html(data.project.name);
        },
    });
});
// for deleting the project
let deleteProject = (e) => {
    var id = $('#deleteProjectId').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "delete",
        url: "/delete-project/" + id ,
        beforeSend : function () {
            $(e).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#deleteProject').modal('hide');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Project is Deleted Successfully");
                $('#project_table').DataTable().ajax.reload();
            }
            $(e).html('Delete');
            $(e).removeAttr('disabled', 'disabled');
        }
    });
}
// for storing the project
$('#save-project').click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = {
        project_name: $('#project_name').val(),
        project_description: $('#project_des').val(),
    }
    $.ajax({
        type: "post",
        url: "/store-project",
        data: data,
        dataType: "json",
        beforeSend : function () {
            $(e.target).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e.target).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#add-project').modal('hide');
            $('#project_table').DataTable().ajax.reload();
            $('#project_name').val(''),
            $('#project_des').val(''),
            $("#small_project_name").html("");
            $("#small_project_des").html("");
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Project is Created Successfully");
                $('#project_table').DataTable().ajax.reload();
            }
            $(e.target).html('Save');
            $(e.target).removeAttr('disabled', 'disabled');
        },
        error: function(error) {
            $("#small_project_name").html(error.responseJSON.errors.project_name);
            $("#small_project_des").html(error.responseJSON.errors.project_description);
            setTimeout(() => {
                $("#small_project_name").html("");
                $("#small_project_des").html("");
            }, 5000);
            $(e.target).html('Save');
            $(e.target).removeAttr('disabled', 'disabled');
        }
    });
});
$('#close').click(function (e) {
    e.preventDefault();
    $('#project_name').val(''),
    $('#project_des').val(''),
    $("#small_project_name").html("");
    $("#small_project_des").html("");
});
$('.btn-close').click(function (e) {
    e.preventDefault();
    $('#project_name').val(''),
    $('#project_des').val(''),
    $("#small_project_name").html("");
    $("#small_project_des").html("");
});
