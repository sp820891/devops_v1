$(document).on('click', '.deleteRule ', function (e) {
    e.preventDefault();
    var id = $(this).attr('id');
    $.ajax({
        url: "/deleteRuleId/" + id,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: "json",
        success: function (data) {
            $('#deleteRule').modal('show');
            $("body").removeAttr("style");
            $("#deleteRuleId").val(id);
            $("#delete_name").html(data.rule.name);
        },
    });
});
// for deleting the rule
let deleteRule = (e) => {
    var id = $('#deleteRuleId').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "delete",
        url: "/delete-rule/" + id,
        beforeSend : function () {
            $(e).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#deleteRule').modal('hide');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Rule is Deleted Successfully");
                $('#rules_table').DataTable().ajax.reload();
            }
            $(e).html('Yes, delete it!');
            $(e).removeAttr('disabled', 'disabled');
        }
    });
}
// for storing the rule
$('#save-rule').click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = {
       rule_name: $('#name').val(),
    }
    $.ajax({
        type: "post",
        url: "/store-rule",
        data: data,
        dataType: "json",
        beforeSend : function () {
            $(e.target).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e.target).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#addRule').modal('hide');
            $('#name').val(''),
            $("#small_rule_name").html("");
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-botton-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Power Point Rules is Added Successfully");
                $('#rules_table').DataTable().ajax.reload();
            }
            $(e.target).html('Save');
            $(e.target).removeAttr('disabled', 'disabled');
        },
        error: function(error) {
            $("#small_rule_name").html(error.responseJSON.errors.rule_name);
            setTimeout(() => {
                $("#small_rule_name").html("");
            }, 5000);
            $(e.target).html('Save');
            $(e.target).removeAttr('disabled', 'disabled');
        }
    });
});
$('#close').click(function (e) {
    e.preventDefault();
    $('#name').val(''),
    $("#small_rule_name").html("");

});
$('.btn-close').click(function (e) {
    e.preventDefault();
    $('#name').val(''),
    $("#small_rule_name").html("");

});
