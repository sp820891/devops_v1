$(document).on('click', '.deleteSprint ', function (e) {
    e.preventDefault();
    var id = $(this).attr('id');
    $.ajax({
        url: "/deleteSprintId/" + id,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: "json",
        success: function (data) {
            $('#deleteSprint').modal('show');
            $("body").removeAttr("style");
            $("#deleteSprintId").val(id);
            $("#delete_name").html(data.sprint.name);
        },
    });
});
// for delete the sprint
let deleteSprint = (e) => {
    var id = $('#deleteSprintId').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "delete",
        url: "/delete-sprint/"+id,
        beforeSend : function () {
            $(e).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#deleteSprint').modal('hide');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Sprint is Deleted Successfully");
                $('#sprint_table').DataTable().ajax.reload();
            }
            $(e).html('Delete');
            $(e).removeAttr('disabled', 'disabled');
        }
    });
}
// for storing the sprint
$('#save-sprint').click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = {
        sprint:$('#name').val(),
        start_date:$('#start_date').val(),
        end_date:$('#end_date').val(),
        goal:$('#goal').val(),
    }
    $.ajax({
        type: "post",
        url: "/store-sprint",
        data: data,
        dataType: "json",
        beforeSend : function () {
            $(e.target).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e.target).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#addSprint').modal('hide');
            $('#name').val("");
            $('#start_date').val("");
            $('#end_date').val("");
            $('#goal').val("");
            $("#name_small").html("");
            $("#start_small").html("");
            $("#end_small").html("");
            $("#goal_small").html("");
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Sprint is Created Successfully");
                $('#sprint_table').DataTable().ajax.reload();
            }
            $(e.target).html('Save');
            $(e.target).removeAttr('disabled', 'disabled');
        },
        error: function(error) {
            $("#name_small").html(error.responseJSON.errors.sprint);
            $("#start_small").html(error.responseJSON.errors.start_date);
            $("#end_small").html(error.responseJSON.errors.end_date);
            $("#goal_small").html(error.responseJSON.errors.goal);
            setTimeout(() => {
                $("#name_small").html("");
                $("#start_small").html("");
                $("#end_small").html("");
                $("#goal_small").html("");
            }, 5000);
            $(e.target).html('Save');
            $(e.target).removeAttr('disabled', 'disabled');
        }
    });
});
$('#close').click(function (e) {
    e.preventDefault();
    $('#name').val("");
    $('#start_date').val("");
    $('#end_date').val("");
    $('#goal').val("");
    $("#name_small").html("");
    $("#start_small").html("");
    $("#end_small").html("");
    $("#goal_small").html("");
});
$('.btn-close').click(function (e) {
    e.preventDefault();
    $('#name').val("");
    $('#start_date').val("");
    $('#end_date').val("");
    $('#goal').val("");
    $("#name_small").html("");
    $("#start_small").html("");
    $("#end_small").html("");
    $("#goal_small").html("");
});
