let fetchPermission = () => {
    $.ajax({
        type: "get",
        url: "/fetchRoleWithPermission/"+$('#role_name').val(),
        success: function (response) {
            $.each(response.permissions, function (i, val) {
                $("#permission_check"+val).prop('checked',true);
            });
            if ($('input[name="permission_check"]:checked').length == $('input[name="permission_check"]').length) {
                $("#permission_check_all").prop('checked',true);
            }
            else {
                $("#permission_check_all").prop('checked',false);
            }
        }
     });
}
fetchPermission();
$("#permission_check_all").click(function(){
   $("input[name='permission_check']").prop('checked',$(this).prop('checked'));
});
let update_permission = (role_name) => {
    if ($('input[name="permission_check"]').is(':checked')) {
        var checked = [];
        var unchecked = [];
        $('input[name="permission_check"]:checked').each(function() {
            var val = this.value;
            checked.push(val);
         });
        $('input[name="permission_check"]:not(:checked)').each(function() {
            let val = this.value;
            unchecked.push(val);
         });
         $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "post",
                url: "/assignRoleWithPermission/"+role_name,
                data: {checked:checked,unchecked:unchecked},
                beforeSend : function () {
                    $('#update_btn').html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
                    $('#update_btn').attr('disabled', 'disabled');
                },
                success: function () {
                    toastr.options = {
                        "positionClass": "toast-bottom-right",
                    }
                    toastr.success("Permissions is Updated Successfully");
                    $('#update_btn').html('Update');
                    $('#update_btn').removeAttr('disabled', 'disabled');
                    if ($('input[name="permission_check"]:checked').length == $('input[name="permission_check"]').length) {
                        $("#permission_check_all").prop('checked',true);
                    }
                    else {
                        $("#permission_check_all").prop('checked',false);
                    }
                }
            });
    }
    else {
        toastr.options = {
            "positionClass": "toast-bottom-right",
        }
        toastr.error("Minimum One Permission Is Checked");
        fetchPermission();
    }
};
