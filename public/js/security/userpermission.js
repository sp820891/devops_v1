// USER BASED PERMISSION START

let viewPermission = (id) => {
    $.ajax({
    type: "get",
    url: "/viewPermission/"+id,
    success: function (response) {
        $("#permission_name").html(response.permission.name);
        $('#id_delete_permission').val(id);
    }
    });
}
let fetchPermission = () => {
    $.ajax({
        type: "get",
        url: "/fetchUserWithPermission/"+$('#user_id').val(),
        success: function (response) {
            $.each(response.permissions, function (i, val) {
                $("#permission_check"+val).prop('checked',true);
            });
            $.each(response.rolePermissions, function (i, val) {
                $("#permission_check"+val.id).prop('checked',true);
            });
            if ($('input[name="permission_check"]:checked').length == $('input[name="permission_check"]').length) {
                $("#permission_check_all").prop('checked',true);
            }
            else {
                $("#permission_check_all").prop('checked',false);
            }
            if(response.userPermissions == ""){
                var html = '<div class="p-0 text-dark">\
                              No Permissions Assigned For Specified User.\
                         </div>';
                $('#userBasedPermissions').html(html);
            }
            $.each(response.userPermissions, function (i, val) {
                 var html = '<div class="p-0 text-dark">\
                 <svg width="13" height="13" viewBox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">\
                 <rect x="0.5" y="0.5" width="12" height="12" fill="white" stroke="black"/>\
                 </svg>&nbsp;'+val.name+'\
                             </div>';
                 $('#userBasedPermissions').append(html);
            });
        }
     });
}
fetchPermission();
let update_permission = (user_id) => {
    if ($('input[name="permission_check"]').is(':checked')) {
    var checked = [];
    var unchecked = [];
    $('input[name="permission_check"]:checked').each(function() {
        var val = this.value;
        checked.push(val);
        });
    $('input[name="permission_check"]:not(:checked)').each(function() {
        let val = this.value;
        unchecked.push(val);
        });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "post",
        url: "/assignUserWithPermission/"+user_id,
        data: {checked:checked,unchecked:unchecked},
        beforeSend : function () {
            $('#update_btn').html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $('#update_btn').attr('disabled', 'disabled');
        },
        success: function () {
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("Permissions is Updated Successfully");
            $('#userBasedPermissions').html("");
            fetchPermission();
            $('#update_btn').html('Update');
            $('#update_btn').removeAttr('disabled', 'disabled');
            if ($('input[name="permission_check"]:checked').length == $('input[name="permission_check"]').length) {
                $("#permission_check_all").prop('checked',true);
            }
            else {
                $("#permission_check_all").prop('checked',false);
            }
        },
    });
    }
    else {
        toastr.options = {
            "positionClass": "toast-bottom-right",
        }
        toastr.error("Minimum One Permission Is Checked");
        fetchPermission();
    }
};

// USER BASED PERMISSION END

