// USER CREATE START

$(document).ready(function () {
    $('#saveUser').click(function (e) {
        e.preventDefault();
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: 'users/store',
            type: "post",
            dataType: "json",
            data: $('#userForm').serialize(),
            beforeSend : function () {
                $(e.target).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
                $(e.target).attr('disabled', 'disabled');
            },
            success: function (response) {
                $('#userModel').modal('hide');
                $('#user-table').DataTable().ajax.reload();
                $('#userForm')[0].reset();
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("User is Added Successfully");
                 $(e.target).html('Save');
                $(e.target).removeAttr('disabled', 'disabled');
                // Application Notification
                let notify = {
                    notify_id: response.user.id,
                    message: "New User is Created",
                    url_name: "user_details",
                    created_by: response.user.created_by,
                    notification_from: "User_Create",
                    created_id: response.created_id,
                };
                $.ajax({
                    type: "get",
                    url: "/notification_channel",
                    data: notify,
                    dataType: "json",
                });
            },
            error: function (error) {
                $('#fname_small').html(error.responseJSON.errors.first_name);
                $('#lname_small').html(error.responseJSON.errors.last_name);
                $('#gender_small').html(error.responseJSON.errors.gender);
                $('#role_small').html(error.responseJSON.errors.role_as);
                $('#password_small').html(error.responseJSON.errors.password);
                $('#email_small').html(error.responseJSON.errors.email);
                setTimeout(() => {
                    $('#fname_small').html('');
                    $('#lname_small').html('');
                    $('#gender_small').html('');
                    $('#role_small').html('');
                    $('#password_small').html('');
                    $('#email_small').html('');
                }, 5000);
                $(e.target).html('Save');
                $(e.target).removeAttr('disabled', 'disabled');
            }

        });
    });

// MODEL CLOSE RESET DATA START

    $('#close').click(function (e) {
        e.preventDefault();
        $('#userForm')[0].reset();
    })

    $('#cancel').click(function (e) {
        e.preventDefault();
        $('#userForm')[0].reset();
    })
// MODEL CLOSE RESET DATA END

});
$('#cancel').click(function (e) {
    e.preventDefault();
    $('#userForm')[0].reset();
})

// USER CREATE END

// EDIT USER START

$(document).on('click', '.edit ', function (e) {
    e.preventDefault();
    var id = $(this).attr('id');

    $.ajax({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: "users/edit/" + id + "/",
        dataType: "json",
        success: function (data) {
            console.log('success:' + data);
            $("#first_name_edit").val(data.user.first_name);
            $("#last_name_edit").val(data.user.last_name);
            $("#email_edit").val(data.user.email);
            $('#editUsers').val(id);
            if (data.user.gender == 'Male') {
                $("#Male_edit").prop('checked', true);
                $("#Female_edit").prop('checked', false);
            } else {
                $("#Male_edit").prop('checked', false);
                $("#Female_edit").prop('checked', true);
            }
            $.each(data.roles, function (i, val) {
                if (data.role == val.name) {
                    $("#roles_edit" + val.id).prop('selected', true);
                }
            });
            $('#editModel').modal('show');
            $("body").removeAttr("style");
        },
        error: function (data) {
            var errors = data.responseJSON;
            console.log(errors)
        }
    });

});

// EDIT USER END

// UPDATE USER START

$('#UpdateUser').click(function (e) {
    e.preventDefault();
    var data = {
        first_name: $("#first_name_edit").val(),
        last_name: $("#last_name_edit").val(),
        email: $("#email_edit").val(),
        gender: $("input[name=gender_edit]:checked").val(),
        role_as: $("#role_as_edit").val(),
    };
    console.log(data);
    var id = $('#editUsers').val();
    $.ajax({
        type: "post",
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: "/updateUser/" + id,
        data: data,
        dataType: "json",
        beforeSend : function () {
            $(e.target).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e.target).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#editModel').modal('hide');
            $('#user-table').DataTable().ajax.reload();
            $(e.target).html('Update');
            $(e.target).removeAttr('disabled', 'disabled');
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("User is Updated Successfully");
        }
    });
});

// UPDATE USER END

// DELETE USER START

$(document).on('click', '.delete ', function (e) {
    e.preventDefault()
    var id = $(this).attr('id');
    $('#deleteUser').val(id);
    $('#DeleteUser').modal('show');
    $("body").removeAttr("style");
    $.ajax({

        url: "users/edit/" + id + "/",
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        dataType: "json",
        success: function (data) {
            console.log('success:' + data);
            $('#user_name').html(data.user.first_name);
        }
    })
});

$(document).on('click', '#delete_btn', function (e) {

    e.preventDefault()
    var id = $('#deleteUser').val();

    $.ajax({

        url: "/deleteUser/" + id,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: "json",
        beforeSend : function () {
            $(e.target).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e.target).attr('disabled', 'disabled');
        },
        success: function (data) {
            $('#user-table').DataTable().ajax.reload();
            $('#DeleteUser').modal('hide');
            $(e.target).html('Delete');
            $(e.target).removeAttr('disabled', 'disabled');
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("User is Deleted Successfully");
        }
    })
})

// DELETE USER END
