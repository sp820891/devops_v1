$(document).ready(function () {
    $('#roleSave').click(function (e) {
        e.preventDefault();
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: 'roles-permissions/store',
            type: "post",
            dataType: "json",
            data: $('#roleForm').serialize(),
            beforeSend : function () {
                $(e.target).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
                $(e.target).attr('disabled', 'disabled');
            },
            success: function (response) {
                $('#RoleModel').modal('hide');
                $('#roles-table').DataTable().ajax.reload();
                $('#roleForm')[0].reset();
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Role is Added Successfully");
                $(e.target).html('Save');
                $(e.target).removeAttr('disabled', 'disabled');
            },
            error:function(error){
                $('#role_small').html(error.responseJSON.errors.name);
                setTimeout(() => {
                    $('#role_small').html('');
                }, 5000);
                $(e.target).html('Save');
                $(e.target).removeAttr('disabled', 'disabled');
            }

        });
    });
});

$(document).on('click', '.deleterole ', function (e) {
    e.preventDefault()
    var id = $(this).attr('id');
    $('#deleteRole').val(id);
    $('#DeleteRole').modal('show');
    $("body").removeAttr("style");
    $.ajax({
        url:'roles-permissions/'+id+'/edit',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: "json",
        success: function (data) {
            $('#role_name').html(data.role.name);
        }
    })
});
$(document).on('click', '#delete_role', function (e) {

    e.preventDefault()
    var id = $('#deleteRole').val();

    $.ajax({

        url: "/deleteRoles/" + id,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: "json",
        beforeSend : function () {
            $(e.target).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e.target).attr('disabled', 'disabled');
        },
        success: function (data) {
            console.log(data);
            $('#roles-table').DataTable().ajax.reload();
            $('#DeleteRole').modal('hide');
            $(e.target).html('Delete');
            $(e.target).removeAttr('disabled', 'disabled');
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("Role is Deleted Successfully");
        }
    })
})

// DELETE USER END
