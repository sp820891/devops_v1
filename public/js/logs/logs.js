$(document).on('click', '.deleteReleaseLog', function (e) {
    e.preventDefault();
    var id = $(this).attr('id');
    $('#deleteReleaseLog').modal('show');
    $("body").removeAttr("style");
    $("#deleteReleaseLogId").val(id);
});
// for deleting the dept
let deleteReleaseLog = (e) => {
    var id = $('#deleteReleaseLogId').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "delete",
        url: "/delete-release-log/" + id,
        beforeSend : function () {
            $(e).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
            $(e).attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#deleteReleaseLog').modal('hide');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Release Log is Deleted Successfully");
                $('#release_log_table').DataTable().ajax.reload();
            }
            $(e).html('Delete');
            $(e).removeAttr('disabled', 'disabled');
        }
    });
}
