// For Departments Single Selection
$("input[name='dep_checked']").on('click', function() {
    // in the handler, 'this' refers to the box clicked on
    var $box = $(this);
    if ($box.is(":checked")) {
      var group = "input:checkbox[name='" + $box.attr("name") + "']";
      $(group).prop("checked", false);
      $box.prop("checked", true);
    } else {
      $box.prop("checked", false);
    }
});
$.ajax({
    type: "get",
    url: "PP_rules_PP_default",
    success: function (response) {
        // For Default Assigned Department With Rules
        $('#dep_check1').prop('checked',true);
        $.each(response.pp_rules_assigned_default, function (i, val) {
            $('#rule_check'+val.powerpoint_rule_id).prop('checked',true);
            $('#pp_select'+val.powerpoint_rule_id).css('visibility', 'visible');
            if(val.power_points == "0.5") {
                var pp = "0point5";
            }
            else {
                var pp = val.power_points;
            }
            $('#pp_select'+val.powerpoint_rule_id+" > #opt"+pp).attr('selected','selected');
        });
       $.each(response.pp_rules, function (i, val) {
            // For Power Point Rules and points
            $("#rule_check"+val.id).change(function (e) {
                e.preventDefault();
                if ($("#rule_check"+val.id).is(':checked')) {
                    $('#pp_select'+val.id).css('visibility', 'visible');
                }
                else {
                    $('#pp_select'+val.id).css('visibility', 'hidden');
                }
            });
       });
    }
});
$('input[name="dep_checked"]').change(function (e) {
    e.preventDefault();
    var department_id = [];
    $('input[name="dep_checked"]:checked').each(function() {
        var val = this.value;
        department_id.push(val);
    });
    $.ajax({
        type: "get",
        url: "/fetchAssignedPPRulesFromDepartment/"+department_id[0],
        success: function (response) {
           $('.rule_assign').prop('checked',false);
           $('.pp_select').css('visibility', 'hidden');
           $.each(response.pp_rule_has_dep, function (i, val) {
                $('#rule_check'+val.powerpoint_rule_id).prop('checked',true);
                $('#pp_select'+val.powerpoint_rule_id).css('visibility', 'visible');
                if(val.power_points == "0.5") {
                    var pp = "0point5";
                }
                else {
                    var pp = val.power_points;
                }
                $('#pp_select'+val.powerpoint_rule_id+" > #opt"+pp).attr('selected','selected');
           });
        }
    });
});
$("#update_rule").click(function() {
    var department_id = [];
    var checked_rules = [];
    var unchecked_rules = [];
    var pp = [];
    var rules_has_pp_checked =[];
    var rules_has_pp_unchecked = [];
    $('input[name="dep_checked"]:checked').each(function() {
        var val = this.value;
        department_id.push(val);
    });
    $('input[name="rule_check"]:checked').each(function() {
        var val = this.value;
        checked_rules.push(val);
    });
    $('input[name="rule_check"]:not(:checked)').each(function() {
        var val = this.value;
        unchecked_rules.push(val);
    });
    $('.pp_select').each(function () {
        var val = this.value;
        pp.push(val);
    });
    for (i = 0; i < checked_rules.length; i++) {
        rules_has_pp_checked[i] = Array(checked_rules[i], pp[i]);
    }
    for (i = 0; i < unchecked_rules.length; i++) {
        rules_has_pp_unchecked[i] = Array(unchecked_rules[i], pp[i]);
    }
    var data = {
        department_id: department_id,
        rules_has_pp_checked: rules_has_pp_checked,
        rules_has_pp_unchecked: rules_has_pp_unchecked,
    };
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });
    $.ajax({
        type: "post",
        url: "/assign_pp_rules_has_departments",
        data: data,
        dataType: "json",
        success: function (response) {
             if (response.status == "dep_id_not_exists") {
                toastr.options = {
                    "positionClass": "toast-top-right",
                }
                toastr.error("Department is Not Selected");
             }
             else if(response.status == "pp_rules_not_exists") {
                toastr.options = {
                    "positionClass": "toast-top-right",
                }
                toastr.error("Rule is Not Selected");
             }
             else {
                toastr.options = {
                    "positionClass": "toast-top-right",
                }
                toastr.success("Power Point Rules is Updated Successfully");
             }
        },
        error: function (response) {
            toastr.options = {
                "positionClass": "toast-top-right",
            }
            toastr.error("Power Point is Not Selected");
        }
    });
});
