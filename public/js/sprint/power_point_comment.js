
$(document).ready(function () {
    $('#save_comment').click(function (e) {
        e.preventDefault();
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: 'pp_commentstore',
            type: "post",
            dataType: "json",
            data: $('#powerpoint_comment').serialize(),
            beforeSend : function () {
                $(e.target).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
                $(e.target).attr('disabled', 'disabled');
            },
            success: function (response) {
                $('#addComment').modal('hide');
                $('#comments_table').DataTable().ajax.reload();
                $('#powerpoint_comment')[0].reset();
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Power Point Comment is Added Successfully");
                 $(e.target).html('Save');
                $(e.target).removeAttr('disabled', 'disabled');
            } ,
             error: function (error) {
                $('#small_comment_name').html(error.responseJSON.errors.PowerpointComment);
                setTimeout(() => {
                    $('#small_comment_name').html('');
                }, 5000);
                $(e.target).html('Save');
                $(e.target).removeAttr('disabled', 'disabled');

             }

        });
    });
    });

    $('#comment_close').click(function (e) {
        e.preventDefault();
        $('#powerpoint_comment')[0].reset();
    })

    $('#close_comment').click(function (e) {
        e.preventDefault();
        $('#powerpoint_comment')[0].reset();
    })

// Delete Power Point Comments -Start
    $(document).on('click', '.deletecomment ', function (e) {
        e.preventDefault();
        var id = $(this).attr('id');
        $.ajax({
            url: "/deleteCommentId/" + id,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "json",
            success: function (data) {
                $('#deleteComment').modal('show');
                $("body").removeAttr("style");
                $("#deleteCommentId").val(id);

                $("#delete_comment").html(data.powerpoint.power_point_comments);
            },
        });
    });
    // for deleting the rule
    let deleteComment = (e) => {
        var id = $('#deleteCommentId').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "delete",
            url: "/delete-Comment/" + id,
            beforeSend : function () {
                $(e).html('<i class="spinner-border spinner-border-sm" role="status"></i> Please Wait...');
                $(e).attr('disabled', 'disabled');
            },
            success: function (response) {
                $('#deleteComment').modal('hide');
                if(response.status == 404) {
                    toastr.options = {
                        "positionClass": "toast-bottom-right",
                    }
                    toastr.error("Something Went Wrong");
                }
                else {
                    toastr.options = {
                        "positionClass": "toast-bottom-right",
                    }
                    toastr.success("Comment is Deleted Successfully");
                    $('#comments_table').DataTable().ajax.reload();
                }
                $(e).html('Yes, delete it!');
                $(e).removeAttr('disabled', 'disabled');
            }
        });
    }
