$('#sprint').change(function (e) {
    e.preventDefault();
    $('.pp_hide').addClass('pp_show');
    $('.pp_hide_btn_emp').addClass('pp_show_button');
});
// to show create button
$('#resource').change(function (e) {
    e.preventDefault();
    $('.pp_hide_btn').addClass('pp_show_button');
});
// Fetch a Sprint Retrospective Data
$.ajax({
    type: "get",
    url: "/fetch_sprint_retro/fetch_default",
    success: function (response) {
        if(response.status == "access_all_field") {
            $("#action_col_retro").addClass("action_col_retro");
            $("#sprint_col_retro").removeClass("sprint_col_retro");
            if(response.sprint_retro == "") {
                let tbody =
                `<tr>
                    <td colspan="7" style="text-align:center;">No Records Found</td>
                </tr>
                `;
                $("#tbody").html(tbody);
            }
            else {
                $("#tbody").html('');
            }
            $.each(response.sprint_retro, function (i, val) {
                let tbody =
                `<tr>
                    <td id="sprint_row${val.sprint_id}">${val.sprint.name}(${val.sprint.start_date}-${val.sprint.end_date})</td>
                    <td id="user_row${val.user_id}${val.sprint_id}">${val.users.full_name}</td>
                    <td>${val.rules.name}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                `;
                $("#tbody").append(tbody);
            });
            $.each(response.sprint_retro_count_sprint, function (i, val) {
                $(`#tbody > tr > #sprint_row${val.sprint_id}`).each(function(index) {
                    if(index % val.sprint_count == 0) {
                        $(this).attr('rowspan',val.sprint_count);
                    } else {
                        $(this).remove();
                    }
                });
            });
            $.each(response.sprint_retro_count_emp, function (i, val) {
                $(`#tbody > tr > #user_row${val.user_id}${val.sprint_id}`).each(function(index) {
                    if(index % val.emp_count == 0) {
                        $(this).attr('rowspan',val.emp_count);
                    } else {
                        $(this).remove();
                    }
                });
            });
        }
        else {
            $("#action_col_retro").addClass("action_col_retro");
            $("#sprint_col_retro").removeClass("sprint_col_retro");
            $("#emp_col_retro").addClass("emp_col_retro");
            if(response.sprint_retro == "") {
                let tbody =
                `<tr>
                    <td colspan="6" style="text-align:center;">No Records Found</td>
                </tr>
                `;
                $("#tbody").html(tbody);
            }
            else {
                $("#tbody").html('');
            }
            $.each(response.sprint_retro, function (i, val) {
                let tbody =
                `<tr>
                    <td id="sprint_row${val.sprint_id}">${val.sprint.name}(${val.sprint.start_date}-${val.sprint.end_date})</td>
                    <td>${val.rules.name}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                `;
                $("#tbody").append(tbody);
            });
            $.each(response.sprint_retro_count_sprint, function (i, val) {
                $(`#tbody > tr > #sprint_row${val.sprint_id}`).each(function(index) {
                    if(index % val.sprint_count == 0) {
                        $(this).attr('rowspan',val.sprint_count);
                    } else {
                        $(this).remove();
                    }
                });
            });
        }
    }
});
// Create a Sprint Retrospective Data
$('#create_sprint_retro').click(function (e) {
    e.preventDefault();
    var data = {
        sprint:$('#sprint').val(),
        resource:$('#resource').val(),
    }
    $.ajax({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: '/create_sprint_retro',
        type: "post",
        dataType: "json",
        data: data,
        success: function (response) {
            if(response.status == "sprint_retro_is_already_created") {
                toastr.options = {
                    "positionClass": "toast-top-right",
                }
                toastr.error("Sprint Retrospective is Already Created for this Resource");
            }
            else {
                $("#action_col_retro").removeClass("action_col_retro");
                $("#sprint_col_retro").addClass("sprint_col_retro");
            }
            // For Fetch Sprint Retro
            data = {
                'sprint': response.sprint,
                'resource':response.resource
            }
            $.ajax({
                type: "get",
                url: "/fetch_sprint_retro/fetch_create",
                dataType: "json",
                data: data,
                success: function (response) {
                    $("#tbody").html('');
                    $.each(response.sprint_retro, function (i, val) {
                        let tbody =
                        `<tr>
                            <td id="user_row${val.user_id}${val.sprint_id}">${val.users.full_name}</td>
                            <td>${val.rules.name}</td>
                            <td></td>
                            <td><input type="text" class="text_field_retro" id="awarded_point${val.pp_rule_id}"></td>
                            <td><input type="text" class="text_field_retro" id="assessment_comment${val.pp_rule_id}"></td>
                            <td><input type="text" class="text_field_retro" id="improvement_comment${val.pp_rule_id}"></td>
                            <td ><button type="button" class="text_field_retro" id="update_retro${val.pp_rule_id}">update</button></td>
                        </tr>
                        `;
                        $("#tbody").append(tbody);
                    });
                    $.each(response.sprint_retro_count_emp, function (i, val) {
                        $(`#tbody > tr > #user_row${val.user_id}${val.sprint_id}`).each(function(index) {
                            if(index % val.emp_count == 0) {
                                $(this).attr('rowspan',val.emp_count);
                            } else {
                                $(this).remove();
                            }
                        });
                    });
                }
            });
        },

    });
});
// For View a Sprint Retrospective Data
$("#view_sprint_retro").click(function (e) {
    e.preventDefault();
    var data = {
        sprint:$('#sprint').val(),
        resource:$('#resource').val(),
    }
    $.ajax({
        type: "get",
        url: "/fetch_sprint_retro/fetch_view",
        dataType: "json",
        data: data,
        success: function (response) {
            if(response.sprint_retro == "") {
                let tbody =
                `<tr>
                    <td colspan="7" style="text-align:center;">No Records Found</td>
                </tr>
                `;
                $("#tbody").html(tbody);
            }
            else {
                $("#tbody").html('');
            }
            $("#action_col_retro").removeClass("action_col_retro");
            $("#sprint_col_retro").addClass("sprint_col_retro");
            $.each(response.sprint_retro, function (i, val) {
                let tbody =
                `<tr>
                    <td id="user_row${val.user_id}${val.sprint_id}">${val.users.full_name}</td>
                    <td>${val.rules.name}</td>
                    <td></td>
                    <td><input type="text" class="text_field_retro" id="awarded_point${val.pp_rule_id}"></td>
                    <td><input type="text" class="text_field_retro" id="assessment_comment${val.pp_rule_id}"></td>
                    <td><input type="text" class="text_field_retro" id="improvement_comment${val.pp_rule_id}"></td>
                    <td ><button type="button" class="text_field_retro" id="update_retro${val.pp_rule_id}">update</button></td>
                </tr>
                `;
                $("#tbody").append(tbody);
            });
            $.each(response.sprint_retro_count_emp, function (i, val) {
                $(`#tbody > tr > #user_row${val.user_id}${val.sprint_id}`).each(function(index) {
                    if(index % val.emp_count == 0) {
                        $(this).attr('rowspan',val.emp_count);
                    } else {
                        $(this).remove();
                    }
                });
            });
        }
    });
});
$("#view_sprint_retro_emp").click(function (e) {
    e.preventDefault();
    var data = {
        sprint:$('#sprint').val(),
    }
    $.ajax({
        type: "get",
        url: "/fetch_sprint_retro/fetch_view_emp",
        dataType: "json",
        data: data,
        success: function (response) {
            if(response.sprint_retro == "") {
                let tbody =
                `<tr>
                    <td colspan="5" style="text-align:center;">No Records Found</td>
                </tr>
                `;
                $("#tbody").html(tbody);
            }
            else {
                $("#tbody").html('');
            }
            $("#action_col_retro").addClass("action_col_retro");
            $("#sprint_col_retro").addClass("sprint_col_retro");
            $("#emp_col_retro").addClass("emp_col_retro");
            $.each(response.sprint_retro, function (i, val) {
                let tbody =
                `<tr>
                    <td>${val.rules.name}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                `;
                $("#tbody").append(tbody);
            });
        }
    });
});
