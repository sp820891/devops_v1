// Dark and light mode toggle button for Horizontal Nav Bar
if($('#app_mode').hasClass("light") || $('#app_mode').hasClass("")) {
    $('#app_mode1').css('display','block');
    $('#app_mode2').css('display','none');
}
else {
    $('#app_mode1').css('display','none');
    $('#app_mode2').css('display','block');
}
$('#app_mode_nav').click(function (e) {
    e.preventDefault();
    if($('#app_mode').hasClass("light") || $('#app_mode').hasClass("")) {
          $('#app_mode1').css('display','block');
          $('#app_mode2').css('display','none');
    }
    else {
          $('#app_mode1').css('display','none');
          $('#app_mode2').css('display','block');
    }
    // For Leaderboard
    $(".leaderboard__profiles").toggleClass("dark_custom");
    $(".leaderboard__title").toggleClass("dark_custom_text");
    $(".leaderboard__icon").toggleClass("dark_custom_icon");
    $(".leaderboard__profile").toggleClass("dark_custom_leaderboard");
    // For Modals
    $(".modal-content").toggleClass('dark_custom_modal');
});
// Leaderboard default
if($("body").hasClass("dark")) {
    $(".leaderboard__profiles").addClass("dark_custom");
    $(".leaderboard__title").addClass("dark_custom_text");
    $(".leaderboard__icon").addClass("dark_custom_icon");
    $(".leaderboard__profile").addClass("dark_custom_leaderboard");
    // For Modals
    $(".modal-content").addClass('dark_custom_modal');
}
// For Sidebar Fixing
$(".sidebar-toggle").click(function (e) {
    e.preventDefault();
    if ($('.sidebar').hasClass('sidebar-mini')) {
        sessionStorage.setItem('sidebar_responsive','sidebar-mini');
    }
    else {
        sessionStorage.setItem('sidebar_responsive','');
    }
});
if (sessionStorage.getItem('sidebar_responsive') == 'sidebar-mini') {
    $('.sidebar').addClass('sidebar-mini');
}
// For Profile Picture
$(".file_open_profile").click(function(e) {
    $("#profile_upload").click();
});
$("#profile_select").hover(function () {
    $('.hoverable_profile').addClass('hover_profile');
    $('.hoverable_profile_text').addClass('hover_profile_text');
    $('.layer').addClass('layer_hover');
    },
    function () {
        $('.hoverable_profile').removeClass('hover_profile');
        $('.hoverable_profile_text').removeClass('hover_profile_text');
        $('.layer').removeClass('layer_hover');
    }
);
$("#profile_upload").change(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = new FormData(document.querySelector('#form_profile_update'));
    $.ajax({
        type: "post",
        url: "/updateProfilePicture",
        data: data,
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        success: function (response) {
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("Your Profile Picture is Updated Successfully");
            var url = response.url+'/'+response.pic;
            $('#new-pic1').attr("src", `${url}`);
            $('#new-pic2').attr("src", `${url}`);
            $('#new-pic3').attr("src", `${url}`);
        }
    });
});
// Modal Document Field Fixing For Merge and Support Requests
$("#formFileMerge").change(function (e) {
    e.preventDefault();
    var fileCount = $(this).prop("files");
    if(fileCount.length > 1) {
        $("#mergeFile").html(fileCount.length+" Files Selected.");
    }
    else {
        $("#mergeFile").html(fileCount.length+" File Selected.");
    }
});
$("#formFileSupport").change(function (e) {
    e.preventDefault();
    var fileCount = $(this).prop("files");
    if(fileCount.length > 1) {
        $("#supportFile").html(fileCount.length+" Files Selected.");
    }
    else {
        $("#supportFile").html(fileCount.length+" File Selected.");
    }
});
// UI Fixing while Modal Opening
$('button[data-bs-toggle="modal"]').click(function (e) {
    e.preventDefault();
    $("body").removeAttr("style");
});
// For Application Notification
$.ajax({
    type: "get",
    url: "/fetchAppNotification",
    success: function (response) {
        if(response.notifications_count == 0) {
            var html = `<h6 class="mt-2 mb-2 text-center" style="color:#6c757d;">No Records Found</h6>`;
            $("#application_notification").html(html);
        }
        else {
           $("#application_notification").html("");
        }
        var now = new Date();
        $.each(response.notifications , function (i, val) {
            var created_at = new Date(val.created_at);
            var diff_time = created_at.getTime() -  now.getTime();
            var diff_hours = Math.abs(created_at - now) / 36e5;
            var yesterday = new Date();
            yesterday.setDate(now.getDate() - 1);
            var hours = Math.trunc(diff_hours);
            var seconds = Math.abs(diff_time) / 1000;
            var minutes = Math.trunc(Math.abs(diff_time / 60000));
            if (seconds <= 60 && created_at.getDate() == now.getDate() && created_at.getMonth() == now.getMonth() && created_at.getFullYear() == now.getFullYear()) {
                var dateTime = "Just Now";
            }
            else if (seconds > 60 && minutes == 1 && created_at.getDate() == now.getDate() && created_at.getMonth() == now.getMonth() && created_at.getFullYear() == now.getFullYear()) {
                var dateTime = minutes+" Min Ago";
            }
            else if (minutes > 1 && minutes <= 60 && created_at.getDate() == now.getDate() && created_at.getMonth() == now.getMonth() && created_at.getFullYear() == now.getFullYear()) {
                var dateTime = minutes+" Mins Ago";
            }
            else if(hours == 1 && created_at.getDate() == now.getDate() && created_at.getMonth() == now.getMonth() && created_at.getFullYear() == now.getFullYear()) {
                var dateTime = hours+" Hr Ago";
            }
            else if(hours > 1 && hours <= 12 && created_at.getDate() == now.getDate() && created_at.getMonth() == now.getMonth() && created_at.getFullYear() == now.getFullYear()) {
                var dateTime = hours+" Hrs Ago";
            }
            else if(hours > 12 && created_at.getDate() == now.getDate() && created_at.getMonth() == now.getMonth() && created_at.getFullYear() == now.getFullYear()) {
                var dateTime = "Today";
            }
            else if (created_at.getDate() != now.getDate() && yesterday.getDate() == created_at.getDate() && created_at.getMonth() == yesterday.getMonth() && created_at.getFullYear() == yesterday.getFullYear()) {
                var dateTime = "Yesterday";
            }
            else {
                if(created_at.getDate() < 10) {
                    var date_format = "0"+created_at.getDate();
                }
                else {
                    var date_format = created_at.getDate();
                }
                if(created_at.getMonth() < 10) {
                    var month_format = "0"+(created_at.getMonth()+1);
                }
                else {
                    var month_format = (created_at.getMonth()+1);
                }
                var dateTime = date_format+"/"+month_format+"/"+created_at.getFullYear();
            }
             let notifications = `<a onclick="application_notification_markasread('${val.id}','${response.url}')" href="${response.url}/${val.data["url_name"]}/${val.data["notify_id"]}" class="iq-sub-card">
                            <div class="d-flex align-items-center">
                            <img class="avatar-40 rounded-pill bg-soft-primary p-1" src="${response.url}/images/shapes/01.png" alt="img">
                            <div class="ms-3 w-100">
                                <h6 class="mb-0 ">${val.data["message"]}</h6>
                                <div class="d-flex justify-content-between align-items-center">
                                <p class="mb-0">${val.data["created_by"]}</p>
                                <small class="float-right font-size-12">${dateTime}</small>
                                </div>
                            </div>
                            </div>
                        </a>`;
            $("#application_notification").append(notifications);
        });
        if (response.notifications_count > 4) {
            $('#notification_read_more').css("display", "block");
        }
        if (response.notifications_count > 100) {
            var notifications_count = `100+`;
            $('#notifications_count').html(notifications_count);
        }
        else {
            $('#notifications_count').html(response.notifications_count);
        }
    }
});
let application_notification_markasread = (id,url) => {
    $.ajax({
     type: "get",
     url: url+"/application_notification_markasread/"+id,
    });
}
// Fixing DevOps Title Nav Bar Link
$(".navbar-brand").removeAttr("href");
