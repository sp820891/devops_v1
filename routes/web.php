<?php
// Controllers
use App\Http\Controllers\ConfigurationController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Security\RolePermission;
use App\Http\Controllers\Security\RoleController;
use App\Http\Controllers\Security\PermissionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SupportRequestController;
use App\Http\Controllers\Devopssecurity\DevopsPermissionController;
use App\Http\Controllers\Devopssecurity\DevopsUserController;
use App\Http\Controllers\Devopssecurity\DevopsRoleController;
use App\Http\Controllers\MergerequestController;
use App\Http\Controllers\PowerPointController;
use App\Http\Controllers\sprint\SprintplanController;
use App\Http\Controllers\sprint\SprintRetrospectiveController;
use App\Http\Controllers\TicketsController;
use Illuminate\Support\Facades\Artisan;
// Packages
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpKernel\Profiler\Profile;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__ . '/auth.php';
Route::get('/storage', function () {
    Artisan::call('storage:link');
});
//UI Pages Routs
Route::get('/design', [HomeController::class, 'uisheet'])->name('uisheet');
Route::group(['middleware' => 'auth'], function () {
    // Page or Menu Routes
    Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');
    Route::get('/mergerequests', [HomeController::class, 'mergerequest'])->name('mergerequest');
    Route::get('/supportrequests', [HomeController::class, 'supportrequest'])->name('supportrequest');
    Route::get('/sprint_planning', [SprintplanController::class, 'sprint_planning'])->name('sprint_planning');
    Route::get('/sprint_retrospective', [SprintRetrospectiveController::class, 'sprint_retrospective'])->name('sprint_retrospective');
    Route::get('projects',[ConfigurationController::class, 'project'])->name('projects');
    Route::get('sprints',[ConfigurationController::class, 'sprint'])->name('sprints');
    Route::get('status',[ConfigurationController::class, 'status'])->name('status');
    Route::get('priorities',[ConfigurationController::class, 'priorities'])->name('priorities');
    Route::get('environments',[ConfigurationController::class, 'environments'])->name('environments');
    Route::get('reject-reasons',[ConfigurationController::class, 'reject_reasons'])->name('reject-reasons');
    Route::get('departments',[ConfigurationController::class,'departments'])->name('departments');
    Route::get('power_point_rules',[ConfigurationController::class,'power_point_rules'])->name('power_point_rules');
    Route::get('/logs',[HomeController::class,'logs'])->name('logs');
    // Dashboard Charts
    Route::get('fetchDasboardMergeRequests',[HomeController::class,'fetchDasboardMergeRequests']);
    Route::get('fetchDashboardReleaseRequests',[HomeController::class,'fetchDashboardReleaseRequests']);
    Route::get('fetchDashboardSupportRequests',[HomeController::class,'fetchDashboardSupportRequests']);
    // Merge Request
    Route::post('/addmerge',[MergerequestController::class,'addmerge'])->name('addmerge');
    Route::get('/fetchMerge',[MergerequestController::class,'fetchMerge'])->name('fetchMerge');
    Route::get('/editMerge/{id}',[MergerequestController::class,'editMerge'])->name('editMerge');
    Route::post('/updateMerge/{id}',[MergerequestController::class,'updateMerge'])->name('updateMerge');


    Route::post('/updateComment/{id}',[MergerequestController::class,'updateComment']);
    Route::get('/showComment/{id}',[MergerequestController::class,'showComment']);


    Route::post('/QaReject/{id}',[MergerequestController::class,'QaReject']);
    Route::post('/MergeReject/{id}',[MergerequestController::class,'MergeReject']);

    Route::post('/QaApprove/{id}',[MergerequestController::class,'QaApprove']);
    Route::post('/mergedone/{id}',[MergerequestController::class,'MergeApprove']);


    Route::post('/deleteMerge/{id}',[MergerequestController::class,'deleteMerge'])->name('deleteMerge');
    Route::get('/mergerequest_view/{id}',[MergerequestController::class,'mergeview'])->name('mergeview');
    //SupportRequest
    Route::post('/addSupport',[SupportRequestController::class,'store'])->name('addSupport');
    Route::get('/fetchSupports',[SupportRequestController::class,'fetchSupports'])->name('fetchSupports');
    Route::get('/editSupport/{id}',[SupportRequestController::class,'editSupport'])->name('editSupports');
    Route::post('/updateSupport/{id}',[SupportRequestController::class,'updateSupport'])->name('updateSupport');
    Route::post('/deleteSupport/{id}',[SupportRequestController::class,'deleteSupport'])->name('deleteSupport');
    Route::get('/supportrequest_view/{id}',[SupportRequestController::class,'supportView'])->name('supportView');
    Route::post('/supportApprove/{id}',[SupportRequestController::class,'supportApprove']);
    Route::post('/supportReject/{id}',[SupportRequestController::class,'supportReject']);
    Route::get('docdeleted/{id}',[SupportRequestController::class,'docdeleted']);
    // sprint-planning-view
    Route::get('/sprint-of-project/{id}', [SprintplanController::class,'show'])->name('sprint-of-project');
    // configuration
    Route::post('store-project', [ConfigurationController::class, 'add_project']);
    Route::get('deleteProjectId/{id}',[ConfigurationController::class,'deleteProjectId']);
    Route::delete('delete-project/{id}',[ConfigurationController::class,'deleteProject']);
    Route::post('store-sprint',[ConfigurationController::class, 'add_sprint']);
    Route::get('deleteSprintId/{id}',[ConfigurationController::class,'deleteSprintId']);
    Route::delete('delete-sprint/{id}',[ConfigurationController::class,'deleteSprint']);
    // For Merge Request Status
    Route::get('fetchMergeStatus',[ConfigurationController::class, 'fetchMergeStatus']);
    Route::post('store-status',[ConfigurationController::class, 'add_status']);
    Route::get('deleteStatusId/{id}',[ConfigurationController::class,'deleteStatusId']);
    Route::delete('delete-status/{id}',[ConfigurationController::class,'deleteStatus']);
    // For Support Request Status
    Route::get('fetchSupportStatus',[ConfigurationController::class, 'fetchSupportStatus']);
    Route::post('store-support-status',[ConfigurationController::class, 'add_support_status']);
    Route::get('deleteSupportStatusId/{id}',[ConfigurationController::class,'deleteSupportStatusId']);
    Route::delete('delete-support-status/{id}',[ConfigurationController::class,'deleteSupportStatus']);
    Route::post('store-priority',[ConfigurationController::class, 'add_priority']);
    Route::get('deletePriorityId/{id}',[ConfigurationController::class,'deletePriorityId']);
    Route::delete('delete-priority/{id}',[ConfigurationController::class,'deletePriority']);
    Route::post('store-env',[ConfigurationController::class, 'add_env']);
    Route::get('deleteEnvId/{id}',[ConfigurationController::class,'deleteEnvId']);
    Route::delete('delete-env/{id}',[ConfigurationController::class,'deleteEnv']);
    Route::post('store-reject-reason',[ConfigurationController::class, 'add_reject_reason']);
    Route::get('deleteRejectReasonId/{id}',[ConfigurationController::class,'deleteRejectReasonId']);
    Route::delete('delete-reject-reason/{id}',[ConfigurationController::class,'deleteRejectReason']);
    Route::post('store-dept',[ConfigurationController::class, 'add_dept']);
    Route::get('deleteDeptId/{id}',[ConfigurationController::class,'deleteDeptId']);
    Route::delete('delete-dept/{id}',[ConfigurationController::class,'deleteDept']);
    Route::post('store-rule',[ConfigurationController::class, 'add_rule']);
    Route::get('deleteRuleId/{id}',[ConfigurationController::class,'deleteRuleId']);
    Route::delete('delete-rule/{id}',[ConfigurationController::class,'deleteRule']);
    Route::get('power_point_comments', [PowerPointController::class, 'power_point_comment'])->name('power_point_comments');
    Route::post('pp_commentstore', [PowerPointController::class, 'power_point_comment_store']);
    Route::get('deleteCommentId/{id}', [PowerPointController::class, 'deletePowerpointComment']);
    Route::delete('delete-Comment/{id}', [PowerPointController::class, 'deletePPComment']);
    Route::get('/power_points_system', [PowerPointController::class, 'power_points_system'])->name('power_points_system');
    // USER ROUTES START
    Route::group(['middleware' => ['permission:can access users']], function () {
        Route::resource('users', DevopsUserController::class);
        Route::group(['middleware' => ['permission:can add users']], function () {
            Route::post('users/store', [DevopsUserController::class, 'store']);
        });
        Route::group(['middleware' => ['permission:can edit users']], function () {
            Route::get('users/edit/{id}/', [DevopsUserController::class, 'edit']);
            Route::post('updateUser/{id}', [DevopsUserController::class, 'update']);
        });
        Route::group(['middleware' => ['permission:can delete users']], function () {
            Route::get('/deleteUser/{id}', [DevopsUserController::class, 'destroy']);
        });
        Route::get('/viewPermission/{id}', [DevopsPermissionController::class, 'update']);
        Route::get('/user_details/{id}', [DevopsPermissionController::class, 'show'])->middleware('auth_user_protection');
        Route::get('/fetchUserWithPermission/{user_id}', [DevopsPermissionController::class, 'fetchUserWithPermission']);
        Route::post('/assignUserWithPermission/{user_id}', [DevopsPermissionController::class, 'assignUserWithPermission']);
    });
    // USER ROUTES END

  // ROLE ROUTES START
    Route::group(['middleware' => ['permission:can access a roles']], function () {
        Route::resource('roles-permissions', DevopsRoleController::class);
        Route::group(['middleware' => ['permission:can add roles']], function () {
            Route::post('roles-permissions/store', [DevopsRoleController::class, 'store']);
        });
        Route::group(['middleware' => ['permission:can delete roles']], function () {
            Route::get('/deleteRoles/{id}', [DevopsRoleController::class, 'destroy']);
        });
        Route::group(['middleware' => ['permission:can access role-based-permissions']], function () {
            Route::get('/role-permissions/{id}', [DevopsPermissionController::class, 'Rolebasedpermission']);
            Route::get('/fetchRoleWithPermission/{role}', [DevopsPermissionController::class, 'fetchRoleWithPermission']);
            Route::post('/assignRoleWithPermission/{role}', [DevopsPermissionController::class, 'assignRoleWithPermission']);
        });
    });
    // ROLE ROUTES END
    // ReleaseLogs
    Route::delete('delete-release-log/{id}',[MergerequestController::class,'deleteReleaseLog']);
    //Notifications
    Route::get('/notification_channel',[HomeController::class,'notification_channel']);
    Route::get('/fetchAppNotification',[HomeController::class,'fetchAppNotification']);
    Route::get('/notifications', [HomeController::class,'notifications_view'])->name('notifications')->middleware('notification_route_protection');
    Route::get("/application_notification",[HomeController::class,'application_notification']);
    Route::get("/application_notification_markasread/{id}",[HomeController::class,'application_notification_markasread']);
    Route::get('/user_credential_mail_notification',[HomeController::class,'user_credential_mail_notification']);
    Route::get('/set-password/{token}/{email}',[HomeController::class,'setPassword']);
    Route::get('/forgot_password_mail_notification',[HomeController::class,'forgot_password_mail_notification']);
    Route::get('/email_notification',[HomeController::class,'email_notification']);
    // For Power Point System
    Route::get('/PP_rules_PP_default',[PowerPointController::class,'PP_rules_PP_default']);
    Route::get('/fetchAssignedPPRulesFromDepartment/{id}',[PowerPointController::class,'fetchAssignedPPRulesFromDepartment']);
    Route::post('/assign_pp_rules_has_departments',[PowerPointController::class,'assign_pp_rules_has_departments']);
    // For Sprint Retrospective
    Route::post('/create_sprint_retro',[SprintRetrospectiveController::class,'create_sprint_retro']);
    Route::get('/fetch_sprint_retro/{from}',[SprintRetrospectiveController::class,'fetch_sprint_retro']);
});
Route::get('Profile', [UserController::class, 'profile'])->name('users.profile');
Route::post('updateProfilePicture', [UserController::class, 'profilePicture']);

//App Details Page => 'Dashboard'], function() {
Route::group(['prefix' => 'menu-style'], function () {
    //MenuStyle Page Routs
    Route::get('horizontal', [HomeController::class, 'horizontal'])->name('menu-style.horizontal');
    Route::get('dual-horizontal', [HomeController::class, 'dualhorizontal'])->name('menu-style.dualhorizontal');
    Route::get('dual-compact', [HomeController::class, 'dualcompact'])->name('menu-style.dualcompact');
    Route::get('boxed', [HomeController::class, 'boxed'])->name('menu-style.boxed');
    Route::get('boxed-fancy', [HomeController::class, 'boxedfancy'])->name('menu-style.boxedfancy');
});
//App Details Page => 'special-pages'], function() {
Route::group(['prefix' => 'special-pages'], function () {
    //Example Page Routs
    Route::get('billing', [HomeController::class, 'billing'])->name('special-pages.billing');
    Route::get('calender', [HomeController::class, 'calender'])->name('special-pages.calender');
    Route::get('kanban', [HomeController::class, 'kanban'])->name('special-pages.kanban');
    Route::get('pricing', [HomeController::class, 'pricing'])->name('special-pages.pricing');
    Route::get('rtl-support', [HomeController::class, 'rtlsupport'])->name('special-pages.rtlsupport');
    Route::get('timeline', [HomeController::class, 'timeline'])->name('special-pages.timeline');
});
//Widget Routs
Route::group(['prefix' => 'widget'], function () {
    Route::get('widget-basic', [HomeController::class, 'widgetbasic'])->name('widget.widgetbasic');
    Route::get('widget-chart', [HomeController::class, 'widgetchart'])->name('widget.widgetchart');
    Route::get('widget-card', [HomeController::class, 'widgetcard'])->name('widget.widgetcard');
});
//Maps Routs
Route::group(['prefix' => 'maps'], function () {
    Route::get('google', [HomeController::class, 'google'])->name('maps.google');
    Route::get('vector', [HomeController::class, 'vector'])->name('maps.vector');
});
//Auth pages Routs
Route::group(['prefix' => 'auth'], function () {
    Route::get('signin', [HomeController::class, 'signin'])->name('auth.signin');
    Route::get('signup', [HomeController::class, 'signup'])->name('auth.signup');
    Route::get('confirmmail', [HomeController::class, 'confirmmail'])->name('auth.confirmmail');
    Route::get('lockscreen', [HomeController::class, 'lockscreen'])->name('auth.lockscreen');
    Route::get('recoverpw', [HomeController::class, 'recoverpw'])->name('auth.recoverpw');
    Route::get('userprivacysetting', [HomeController::class, 'userprivacysetting'])->name('auth.userprivacysetting');
});
//Error Page Route
Route::group(['prefix' => 'errors'], function () {
    Route::get('error404', [HomeController::class, 'error404'])->name('errors.error404');
    Route::get('error500', [HomeController::class, 'error500'])->name('errors.error500');
    Route::get('maintenance', [HomeController::class, 'maintenance'])->name('errors.maintenance');
});

//Forms Pages Routs
Route::group(['prefix' => 'forms'], function () {
    Route::get('element', [HomeController::class, 'element'])->name('forms.element');
    Route::get('wizard', [HomeController::class, 'wizard'])->name('forms.wizard');
    Route::get('validation', [HomeController::class, 'validation'])->name('forms.validation');
});

//Table Page Routs
Route::group(['prefix' => 'table'], function () {
    Route::get('bootstraptable', [HomeController::class, 'bootstraptable'])->name('table.bootstraptable');
    Route::get('datatable', [HomeController::class, 'datatable'])->name('table.datatable');
});
//Icons Page Routs
Route::group(['prefix' => 'icons'], function () {
    Route::get('solid', [HomeController::class, 'solid'])->name('icons.solid');
    Route::get('outline', [HomeController::class, 'outline'])->name('icons.outline');
    Route::get('dualtone', [HomeController::class, 'dualtone'])->name('icons.dualtone');
    Route::get('colored', [HomeController::class, 'colored'])->name('icons.colored');
});
//Extra Page Routs
Route::get('privacy-policy', [HomeController::class, 'privacypolicy'])->name('pages.privacy-policy');
Route::get('terms-of-use', [HomeController::class, 'termsofuse'])->name('pages.term-of-use');

// routes for tickets
Route::get('tickets',[TicketsController::class, 'show'])->name('tickets');
Route::post('ticket-store',[TicketsController::class,'store'])->name('ticket-store');
Route::post('update-ticket',[TicketsController::class,'update'])->name('update-ticket');
Route::get('spend-time/{id}',[TicketsController::class,'ticket_view'])->name('spend-time');
