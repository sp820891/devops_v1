<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                $user = User::find(auth()->user()->id);
            if ($user->hasPermissionTo('can access dashboard')) {
                    return redirect()->intended(RouteServiceProvider::HOME);
            } else if ($user->hasPermissionTo('can access merge requests')) {
                return redirect()->intended(RouteServiceProvider::MERGE);
            } else if ($user->hasPermissionTo('can access support requests')) {
                return redirect()->intended(RouteServiceProvider::SUPPORT);
            } else if ($user->hasPermissionTo('can access a project')) {
                return redirect()->intended(RouteServiceProvider::PROJECT);
            } else if ($user->hasPermissionTo('can access sprint')) {
                return redirect()->intended(RouteServiceProvider::SPRINT);
            } else if ($user->hasPermissionTo('can access environment')) {
                return redirect()->intended(RouteServiceProvider::ENV);
            } else if ($user->hasPermissionTo('can access priority')) {
                return redirect()->intended(RouteServiceProvider::PRIORITY);
            } else if ($user->hasPermissionTo('can access reject reasons')) {
                return redirect()->intended(RouteServiceProvider::REJECTREASONS);
            } else if ($user->hasPermissionTo('can access logs')) {
                return redirect()->intended(RouteServiceProvider::LOG);
            } else if ($user->hasPermissionTo('can access status')) {
                return redirect()->intended(RouteServiceProvider::STATUS);
            } else if ($user->hasPermissionTo('can access users')) {
                return redirect()->intended(RouteServiceProvider::USERS);
            } else if ($user->hasPermissionTo('can access a roles')) {
                return redirect()->intended(RouteServiceProvider::ROLE);
            } else {
                Auth::logout();
                return redirect("/")->withMessage('<script>toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("You Have No Permissions To Access DevOps Portal");</script>');
            }
        }

        return $next($request);
    }
}
}
