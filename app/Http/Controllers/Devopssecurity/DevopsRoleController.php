<?php

namespace App\Http\Controllers\Devopssecurity;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DevopsRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        if (request()->ajax()) {
            $role = User::find(auth()->user()->id);
            return datatables()->of(Role::select("*"))
                ->addColumn('action', function ($row) use ($role) {
                    $btn = '';


                    if ($role->hasPermissionTo('can delete roles')) {
                        $btn = '<button type="button" name="deleterole"     id="' . $row->id . '"class="deleterole text-danger " style="border:none; background:none;">
             <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" stroke="currentColor">
             <path d="M19.3248 9.46826C19.3248 9.46826 18.7818 16.2033 18.4668 19.0403C18.3168 20.3953 17.4798 21.1893 16.1088 21.2143C13.4998 21.2613 10.8878 21.2643 8.27979 21.2093C6.96079 21.1823 6.13779 20.3783 5.99079 19.0473C5.67379 16.1853 5.13379 9.46826 5.13379 9.46826" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
             <path d="M20.708 6.23975H3.75" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
             <path d="M17.4406 6.23973C16.6556 6.23973 15.9796 5.68473 15.8256 4.91573L15.5826 3.69973C15.4326 3.13873 14.9246 2.75073 14.3456 2.75073H10.1126C9.53358 2.75073 9.02558 3.13873 8.87558 3.69973L8.63258 4.91573C8.47858 5.68473 7.80258 6.23973 7.01758 6.23973" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
         </svg>
             </button>';
                    }
                    return $btn;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->setRowId(function ($data) {

                    return $data->id;
                })
                ->make(true);
        }
        return view('security.role');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'unique:' . Role::class],
        ]);
        $role = Role::create([
            'name' => $request->name,
        ]);
        if ($role) {
            return response()->json(
                [
                    'status' => 200,
                ]
            );
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $row=Role::findOrFail($id);
            return response()->json([
                'role'=>$row]
            );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {

        $role = Role::find($id)->delete();
        if ($role) {
            return response()->json(
                [
                    'roles' => $role,
                    'status' => 200,
                ]
            );
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }
}
