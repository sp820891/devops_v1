<?php

namespace App\Http\Controllers\Devopssecurity;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\ApplicationNotification;
use Illuminate\Support\Facades\Hash;

class DevopsUserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
            $roles = Role::all();

        if(request()->ajax()) {
            $user = User::find(auth()->user()->id);
            return datatables()->of(User::where('first_name' , '!=' , auth()->user()->first_name)->where('id' , '!=' , "1")->with('roles')->get())
            ->addColumn('action', function($row) use ($user) {

                    $btn = '';


                    if($user->hasPermissionTo('can edit users')) {
                        $btn =   $btn .'<button type="button" name="edit"  id="'.$row->id.'"class="edit text-primary" style="border:none; background:none;">
                        <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M11.4925 2.78906H7.75349C4.67849 2.78906 2.75049 4.96606 2.75049 8.04806V16.3621C2.75049 19.4441 4.66949 21.6211 7.75349 21.6211H16.5775C19.6625 21.6211 21.5815 19.4441 21.5815 16.3621V12.3341" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M8.82812 10.921L16.3011 3.44799C17.2321 2.51799 18.7411 2.51799 19.6721 3.44799L20.8891 4.66499C21.8201 5.59599 21.8201 7.10599 20.8891 8.03599L13.3801 15.545C12.9731 15.952 12.4211 16.181 11.8451 16.181H8.09912L8.19312 12.401C8.20712 11.845 8.43412 11.315 8.82812 10.921Z"
                        stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path><path d="M15.1655 4.60254L19.7315 9.16854" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path></svg></button>';

                    }
                    if($user->hasPermissionTo('can delete users')) {
                        $btn=   $btn .'<button type="button" name="delete"     id="'.$row->id.'"class="delete text-danger " style="border:none; background:none;">
                         <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" stroke="currentColor">
                         <path d="M19.3248 9.46826C19.3248 9.46826 18.7818 16.2033 18.4668 19.0403C18.3168 20.3953 17.4798 21.1893 16.1088 21.2143C13.4998 21.2613 10.8878 21.2643 8.27979 21.2093C6.96079 21.1823 6.13779 20.3783 5.99079 19.0473C5.67379 16.1853 5.13379 9.46826 5.13379 9.46826" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                         <path d="M20.708 6.23975H3.75" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                         <path d="M17.4406 6.23973C16.6556 6.23973 15.9796 5.68473 15.8256 4.91573L15.5826 3.69973C15.4326 3.13873 14.9246 2.75073 14.3456 2.75073H10.1126C9.53358 2.75073 9.02558 3.13873 8.87558 3.69973L8.63258 4.91573C8.47858 5.68473 7.80258 6.23973 7.01758 6.23973" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                     </svg>
                         </button></div></div>';
                    }

                    return $btn;
                })
            ->rawColumns(['action'])
            ->setRowId(function ($data) {

                return $data->id;
            })
            ->make(true);
        }
        return view('security.user',compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        $user = User::find(auth()->user()->id);
        if($user->hasPermissionTo('can access user based permissions') && $user->hasPermissionTo('can edit users') && $user->hasPermissionTo('can delete users')) {
            return response()->json([
                'status'=>200,
                'users' => User::where('first_name' , '!=' , auth()->user()->first_name)->where('id' , '!=' , "1")->with('roles')->get(),
            ]);
    }
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'gender' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:'.User::class],
            'password' => ['required', 'confirmed'],
            'role_as' => 'required',
       ]);
       $user = User::create([
        'first_name' => $request->first_name,
        'last_name' => $request->last_name,
        'gender' => $request->gender,
        'email' => $request->email,
        'password' => Hash::make($request->password),
        'created_by' => auth()->user()->fullname
        ]);
        $user->assignRole($request->role_as);
        if($user){
            return response()->json(
                [
                    'user' => $user,
                    'role' => $user->roles->pluck('name')->first(),
                    'name' => $request->first_name,
                    'email' => $request->email,
                    'created_id' => auth()->user()->id,
                    'status' => 200
                ]
            );
        }
        else{
            return response()->json([
                'status'=>404,
            ]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {

            $row=User::findOrFail($id);
            return response()->json([

                'role' => $row->roles->pluck('name')->first(),
                'status' => 200,
                'roles' => Role::all(),
                'user'=>$row]
            );


    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request,  $id)
    {
        // dd($request->all());
        $request->validate([
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'gender' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'role_as' => 'required',
       ]);
       $user = User::find($id);
       $user->update([
        'first_name' => $request->first_name,
        'last_name' => $request->last_name,
        'gender' => $request->gender,
        'email' => $request->email,
        ]);
        $user->removeRole($user->roles->pluck('name')->first());
        $user->assignRole($request->role_as);
        if($user){
            return response()->json(
                [
                    'status' => 200,
                    'user' => $user,
                    'email' => $request->email,
                ]
            );
        }
        else{
            return response()->json([
                'status'=>404,
            ]);
        }
       }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $user = User::find($id)->delete();
        if($user){
            return response()->json(
                [
                    'user' => $user,
                    'status' => 200,
                ]
            );
        }
        else{
            return response()->json([
                'status'=>404,
            ]);
        }
   }
    }

