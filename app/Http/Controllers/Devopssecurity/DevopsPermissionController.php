<?php

namespace App\Http\Controllers\Devopssecurity;

use App\Models\Role;
use App\Models\User;
use App\Models\Permission;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class DevopsPermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $user = User::find($id);
        $Permissions = $user->getPermissionsViaRoles();
        $allPermissions = Permission::all();
        return view('security.userpermission', compact('user', 'allPermissions', 'Permissions'));
    }
    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update($id)
    {
        $permission = Permission::find($id);
        return response()->json(
            [
                'permission' => $permission,
                'status' => 200,
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }


    public function fetchUserWithPermission($user_id)
    {
        $user = User::find($user_id);
        $permissions = $user->permissions->pluck('id');
        $rolePermissions = $user->getPermissionsViaRoles();
        $userPermissions = $user->getAllPermissions();
        $diffPermissions = $userPermissions->diff($rolePermissions);
        return response()->json([
            'permissions' => $permissions,
            'userPermissions' => $diffPermissions,
            'rolePermissions' => $rolePermissions,
            'status' => 200
        ]);
    }
    public function assignUserWithPermission(Request $request, $user_id)
    {
        $user = User::find($user_id);
        foreach ($request->checked as $key => $value) {
            $permission = Permission::findByName($value);
            $user->givePermissionTo($permission);
        }
        if ($request->unchecked != "") {
            foreach ($request->unchecked as $key => $value) {
                $permission = Permission::findByName($value);
                $user->revokePermissionTo($permission);
            }
        }
        return response()->json([
            'status' => 200,
        ]);
    }
    public function Rolebasedpermission($id)
    {
        $role = Role::findById($id);
        $Permissions = Permission::all();
        return view('security.rolepermission', compact('role', 'Permissions'));
    }
    public function fetchRoleWithPermission($role_name)
    {
        $role = Role::findByName($role_name);
        $permissions = $role->permissions->pluck('id');
        return response()->json([
            'permissions' => $permissions,
            'status' => 200
        ]);
    }
    public function assignRoleWithPermission(Request $request, $role_name)
    {
        $role = Role::findByName($role_name);
        foreach ($request->checked as $key => $value) {
            $permission = Permission::findByName($value);
            $role->givePermissionTo($permission);
        }
        if ($request->unchecked != "") {
            foreach ($request->unchecked as $key => $value) {
                $permission = Permission::findByName($value);
                $role->revokePermissionTo($permission);
            }
        }
        return response()->json([
            'status' => 200,
        ]);
    }
}
