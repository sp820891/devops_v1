<?php

namespace App\Http\Controllers\sprint;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\PPRulesHasDep;
use App\Models\Role;
use App\Models\Sprint;
use App\Models\SprintRetrospective;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SprintRetrospectiveController extends Controller
{
    public function sprint_retrospective()
    {
        $sprint = Sprint::all();
        $assignee = User::where('id','!=',1)->get();
        return view('sprint.sprint-retrospective',compact('sprint','assignee'));
    }
    public function create_sprint_retro(Request $request) {
        $user = User::find($request->resource);
        $user_role = $user->roles->pluck('id')->first();
        $department = Department::where('role_id',$user_role)->first();
        $pp_rule_has_dept = PPRulesHasDep::where('department_id', $department->id)->get();
        if($pp_rule_has_dept == "") {
            return response()->json([
                'status' => 'department_and_rule_is_not_assigned',
            ]);
        }
        else {
            $sprint_retro_insert_prevent = SprintRetrospective::where('sprint_id','=',$request->sprint)->where( 'user_id','=',$request->resource)->first();
            if($sprint_retro_insert_prevent == "") {
                foreach ($pp_rule_has_dept as $key => $val) {
                    $sprint_retro = new SprintRetrospective;
                    $sprint_retro->sprint_id = $request->sprint;
                    $sprint_retro->user_id = $request->resource;
                    $sprint_retro->pp_rule_id = $val->powerpoint_rule_id;
                    $sprint_retro->power_points = $val->power_points;
                    $sprint_retro->save();
                }
                return response()->json([
                    'status' => 'sprint_retro_is_created',
                    'sprint' => $request->sprint,
                    'resource' => $request->resource

                ]);
            }
            else {
                return response()->json([
                    'status' => 'sprint_retro_is_already_created',
                ]);
            }
        }
       }
    public function fetch_sprint_retro(Request $request, $from) {
        $sprint_retro_count_sprint = SprintRetrospective::select(DB::raw('COUNT(sprint_id) as sprint_count'),'sprint_id')->groupBy('sprint_id')->get();
        $sprint_retro_count_emp = SprintRetrospective::select(DB::raw('COUNT(user_id) as emp_count'),'user_id','sprint_id')->groupBy('sprint_id','user_id')->get();
        if ($from == "fetch_default") {
            $user = User::find(auth()->user()->id);
            if($user->hasAnyRole('DevOps Admin','DevOps Manager')) {
                $sprint_retro = SprintRetrospective::with('users','sprint','rules')->orderBy('sprint_id','desc')->get();
                return response()->json([
                    'sprint_retro' => $sprint_retro,
                    'status' => 'access_all_field',
                    'sprint_retro_count_sprint' => $sprint_retro_count_sprint,
                    'sprint_retro_count_emp' => $sprint_retro_count_emp
                ]);
            }
            else {
                $sprint_retro = SprintRetrospective::with('sprint','rules')->where('user_id',auth()->user()->id)->orderBy('sprint_id','desc')->get();
                $sprint_retro_count_sprint = SprintRetrospective::select(DB::raw('COUNT(sprint_id) as sprint_count'),'sprint_id')->where('user_id',auth()->user()->id)->groupBy('sprint_id')->get();
                return response()->json([
                    'sprint_retro' => $sprint_retro,
                    'status' => 'cannot_access_all_field',
                    'sprint_retro_count_sprint' => $sprint_retro_count_sprint,
                ]);
            }
        }
        else if($from == "fetch_create" || $from == "fetch_view"){
            $sprint_retro = SprintRetrospective::with('users','sprint','rules')->where('sprint_id',$request->sprint)->where('user_id',$request->resource)->get();
            return response()->json([
                'sprint_retro' => $sprint_retro,
                'sprint_retro_count_emp' => $sprint_retro_count_emp
            ]);
        }
        else {
            $sprint_retro = SprintRetrospective::with('users','sprint','rules')->where('sprint_id',$request->sprint)->where('user_id',auth()->user()->id)->get();
            return response()->json([
                'sprint_retro' => $sprint_retro,
            ]);
        }
    }
}
