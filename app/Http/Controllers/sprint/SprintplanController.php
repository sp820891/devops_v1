<?php

namespace App\Http\Controllers\sprint;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Sprint;
use App\Models\Ticket;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Models\SprintPlanning;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use PhpOffice\PhpSpreadsheet\Calculation\MathTrig\Sum;
use Yajra\DataTables\Facades\DataTables;

class SprintplanController extends Controller
{
    public function sprint_planning(Request $request)
    {


        if ($request->ajax()) {
            $today_date = Carbon::now();
            $sprint =  Sprint::whereDate('start_date', '<=', $today_date)->whereDate('end_date', '>=', $today_date)->first();
            $data = Ticket::select(
                'projects.id as projectid',
                'projects.name as project',
                'sprints.id',
                'users.id',
                'sprints.name as sprint_name',
                'sprints.start_date as sprint_start_date',
                'sprints.end_date as sprint_end_date',
                DB::raw("CONCAT(users.first_name,' ',users.last_name) AS full_name")
            )
                ->join('projects', 'projects.id', '=', 'tickets.project_id')
                ->join('sprints', 'sprints.id', '=', 'tickets.sprint_id')
                ->join('users', 'users.id', '=', 'tickets.created_by')
                ->join('spend_times', 'spend_times.ticket_id', '=', 'tickets.id')
                ->groupby('projectid', 'sprints.id', 'users.id', 'project', 'sprint_name', 'sprint_start_date', 'sprint_end_date', 'full_name')
                ->where('tickets.sprint_id', $sprint->id)
                ->get();

            // DB::select("select `spend_times`.`spend_time` as spendtimes , spend_times.estimate_time as estimatetime,
            // CONCAT(tickets.subject,'/',tickets.issue_no)as sprintgoal, project_name , sprint_name, CONCAT(users_firstname,' ',user_lastname)
            // as fullname, project_id,sprint_start_date,sprint_end_date from `tickets` inner join `spend_times` on `tickets`.`id`
            // = `spend_times`.`ticket_id` inner join (select `projects`.`name` as `project_name`, projects.id from `projects`)
            // as `projects` on  projects.id = `tickets`.`project_id` inner join (select `sprints`.`name` as `sprint_name` , sprints.id,
            // sprints.start_date as sprint_start_date, sprints.end_date as sprint_end_date from `sprints`) as `sprints` on `sprints`.`id`
            // = `tickets`.`sprint_id` inner join (select `users`.`first_name` as `users_firstname`,users.last_name as user_lastname,users.id
            // from `users`) as `users` on `users`.`id` = `tickets`.`created_by` GROUP BY spendtimes,sprint_name, fullname, project_id
            // ,sprint_start_date,sprint_end_date,spend_times.estimate_time,sprintgoal");


            return DataTables::of($data)
                ->setRowId(function ($data) {
                    return $data->projectid;
                })
                ->addColumn('Sprint', function ($data) {

                    return  $data->sprint_name . '(' . \Carbon\Carbon::parse($data->sprint_start_date)->isoFormat('MMM  D') . '-' . \Carbon\Carbon::parse($data->sprint_end_date)->isoFormat('MMM  D') . ')';
                })
                ->rawColumns(['Sprint'])
                ->make(true);
        }

        return view('sprint.sprint-plans',);
    }
    public  function show($id)
    {
        // $today_date = Carbon::now();
        // $sprint =  Sprint::whereDate('start_date', '<=', $today_date)->whereDate('end_date', '>=', $today_date)->first();
        if (request()->ajax()) {
            $today_date = Carbon::now();
            $sprint =  Sprint::whereDate('start_date', '<=', $today_date)->whereDate('end_date', '>=', $today_date)->first();

            $data = Ticket::select(
                'projects.id as projectid',
                'projects.name as project',
                'sprints.id',
                'users.id',
                'sprints.id',
                'sprints.name as sprint_name',
                'sprints.start_date as sprint_start_date',
                'sprints.end_date as sprint_end_date',
                'tickets.estimate_time as estimate_time',
                DB::raw("CONCAT(tickets.subject,'/',tickets.issue_no) AS sprintgoal"),
                // users table
                DB::raw("CONCAT(users.first_name,' ',users.last_name) AS full_name"),
                // spendtimes table
                DB::raw("SUM(spend_times.spend_time) AS spendtime")


            )
                ->join('projects', 'projects.id', '=', 'tickets.project_id')
                ->join('sprints', 'sprints.id', '=', 'tickets.sprint_id')
                ->join('users', 'users.id', '=', 'tickets.assignee')
                ->join('spend_times', 'spend_times.ticket_id', '=', 'tickets.id')
                ->where('tickets.project_id', $id)->where('tickets.sprint_id', $sprint->id)

                ->groupby('projectid', 'sprints.id', 'users.id', 'project', 'sprint_name', 'sprint_start_date', 'sprint_end_date', 'full_name','sprintgoal','estimate_time')
                ->get();
            Log::info("GET Data : " . json_encode($data));

            return datatables()->of($data)

                ->addColumn('Sprint', function ($data) {

                    return  $data->sprint_name . '(' . \Carbon\Carbon::parse($data->sprint_start_date)->isoFormat('MMM  D') . '-' . \Carbon\Carbon::parse($data->sprint_end_date)->isoFormat('MMM  D') . ')';
                })
                ->make(true);
        }
        return view('sprint.sprint-view', compact('id'));
    }
}
