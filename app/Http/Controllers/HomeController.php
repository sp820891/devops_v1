<?php
namespace App\Http\Controllers;

use App\Events\NotificationEvent;
use App\Models\Mergerequest;
use App\Models\priority;
use App\Models\Releaseenv;
use App\Models\Status;
use App\Models\Project;
use App\Models\supportDoucument;
use DataTables;
use Illuminate\Http\Request;
use App\Models\Audit;
use App\Models\Releaselog;
use App\Models\Support;
use App\Models\SupportRequestStatus;
use App\Models\User;
use App\Notifications\ApplicationNotification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    /*
     * Dashboard Pages Routs
     */
    public function index(Request $request)
    {
        $assets = ['chart', 'animation'];
        $mergerequest_count = Mergerequest::count();
        $support_request_count = Support::count();
        $test_release_done_count = Mergerequest::sum('test_release_done_count');
        $prod_release_done_count = Mergerequest::sum('prod_release_done_count');
        $test_release_request_count = Mergerequest::sum('test_release_request_count');
        $prod_release_request_count = Mergerequest::sum('prod_release_request_count');
        $test_reject_count = Mergerequest::sum('test_reject_count');
        $prod_reject_count = Mergerequest::sum('prod_reject_count');
        return view('dashboards.dashboard', compact('assets','mergerequest_count','test_release_done_count',
        'prod_release_done_count','test_reject_count','prod_reject_count','test_release_request_count','prod_release_request_count','support_request_count'));
    }
    public function fetchDasboardMergeRequests() {
        $test_release = DB::select("SELECT SUM(test_release_done_count) as count , MONTH(test_release_date) as month FROM `mergerequests` WHERE `test_release_date` IS NOT NULL AND YEAR(test_release_date) = YEAR(CURRENT_DATE) GROUP BY month;");
        $prod_release = DB::select("SELECT SUM(prod_release_done_count) as count , MONTH(prod_release_date) as month FROM `mergerequests` WHERE `prod_release_date` IS NOT NULL AND YEAR(prod_release_date) = YEAR(CURRENT_DATE) GROUP BY month;");
        return response()->json([
            'status' => 200,
            'test_release' => $test_release,
            'prod_release' => $prod_release
        ]);
    }
    public function fetchDashboardReleaseRequests() {
        $test_release = Mergerequest::sum('test_release_request_count');
        $prod_release = Mergerequest::sum('prod_release_request_count');
        return response()->json([
            'status' => 200,
            'test_release' => $test_release,
            'prod_release' => $prod_release
        ]);
    }
    public function fetchDashboardSupportRequests() {
        $supportrequest_count = DB::select("SELECT COUNT(id) as count , MONTH(created_at) as month FROM `supportrequest` WHERE YEAR(created_at) = YEAR(CURRENT_DATE) GROUP BY month;");
        return response()->json([
            'status' => 200,
            'supportrequest_count' => $supportrequest_count,
        ]);
    }
    public function mergerequest()
    {
        $statuses = Status::all();
        $priorities = priority::all();
        $releaseenvs = Releaseenv::all();
        return view('dashboards.mergerequest',compact('statuses','priorities','releaseenvs'));
    }
    public function supportrequest()
    {
        $project=Project::all();
        $status= SupportRequestStatus::all();
        $priority=priority::all();
        $env=Releaseenv::all();
        $supportdocument=supportDoucument::all();

        return view('dashboards.supportrequest',compact('project','status','priority','env','supportdocument'));
    }
    public function Logs() {
        $data = Releaselog::with('users','mergerequest','reason','mergerequest.status')->get();
        if(request()->ajax()) {
            return datatables()->of($data)
            ->addColumn('action', function($row){
                $actionBtn = '<button type="button" name="deleteReleaseLog"  id="'.$row->id.'"class="deleteReleaseLog text-danger " style="border:none; background:none;">
                 <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" stroke="currentColor">
                 <path d="M19.3248 9.46826C19.3248 9.46826 18.7818 16.2033 18.4668 19.0403C18.3168 20.3953 17.4798 21.1893 16.1088 21.2143C13.4998 21.2613 10.8878 21.2643 8.27979 21.2093C6.96079 21.1823 6.13779 20.3783 5.99079 19.0473C5.67379 16.1853 5.13379 9.46826 5.13379 9.46826" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                 <path d="M20.708 6.23975H3.75" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                 <path d="M17.4406 6.23973C16.6556 6.23973 15.9796 5.68473 15.8256 4.91573L15.5826 3.69973C15.4326 3.13873 14.9246 2.75073 14.3456 2.75073H10.1126C9.53358 2.75073 9.02558 3.13873 8.87558 3.69973L8.63258 4.91573C8.47858 5.68473 7.80258 6.23973 7.01758 6.23973" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
             </svg>
                 </button>';
                return $actionBtn;
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
         return view('logs.logs');
    }
    public function notification_channel (Request $request) {
        $notify_id = $request->notify_id;
        $message = $request->message;
        $created_by = $request->created_by;
        $url_name = $request->url_name;
        $notification_from = $request->notification_from;
        $created_id = $request->created_id;
        broadcast(new NotificationEvent($notify_id,$url_name,$message,$created_by,$notification_from,$created_id))->toOthers();
    }
    public function fetchAppNotification(){
        return response()->json([
            'notifications' => auth()->user()->unreadNotifications->take(4),
            'url' => env("APP_URL"),
            'notifications_count' => auth()->user()->unreadNotifications->count(),
        ]);
    }
    public function application_notification(Request $request) {
        $notify_id = $request->notify_id;
        $message = $request->message;
        $created_by = $request->created_by;
        $url_name = $request->url_name;
        $notification_from = $request->notification_from;
        $created_id = $request->created_id;
        if(auth()->user()) {
            $users = User::where('id','!=',$created_id)->get();
            foreach ($users as $key => $user) {
                if($notification_from == "User_Create") {
                    if($user->hasPermissionTo('can access users')) {
                        $user->notify(new ApplicationNotification($notify_id,$url_name,$message,$created_by));
                    }
                }
                else if($notification_from == "MergeRequest_Create" || $notification_from == "MergeRequest_Edit") {
                    if($user->hasRole('DevOps Admin') || $user->hasRole('DevOps Engineer') || $user->hasRole('DevOps QA')) {
                        $user->notify(new ApplicationNotification($notify_id,$url_name,$message,$created_by));
                    }
                }
                else if($notification_from == "MergeRequest_QA" || $notification_from == "MergeRequest_Engineer") {
                    if($user->hasRole('DevOps Admin') || $user->hasRole('DevOps Engineer') || $user->hasRole('DevOps Developer')) {
                        $user->notify(new ApplicationNotification($notify_id,$url_name,$message,$created_by));
                    }
                }
                else if($notification_from == "SupportRequest_Create" || $notification_from == "SupportRequest_Edit") {
                    if($user->hasRole('DevOps Admin') || $user->hasRole('DevOps Engineer')) {
                        $user->notify(new ApplicationNotification($notify_id,$url_name,$message,$created_by));
                    }
                }
                else if($notification_from == "SupportRequest_Engineer") {
                    if($user->hasRole('DevOps Admin') || $user->hasRole('DevOps Engineer') || $user->hasRole('DevOps Support')) {
                        $user->notify(new ApplicationNotification($notify_id,$url_name,$message,$created_by));
                    }
                }
            }
        }
        else {
            $users = User::all();
            foreach ($users as $key => $user) {
                if($notification_from == "User_Create") {
                    if($user->hasPermissionTo('can access users')) {
                        $user->notify(new ApplicationNotification($notify_id,$url_name,$message,$created_by));
                    }
                }
                else if($notification_from == "MergeRequest_Create" || $notification_from == "MergeRequest_Edit") {
                    if($user->hasRole('DevOps Admin') || $user->hasRole('DevOps Engineer') || $user->hasRole('DevOps QA')) {
                        $user->notify(new ApplicationNotification($notify_id,$url_name,$message,$created_by));
                    }
                }
                else if($notification_from == "MergeRequest_QA" || $notification_from == "MergeRequest_Engineer") {
                    if($user->hasRole('DevOps Admin') || $user->hasRole('DevOps Engineer') || $user->hasRole('DevOps Developer')) {
                        $user->notify(new ApplicationNotification($notify_id,$url_name,$message,$created_by));
                    }
                }
                else if($notification_from == "SupportRequest_Create" || $notification_from == "SupportRequest_Edit") {
                    if($user->hasRole('DevOps Admin') || $user->hasRole('DevOps Engineer')) {
                        $user->notify(new ApplicationNotification($notify_id,$url_name,$message,$created_by));
                    }
                }
                else if($notification_from == "SupportRequest_Engineer") {
                    if($user->hasRole('DevOps Admin') || $user->hasRole('DevOps Engineer') || $user->hasRole('DevOps Support')) {
                        $user->notify(new ApplicationNotification($notify_id,$url_name,$message,$created_by));
                    }
                }
            }
        }
        return response()->json([
            'notifications' => auth()->user()->unreadNotifications->take(4),
            'url' => env("APP_URL"),
            'notifications_count' => auth()->user()->unreadNotifications->count(),
        ]);
    }
    public function application_notification_markasread($id) {
         auth()->user()->notifications->where('id',$id)->markAsRead();
    }
    public function notifications_view() {
        return view('notifications.notifications');
    }
    /*
     * Menu Style Routs
     */
    public function horizontal(Request $request)
    {
        $assets = ['chart', 'animation'];
        return view('menu-style.horizontal',compact('assets'));
    }
    public function dualhorizontal(Request $request)
    {
        $assets = ['chart', 'animation'];
        return view('menu-style.dual-horizontal',compact('assets'));
    }
    public function dualcompact(Request $request)
    {
        $assets = ['chart', 'animation'];
        return view('menu-style.dual-compact',compact('assets'));
    }
    public function boxed(Request $request)
    {
        $assets = ['chart', 'animation'];
        return view('menu-style.boxed',compact('assets'));
    }
    public function boxedfancy(Request $request)
    {
        $assets = ['chart', 'animation'];
        return view('menu-style.boxed-fancy',compact('assets'));
    }
    /*
     * Pages Routs
     */
    public function billing(Request $request)
    {
        return view('special-pages.billing');
    }
    public function calender(Request $request)
    {
        $assets = ['calender'];
        return view('special-pages.calender',compact('assets'));
    }
    public function kanban(Request $request)
    {
        return view('special-pages.kanban');
    }
    public function pricing(Request $request)
    {
        return view('special-pages.pricing');
    }
    public function rtlsupport(Request $request)
    {
        return view('special-pages.rtl-support');
    }
    public function timeline(Request $request)
    {
        return view('special-pages.timeline');
    }

    /*
     * Widget Routs
     */
    public function widgetbasic(Request $request)
    {
        return view('widget.widget-basic');
    }
    public function widgetchart(Request $request)
    {
        $assets = ['chart'];
        return view('widget.widget-chart', compact('assets'));
    }
    public function widgetcard(Request $request)
    {
        return view('widget.widget-card');
    }
    /*
     * Maps Routs
     */
    public function google(Request $request)
    {
        return view('maps.google');
    }
    public function vector(Request $request)
    {
        return view('maps.vector');
    }
    /*
     * Auth Routs
     */
    public function signin(Request $request)
    {
        return view('auth.login');
    }
    public function signup(Request $request)
    {
        return view('auth.register');
    }
    public function confirmmail(Request $request)
    {
        return view('auth.confirm-mail');
    }
    public function lockscreen(Request $request)
    {
        return view('auth.lockscreen');
    }
    public function recoverpw(Request $request)
    {
        return view('auth.recoverpw');
    }
    public function userprivacysetting(Request $request)
    {
        return view('auth.user-privacy-setting');
    }
    /*
     * Error Page Routs
     */
    public function error404(Request $request)
    {
        return view('errors.error404');
    }
    public function error500(Request $request)
    {
        return view('errors.error500');
    }
    public function maintenance(Request $request)
    {
        return view('errors.maintenance');
    }
    /*
     * uisheet Page Routs
     */
    public function uisheet(Request $request)
    {
        return view('uisheet');
    }
    /*
     * Form Page Routs
     */
    public function element(Request $request)
    {
        return view('forms.element');
    }
    public function wizard(Request $request)
    {
        return view('forms.wizard');
    }
    public function validation(Request $request)
    {
        return view('forms.validation');
    }
     /*
     * Table Page Routs
     */
    public function bootstraptable(Request $request)
    {
        return view('table.bootstraptable');
    }
    public function datatable(Request $request)
    {
        return view('table.datatable');
    }
    /*
     * Icons Page Routs
     */
    public function solid(Request $request)
    {
        return view('icons.solid');
    }
    public function outline(Request $request)
    {
        return view('icons.outline');
    }
    public function dualtone(Request $request)
    {
        return view('icons.dualtone');
    }
    public function colored(Request $request)
    {
        return view('icons.colored');
    }
    /*
     * Extra Page Routs
     */
    public function privacypolicy(Request $request)
    {
        return view('privacy-policy');
    }
    public function termsofuse(Request $request)
    {
        return view('terms-of-use');
    }
}
