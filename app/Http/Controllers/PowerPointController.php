<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\PowerPointComments;
use App\Models\PowerPointRule;
use App\Models\PPRulesHasDep;
use App\Models\SprintRetrospective;
use App\Models\User;
use Illuminate\Http\Request;

class PowerPointController extends Controller
{
    public function power_point_comment()
    {
        $data = PowerPointComments::with('users')->get();
        if (request()->ajax()) {
            return datatables()->of($data)
                ->addColumn('action', function ($row) {
                    $actionBtn = '<button type="button" name="deletecomment"  id="' . $row->id . '"class="deletecomment text-danger " style="border:none; background:none;">
                 <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" stroke="currentColor">
                 <path d="M19.3248 9.46826C19.3248 9.46826 18.7818 16.2033 18.4668 19.0403C18.3168 20.3953 17.4798 21.1893 16.1088 21.2143C13.4998 21.2613 10.8878 21.2643 8.27979 21.2093C6.96079 21.1823 6.13779 20.3783 5.99079 19.0473C5.67379 16.1853 5.13379 9.46826 5.13379 9.46826" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                 <path d="M20.708 6.23975H3.75" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                 <path d="M17.4406 6.23973C16.6556 6.23973 15.9796 5.68473 15.8256 4.91573L15.5826 3.69973C15.4326 3.13873 14.9246 2.75073 14.3456 2.75073H10.1126C9.53358 2.75073 9.02558 3.13873 8.87558 3.69973L8.63258 4.91573C8.47858 5.68473 7.80258 6.23973 7.01758 6.23973" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
             </svg>
                 </button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('sprint.powerpointsystem.powerpoint_comments');
    }

    public function power_point_comment_store(Request $request)
    {
        $request->validate([
            'PowerpointComment' => 'required|string|max:191|unique:power_point_comments,power_point_comments',
        ]);
        $powerpointcomment = new PowerPointComments();
        $powerpointcomment->power_point_comments = $request->input('PowerpointComment');
        $powerpointcomment->created_by = auth()->user()->id;
        $powerpointcomment->save();
        if ($powerpointcomment) {
            return response()->json([
                'status' => 200,
            ],);
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }
    public function deletePowerpointComment($id)
    {
        $pp = PowerPointComments::find($id);
        return response()->json(
            [
                'powerpoint' => $pp,
                'status' => 200
            ]
        );
    }
    public function deletePPComment($id)
    {
        $pp = PowerPointComments::find($id);
        if ($pp) {
            $pp->delete();
            return response()->json([
                'status' => 200,
            ]);
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }

    public function power_points_system(){

        $department = Department::all();
        $pprules = PowerPointRule::all();
        return view('sprint.powerpointsystem.power_points_system',compact('department','pprules'));
    }


    // For PP_Rules has Departments
    public function PP_rules_PP_default() {
        $pp_rules = PowerPointRule::all();
        $pp_rules_assigned_default = PPRulesHasDep::where('department_id',1)->get();
        return response()->json([
            'pp_rules' => $pp_rules,
            'pp_rules_assigned_default' => $pp_rules_assigned_default
        ]);
    }
    public function fetchAssignedPPRulesFromDepartment($id){
        $pp_rule_has_dep = PPRulesHasDep::where('department_id',$id)->get();
        return response()->json([
            'pp_rule_has_dep' => $pp_rule_has_dep
        ]);
    }
    public function assign_pp_rules_has_departments(Request $request) {
        $department_id = $request->department_id;
        $rules_has_pp_checked = $request->rules_has_pp_checked;
        $rules_has_pp_unchecked = $request->rules_has_pp_unchecked;
        if($department_id == NULL) {
            return response()->json([
                'status' => 'dep_id_not_exists'
            ]);
        }
        else if($rules_has_pp_checked == NULL) {
            return response()->json([
                'status' => 'pp_rules_not_exists'
            ]);
        }
        else {
            foreach ($rules_has_pp_checked as $key => $val) {
            $pp_rule_has_dep_assign = PPRulesHasDep::where('department_id',$department_id[0])->where('powerpoint_rule_id',$val[0])->first();
            if($pp_rule_has_dep_assign == ""){
                $pp_rule_has_dep = new PPRulesHasDep();
                $pp_rule_has_dep->department_id = $department_id[0];
                $pp_rule_has_dep->powerpoint_rule_id = $val[0];
                $pp_rule_has_dep->power_points = $val[1];
                $pp_rule_has_dep->save();
            }
            else {
                $pp_rule_has_dep_assign->power_points = $val[1];
                $pp_rule_has_dep_assign->update();
            }
            }
            if($rules_has_pp_unchecked != NULL) {
            foreach ($rules_has_pp_unchecked as $key => $val) {
                $pp_rule_has_dep_unassign = PPRulesHasDep::where('department_id',$department_id[0])->where('powerpoint_rule_id',$val[0])->first();
                if($pp_rule_has_dep_unassign) {
                $pp_rule_has_dep_unassign->delete();
                }
            }
            }
            return response()->json([
                'status' => 200
            ]);
        }
    }
    }
