<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Sprint;
use App\Models\Status;
use App\Models\Project;
use App\Models\Document;
use App\Models\priority;
use App\Models\Releaseenv;
use App\Models\Releaselog;
use App\Models\Mergerequest;
use App\Models\RejectReason;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class MergerequestController extends Controller
{
    public function addmerge(Request $request)
    {
        $request->validate([
            'issue_no' => 'required',
        ]);
        $merge = new Mergerequest();
        $user = Auth::user()->id;
        $merge->user_id = $user;
        $merge->project_id = 1;
        if ($merge->git_test_link = $request->input('git_test_link')) {
            $merge->status_id = 1;
            $merge->test_release_request_count = 1;
        }
        $merge->sql_update_link = $request->input('sql_update_link');
        $merge->env_update_link = $request->input('env_update_link');

        $merge->priority_id = 1;
        $merge->releaseenv_id = 1;
        $today_date = Carbon::now();
        $sprint = Sprint::whereDate('start_date', '<=', $today_date)->whereDate('end_date', '>=', $today_date)->first();
        $merge->sprint_id = $sprint->id;
        $merge->artisan_command = $request->input('artisan_command');
        $merge->issue_url = $request->input('issue_no');
        $merge->save();
        if ($request->file) {
            foreach ($request->file as $key => $value) {
                $document = new Document();
                $fileName = time() . '-' . $value->getClientOriginalName();
                $value->move(public_path("uploads"), $fileName);
                $document->path = $fileName;
                $document->mergerequest_id = $merge->id;
                $document->created_by = $user;
                $document->save();
            }
        }
        return response()->json([
            'merge' => Mergerequest::with('user')->find($merge->id),
            'created_id' => auth()->user()->id,
            'status' => 200
        ]);
    }
    public function fetchMerge()
    {

        if (request()->ajax()) {
            $user = User::find(auth()->user()->id);
            $data = Mergerequest::with('projects', 'user', 'status')->orderBy('created_at', 'desc')->get();
            return datatables()->of($data)
                ->addColumn('action', function ($row) use ($user) {

                    $btn = '';

                    if ($user->hasPermissionTo('can edit merge requests')) {
                        $btn =   $btn . '<button type="button" name="edit"  id="' . $row->id . '"class="edit text-primary" style="border:none; background:none;">
                        <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M11.4925 2.78906H7.75349C4.67849 2.78906 2.75049 4.96606 2.75049 8.04806V16.3621C2.75049 19.4441 4.66949 21.6211 7.75349 21.6211H16.5775C19.6625 21.6211 21.5815 19.4441 21.5815 16.3621V12.3341" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M8.82812 10.921L16.3011 3.44799C17.2321 2.51799 18.7411 2.51799 19.6721 3.44799L20.8891 4.66499C21.8201 5.59599 21.8201 7.10599 20.8891 8.03599L13.3801 15.545C12.9731 15.952 12.4211 16.181 11.8451 16.181H8.09912L8.19312 12.401C8.20712 11.845 8.43412 11.315 8.82812 10.921Z"
                        stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path><path d="M15.1655 4.60254L19.7315 9.16854" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path></svg></button>';
                    }
                    if ($user->hasPermissionTo('can delete merge requests')) {
                        $btn =   $btn . '<button type="button" name="delete"     id="' . $row->id . '"class="delete text-danger " style="border:none; background:none;">
                         <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" stroke="currentColor">
                         <path d="M19.3248 9.46826C19.3248 9.46826 18.7818 16.2033 18.4668 19.0403C18.3168 20.3953 17.4798 21.1893 16.1088 21.2143C13.4998 21.2613 10.8878 21.2643 8.27979 21.2093C6.96079 21.1823 6.13779 20.3783 5.99079 19.0473C5.67379 16.1853 5.13379 9.46826 5.13379 9.46826" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                         <path d="M20.708 6.23975H3.75" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                         <path d="M17.4406 6.23973C16.6556 6.23973 15.9796 5.68473 15.8256 4.91573L15.5826 3.69973C15.4326 3.13873 14.9246 2.75073 14.3456 2.75073H10.1126C9.53358 2.75073 9.02558 3.13873 8.87558 3.69973L8.63258 4.91573C8.47858 5.68473 7.80258 6.23973 7.01758 6.23973" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                     </svg>
                         </button></div></div>';
                    }

                    return $btn;
                })
                ->rawColumns(['action'])

                ->setRowId(function ($data) {
                    return $data->id;
                })
                ->make(true);
        }
        return response()->json([
            'status' => 200,
        ]);
    }

    public function editMerge($id)
    {
        $merge = Mergerequest::with('release_env', 'status', 'priority')->find($id);
        return response()->json(
            [
                'merge' => $merge,
                'statuses' => Status::all(),
                'priority' => priority::all(),
                'releaseenv' => Releaseenv::all(),
                'status' => 200
            ]
        );
    }
    public function updateMerge(Request $request, $id)
    {
        $user = Auth::user()->id;
        $merge = Mergerequest::with('user')->find($id);

        if ($merge->status_id == 4) {
            $merge->git_prod_link = $request->input('git_prod_link');
            $merge->prod_release_request_count = $merge->prod_release_request_count + 1;
            $merge->status_id = 2;
        }

        if ($merge->status_id == 5) {
            $merge->git_prod_link = $request->input('git_prod_link');
            $merge->prod_release_request_count = $merge->prod_release_request_count + 1;
            $merge->status_id = 2;
        }
        if ($merge->status_id == 7) {
            $merge->git_prod_link = $request->input('git_prod_link');
            $merge->prod_release_request_count = $merge->prod_release_request_count + 1;
            $merge->status_id = 2;
        }
        if ($merge->status_id == 6) {
            $merge->git_test_link = $request->input('git_test_link_edit');
            $merge->test_release_request_count = $merge->test_release_request_count + 1;
            $merge->status_id = 1;
        }

        $merge->priority_id = $request->input('priority');
        $merge->releaseenv_id = $request->input('release_env');
        $merge->artisan_command = $request->input('artisan_command');
        $merge->issue_url = $request->input('issue_url');
        $merge->update();
        if ($request->file) {
            foreach ($request->file as $key => $value) {
                $document = new Document();
                $fileName = time() . '-' . $value->getClientOriginalName();
                $value->move(public_path("uploads"), $fileName);
                $document->path = $fileName;
                $document->mergerequest_id = $merge->id;
                $document->created_by = $user;
                $document->save();
            }
        }
        return response()->json([
            'status' => 200,
            'merge' => $merge,
            'created_id' => auth()->user()->id,
        ]);
    }

    public function deleteMerge($id)
    { {
            $merge = Mergerequest::find($id);
            // Releaselog::where('mergerequest_id',$id)->delete();
            $documents =  Document::where('mergerequest_id', $id);
            // foreach($documents as $key=>$val) {
            //     if(File::exists($val->path)) {
            //         File::delete($val->path);
            //     }
            // }
            $documents->delete();
            $merge->delete();
            return response()->json([
                'status' => 200,
            ]);
        }
    }
    public function mergeview($id)
    {
        $merge = Mergerequest::find($id);
        $sprint = Sprint::join('mergerequests', 'mergerequests.sprint_id', '=', 'sprints.id')->where('mergerequests.id', $id)->first();
        $project = Project::join('mergerequests', 'mergerequests.project_id', '=', 'projects.id')->where('mergerequests.id', $id)->first();
        $priority = Priority::join('mergerequests', 'mergerequests.priority_id', '=', 'priorities.id')->where('mergerequests.id', $id)->first();
        $env = Releaseenv::join('mergerequests', 'mergerequests.releaseenv_id', '=', 'releaseenvs.id')->where('mergerequests.id', $id)->first();
        $status = Status::join('mergerequests', 'mergerequests.status_id', '=', 'statuses.id')->where('mergerequests.id', $id)->first();
        $reject = RejectReason::all();
        return view('dashboards.mergeview', compact('merge', 'sprint', 'project', 'priority', 'env', 'status', 'reject', 'id'));
    }

    public function QaApprove($id)
    {
        // test done
        $merge = Mergerequest::find($id);
        $user = Auth::user()->id;

        $merge->quality_report = "Test Release by-$user";
        if ($merge->status_id == 3) {
            $merge->status_id = 4;
            $merge->test_release_done_count= $merge-> test_release_done_count+1;
            $merge->test_release_date= Carbon::today();
            $merge->update();
        }

        return response()->json([
            'status' => 200,
            'merge' => $merge,
            'approved_by' => auth()->user()->full_name,
            'created_id' => auth()->user()->id,
        ]);
    }



    public function MergeApprove(Request $request, $id)
    {
        $merge = Mergerequest::find($id);
        $user = Auth::user()->id;
        $merge->quality_score = $request->input('quality_score');

        // merge done//
        if ($merge->status_id == 1) {
            $merge->status_id = 3;
            $merge->test_release_request_count = $merge->test_release_request_count - 1;
            $merge->engineer_report = "Merge Done by-$user";
            $merge->update();
            return response()->json([
                'status' => 200,
                'merge' => $merge,
                'approved_by' => auth()->user()->full_name,
                'created_id' => auth()->user()->id,
            ]);
                  // production done

        } elseif ($merge->status_id == 2) {
            $merge->prod_release_date = Carbon::today();
            $merge->prod_release_done_count = $merge->prod_release_done_count +1;
            $merge->prod_release_request_count = $merge->prod_release_request_count - 1;
            $merge->status_id = 5;
            $merge->engineer_report = "Prod Release by-$user";
            $merge->update();
            return response()->json([
                'status' => 200,
                'merge' => $merge,
                'approved_by' => auth()->user()->full_name,
                'created_id' => auth()->user()->id,
            ]);
        }



        return response()->json([
            'status' => 200,
            'merge' => $merge,
            'approved_by' => auth()->user()->full_name,
            'created_id' => auth()->user()->id,
        ]);
    }
    public function QaReject(Request $request, $id)
    {
        // test reject
        $merge = Mergerequest::find($id);
        $user = Auth::user()->id;
        if ($merge->status_id == 3) {
            $merge->status_id = 6;
            $merge->quality_report = "Test Rejected by-$user";
            $merge->test_reject_count=  $merge->test_reject_count+1;
            $merge->update();
            $comment = new Comment();
            $comment->mergerequest_id = $id;
            $comment->comment = $request->input('comment');
            $comment->created_by = $user;
            $comment->save();
            $releaseLogs = new Releaselog();
            $releaseLogs->mergerequest_id = $merge->id;
            $releaseLogs->rejectreason_id  = 1;
            $releaseLogs->rejected_by = auth()->user()->id;
            $releaseLogs->save();

        } elseif ($merge->status_id == 6) {
            $merge->status_id = 1;
            $merge->update();
        }

        return response()->json([
            'status' => 200,
            'merge' => $merge,
            'rejected_by' => auth()->user()->full_name,
            'created_id' => auth()->user()->id,
        ]);
    }
    public function MergeReject(Request $request, $id)
    {
        // merge reject
        $reject = new Releaselog();
        $merge = Mergerequest::find($id);
        $user = Auth::user()->id;
        if ($merge->status_id == 1) {
            $merge->status_id = 6;
            $comment = new Comment();
            $comment->mergerequest_id = $id;
            $comment->comment = $request->input('comment');
            $comment->created_by = $user;
            $comment->save();
            $merge->quality_report = "Test Rejected by-$user";
            $merge->test_reject_count=  $merge->test_reject_count+1;
            $merge->test_release_request_count = $merge->test_release_request_count - 1;
            $merge->update();
            $releaseLogs = new Releaselog();
            $releaseLogs->mergerequest_id = $merge->id;
            $releaseLogs->rejectreason_id  = 1;
            $releaseLogs->rejected_by = auth()->user()->id;
            $releaseLogs->save();

      }
       elseif ($merge->status_id == 6) {
            $merge->status_id = 1;
            $merge->update();
        }
        // Prod reject
        elseif ($merge->status_id == 2) {
            $comment = new Comment();
            $comment->mergerequest_id = $id;
            $comment->comment = $request->input('comment');
            $comment->created_by = $user;
            $comment->save();
            $merge->status_id = 7;
            $merge->prod_release_request_count = $merge->prod_release_request_count -1;
            $merge->engineer_report = "Prod Rejected by-$user";
            $merge->prod_reject_count = $merge->prod_reject_count + 1;
            $merge->update();
            $releaseLogs = new Releaselog();
            $releaseLogs->mergerequest_id = $merge->id;
            $releaseLogs->rejectreason_id  = 1;
            $releaseLogs->rejected_by = auth()->user()->id;
            $releaseLogs->save();
        }

        return response()->json([
            'status' => 200,
            'merge' => $merge,
            'rejected_by' => auth()->user()->full_name,
            'created_id' => auth()->user()->id,
        ]);
    }
    //ReleaseLogs
    public function deleteReleaseLog($id){
        $log=Releaselog::find($id);
        if($log){
            $log->delete();
            return response()->json([
                'status'=>200,
            ]);
        }else{
        return response()->json([
            'status'=> 404,
        ]);
        }
    }

    //comment
    public function updateComment($id,Request $request){
        $name = auth()->user()->id;
        $comment= new Comment();
        $comment->mergerequest_id =$id;
        $comment->comment=$request->comment;
        $comment->created_by=$name;
        $comment->save();
        return response()->json(
            [
                'status'=>200,
            ]
            );
    }
    public function showComment($id){
        $comment=Comment::with('users')->where('mergerequest_id',$id)->orderBy('id','desc')->get();
            if($comment->count()>0){
                return response()->json([
                    'status'=>200,
                    'comment'=>$comment,
                    'auth' => auth()->user()->first_name,
                ]);
            }else{
                return response()->json([
                    'status'=>404,
                    'comment'=>"Start Coversation"
                ]);
               }
    }

}
