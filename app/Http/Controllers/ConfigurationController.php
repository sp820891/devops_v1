<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\DeptRoleAssign;
use App\Models\Item;
use App\Models\PowerPointRule;
use App\Models\priority;
use App\Models\Project;
use App\Models\RejectReason;
use App\Models\Releaseenv;
use App\Models\Role;
use App\Models\Sprint;
use App\Models\Status;
use App\Models\SupportRequestStatus;
use Illuminate\Http\Request;

class ConfigurationController extends Controller
{
    public function project()
    {
        $data = Project::with('users')->get();
        if (request()->ajax()) {
            return datatables()->of($data)
                ->addColumn('action', function ($row) {
                    $actionBtn = '<button type="button" name="deleteProject"  id="' . $row->id . '"class="deleteProject text-danger " style="border:none; background:none;">
             <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" stroke="currentColor">
             <path d="M19.3248 9.46826C19.3248 9.46826 18.7818 16.2033 18.4668 19.0403C18.3168 20.3953 17.4798 21.1893 16.1088 21.2143C13.4998 21.2613 10.8878 21.2643 8.27979 21.2093C6.96079 21.1823 6.13779 20.3783 5.99079 19.0473C5.67379 16.1853 5.13379 9.46826 5.13379 9.46826" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
             <path d="M20.708 6.23975H3.75" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
             <path d="M17.4406 6.23973C16.6556 6.23973 15.9796 5.68473 15.8256 4.91573L15.5826 3.69973C15.4326 3.13873 14.9246 2.75073 14.3456 2.75073H10.1126C9.53358 2.75073 9.02558 3.13873 8.87558 3.69973L8.63258 4.91573C8.47858 5.68473 7.80258 6.23973 7.01758 6.23973" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
         </svg>
             </button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->setRowId(function ($data) {

                    return $data->id;
                })
                ->addIndexColumn()
                ->make(true);
        }
        return view('configuration.projects');
    }
    public function sprint()
    {
        $data = Sprint::with('users')->get();
        if (request()->ajax()) {
            return datatables()->of($data)
                ->addColumn('action', function ($row) {
                    $actionBtn = '<button type="button" name="deleteSprint"  id="' . $row->id . '"class="deleteSprint text-danger " style="border:none; background:none;">
                 <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" stroke="currentColor">
                 <path d="M19.3248 9.46826C19.3248 9.46826 18.7818 16.2033 18.4668 19.0403C18.3168 20.3953 17.4798 21.1893 16.1088 21.2143C13.4998 21.2613 10.8878 21.2643 8.27979 21.2093C6.96079 21.1823 6.13779 20.3783 5.99079 19.0473C5.67379 16.1853 5.13379 9.46826 5.13379 9.46826" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                 <path d="M20.708 6.23975H3.75" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                 <path d="M17.4406 6.23973C16.6556 6.23973 15.9796 5.68473 15.8256 4.91573L15.5826 3.69973C15.4326 3.13873 14.9246 2.75073 14.3456 2.75073H10.1126C9.53358 2.75073 9.02558 3.13873 8.87558 3.69973L8.63258 4.91573C8.47858 5.68473 7.80258 6.23973 7.01758 6.23973" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
             </svg>
                 </button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('configuration.sprints');
    }
    public function status()
    {
        return view('configuration.status');
    }
    public function fetchMergeStatus()
    {
        $data = Status::with('users')->get();
        if (request()->ajax()) {
            return datatables()->of($data)
                ->addColumn('action', function ($row) {
                    $actionBtn = '<button type="button" name="deleteStatus"  id="' . $row->id . '"class="deleteStatus text-danger " style="border:none; background:none;">
                 <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" stroke="currentColor">
                 <path d="M19.3248 9.46826C19.3248 9.46826 18.7818 16.2033 18.4668 19.0403C18.3168 20.3953 17.4798 21.1893 16.1088 21.2143C13.4998 21.2613 10.8878 21.2643 8.27979 21.2093C6.96079 21.1823 6.13779 20.3783 5.99079 19.0473C5.67379 16.1853 5.13379 9.46826 5.13379 9.46826" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                 <path d="M20.708 6.23975H3.75" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                 <path d="M17.4406 6.23973C16.6556 6.23973 15.9796 5.68473 15.8256 4.91573L15.5826 3.69973C15.4326 3.13873 14.9246 2.75073 14.3456 2.75073H10.1126C9.53358 2.75073 9.02558 3.13873 8.87558 3.69973L8.63258 4.91573C8.47858 5.68473 7.80258 6.23973 7.01758 6.23973" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
             </svg>
                 </button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
    }
    public function fetchSupportStatus()
    {
        $data = SupportRequestStatus::with('users')->get();
        if (request()->ajax()) {
            return datatables()->of($data)
                ->addColumn('action', function ($row) {
                    $actionBtn = '<button type="button" name="deleteSupportStatus"  id="' . $row->id . '"class="deleteSupportStatus text-danger " style="border:none; background:none;">
                 <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" stroke="currentColor">
                 <path d="M19.3248 9.46826C19.3248 9.46826 18.7818 16.2033 18.4668 19.0403C18.3168 20.3953 17.4798 21.1893 16.1088 21.2143C13.4998 21.2613 10.8878 21.2643 8.27979 21.2093C6.96079 21.1823 6.13779 20.3783 5.99079 19.0473C5.67379 16.1853 5.13379 9.46826 5.13379 9.46826" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                 <path d="M20.708 6.23975H3.75" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                 <path d="M17.4406 6.23973C16.6556 6.23973 15.9796 5.68473 15.8256 4.91573L15.5826 3.69973C15.4326 3.13873 14.9246 2.75073 14.3456 2.75073H10.1126C9.53358 2.75073 9.02558 3.13873 8.87558 3.69973L8.63258 4.91573C8.47858 5.68473 7.80258 6.23973 7.01758 6.23973" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
             </svg>
                 </button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
    }
    public function priorities()
    {
        $data = priority::with('users')->get();
        if (request()->ajax()) {
            return datatables()->of($data)
                ->addColumn('action', function ($row) {
                    $actionBtn = '<button type="button" name="deletePriority"  id="' . $row->id . '"class="deletePriority text-danger " style="border:none; background:none;">
                 <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" stroke="currentColor">
                 <path d="M19.3248 9.46826C19.3248 9.46826 18.7818 16.2033 18.4668 19.0403C18.3168 20.3953 17.4798 21.1893 16.1088 21.2143C13.4998 21.2613 10.8878 21.2643 8.27979 21.2093C6.96079 21.1823 6.13779 20.3783 5.99079 19.0473C5.67379 16.1853 5.13379 9.46826 5.13379 9.46826" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                 <path d="M20.708 6.23975H3.75" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                 <path d="M17.4406 6.23973C16.6556 6.23973 15.9796 5.68473 15.8256 4.91573L15.5826 3.69973C15.4326 3.13873 14.9246 2.75073 14.3456 2.75073H10.1126C9.53358 2.75073 9.02558 3.13873 8.87558 3.69973L8.63258 4.91573C8.47858 5.68473 7.80258 6.23973 7.01758 6.23973" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
             </svg>
                 </button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('configuration.priorities');
    }
    public function environments()
    {
        $data = Releaseenv::with('users')->get();
        if (request()->ajax()) {
            return datatables()->of($data)
                ->addColumn('action', function ($row) {
                    $actionBtn = '<button type="button" name="deleteEnv"  id="' . $row->id . '"class="deleteEnv text-danger " style="border:none; background:none;">
                 <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" stroke="currentColor">
                 <path d="M19.3248 9.46826C19.3248 9.46826 18.7818 16.2033 18.4668 19.0403C18.3168 20.3953 17.4798 21.1893 16.1088 21.2143C13.4998 21.2613 10.8878 21.2643 8.27979 21.2093C6.96079 21.1823 6.13779 20.3783 5.99079 19.0473C5.67379 16.1853 5.13379 9.46826 5.13379 9.46826" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                 <path d="M20.708 6.23975H3.75" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                 <path d="M17.4406 6.23973C16.6556 6.23973 15.9796 5.68473 15.8256 4.91573L15.5826 3.69973C15.4326 3.13873 14.9246 2.75073 14.3456 2.75073H10.1126C9.53358 2.75073 9.02558 3.13873 8.87558 3.69973L8.63258 4.91573C8.47858 5.68473 7.80258 6.23973 7.01758 6.23973" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
             </svg>
                 </button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('configuration.environments');
    }
    public function reject_reasons()
    {
        $data = RejectReason::with('users')->get();
        if (request()->ajax()) {
            return datatables()->of($data)
                ->addColumn('action', function ($row) {
                    $actionBtn = '<button type="button" name="deleteRejectReason"  id="' . $row->id . '"class="deleteRejectReason text-danger " style="border:none; background:none;">
                 <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" stroke="currentColor">
                 <path d="M19.3248 9.46826C19.3248 9.46826 18.7818 16.2033 18.4668 19.0403C18.3168 20.3953 17.4798 21.1893 16.1088 21.2143C13.4998 21.2613 10.8878 21.2643 8.27979 21.2093C6.96079 21.1823 6.13779 20.3783 5.99079 19.0473C5.67379 16.1853 5.13379 9.46826 5.13379 9.46826" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                 <path d="M20.708 6.23975H3.75" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                 <path d="M17.4406 6.23973C16.6556 6.23973 15.9796 5.68473 15.8256 4.91573L15.5826 3.69973C15.4326 3.13873 14.9246 2.75073 14.3456 2.75073H10.1126C9.53358 2.75073 9.02558 3.13873 8.87558 3.69973L8.63258 4.91573C8.47858 5.68473 7.80258 6.23973 7.01758 6.23973" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
             </svg>
                 </button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('configuration.reject-reasons');
    }
    public function departments()
    {
        $data = Department::with('users', 'roles')->get();
        $roles = Role::all();
        if (request()->ajax()) {
            return datatables()->of($data)
                ->addColumn('action', function ($row) {
                    $actionBtn = '<button type="button" name="deleteDept"  id="' . $row->id . '"class="deleteDept text-danger " style="border:none; background:none;">
                 <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" stroke="currentColor">
                 <path d="M19.3248 9.46826C19.3248 9.46826 18.7818 16.2033 18.4668 19.0403C18.3168 20.3953 17.4798 21.1893 16.1088 21.2143C13.4998 21.2613 10.8878 21.2643 8.27979 21.2093C6.96079 21.1823 6.13779 20.3783 5.99079 19.0473C5.67379 16.1853 5.13379 9.46826 5.13379 9.46826" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                 <path d="M20.708 6.23975H3.75" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                 <path d="M17.4406 6.23973C16.6556 6.23973 15.9796 5.68473 15.8256 4.91573L15.5826 3.69973C15.4326 3.13873 14.9246 2.75073 14.3456 2.75073H10.1126C9.53358 2.75073 9.02558 3.13873 8.87558 3.69973L8.63258 4.91573C8.47858 5.68473 7.80258 6.23973 7.01758 6.23973" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
             </svg>
                 </button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->setRowId(function ($data) {

                    return $data->id;
                })
                ->make(true);
        }
        return view('sprint.powerpointsystem.departments', compact('roles'));
    }

    public function power_point_rules()
    {
        $data = PowerPointRule::with('users')->get();
        if (request()->ajax()) {
            return datatables()->of($data)
                ->addColumn('action', function ($row) {
                    $actionBtn = '<button type="button" name="deleteRule"  id="' . $row->id . '"class="deleteRule text-danger " style="border:none; background:none;">
                 <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" stroke="currentColor">
                 <path d="M19.3248 9.46826C19.3248 9.46826 18.7818 16.2033 18.4668 19.0403C18.3168 20.3953 17.4798 21.1893 16.1088 21.2143C13.4998 21.2613 10.8878 21.2643 8.27979 21.2093C6.96079 21.1823 6.13779 20.3783 5.99079 19.0473C5.67379 16.1853 5.13379 9.46826 5.13379 9.46826" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                 <path d="M20.708 6.23975H3.75" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                 <path d="M17.4406 6.23973C16.6556 6.23973 15.9796 5.68473 15.8256 4.91573L15.5826 3.69973C15.4326 3.13873 14.9246 2.75073 14.3456 2.75073H10.1126C9.53358 2.75073 9.02558 3.13873 8.87558 3.69973L8.63258 4.91573C8.47858 5.68473 7.80258 6.23973 7.01758 6.23973" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
             </svg>
                 </button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('sprint.powerpointsystem.power_point_rules');
    }
    public function add_project(Request $request)
    {
        $request->validate([
            'project_name' => 'required|string|max:191',
            'project_description' => 'required|string|max:191',
        ]);
        $project = Project::create([
            'name' => $request->project_name,
            'description' => $request->project_description,
            'created_by' => auth()->user()->id
        ]);
        if ($project) {
            return response()->json([
                'status' => 200,
            ]);
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }
    public function deleteProjectId($id)
    {
        $project = Project::find($id);
        return response()->json(
            [
                'project' => $project,
                'status' => 200
            ]
        );
    }
    public function deleteProject($id)
    {
        $project = Project::find($id);
        if ($project) {
            $project->delete();
            return response()->json([
                'status' => 200,
            ]);
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }
    public function add_sprint(Request $request)
    {
        $request->validate([
            'sprint' => 'required|int|max:191|unique:sprints,name',
            'start_date' => 'required|date|max:191',
            'end_date' => 'required|date|max:191',
            'goal' => 'required|string|max:191',
        ]);
        $sprints = Sprint::create([
            'name' => $request->sprint,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'goal' => $request->goal,
            'owner' => auth()->user()->id,
        ]);
        if ($sprints) {
            return response()->json([
                'status' => 200,
            ]);
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }
    public function deleteSprintId($id)
    {
        $sprint = Sprint::find($id);
        return response()->json(
            [
                'sprint' => $sprint,
                'status' => 200
            ]
        );
    }
    public function deleteSprint($id)
    {
        $sprint = Sprint::find($id);
        if ($sprint) {
            $sprint->delete();
            return response()->json([
                'status' => 200,
            ]);
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }
    // For Mergerequest Status
    public function add_status(Request $request)
    {
        $request->validate([
            'status_name' => 'required|string|max:191',
            'status_description' => 'required|string|max:191',
        ]);
        $status = Status::create([
            'name' => $request->status_name,
            'description' => $request->status_description,
            'created_by' => auth()->user()->id,
        ]);
        if ($status) {
            return response()->json([
                'status' => 200,
            ]);
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }
    public function deleteStatusId($id)
    {
        $status = Status::find($id);
        return response()->json(
            [
                'statuses' => $status,
                'status' => 200
            ]
        );
    }
    public function deleteStatus($id)
    {
        $status = Status::find($id);
        if ($status) {
            $status->delete();
            return response()->json([
                'status' => 200,
            ]);
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }
    // For Supportrequest Status
    public function add_support_status(Request $request)
    {
        $request->validate([
            'status_name' => 'required|string|max:191',
            'status_description' => 'required|string|max:191',
        ]);
        $status = SupportRequestStatus::create([
            'name' => $request->status_name,
            'description' => $request->status_description,
            'created_by' => auth()->user()->id,
        ]);
        if ($status) {
            return response()->json([
                'status' => 200,
            ]);
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }
    public function deleteSupportStatusId($id)
    {
        $status = SupportRequestStatus::find($id);
        return response()->json(
            [
                'statuses' => $status,
                'status' => 200
            ]
        );
    }
    public function deleteSupportStatus($id)
    {
        $status = SupportRequestStatus::find($id);
        if ($status) {
            $status->delete();
            return response()->json([
                'status' => 200,
            ]);
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }
    public function add_priority(Request $request)
    {
        $request->validate([
            'priority_name' => 'required|string|max:191',
            'priority_description' => 'required|string|max:191',
        ]);
        $priority = priority::create([
            'name' => $request->priority_name,
            'description' => $request->priority_description,
            'created_by' => auth()->user()->id,
        ]);
        if ($priority) {
            return response()->json([
                'status' => 200,
            ]);
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }
    public function deletePriorityId($id)
    {
        $priority = Priority::find($id);
        return response()->json(
            [
                'priority' => $priority,
                'status' => 200
            ]
        );
    }
    public function deletePriority($id)
    {
        $priority = Priority::find($id);
        if ($priority) {
            $priority->delete();
            return response()->json([
                'status' => 200,
            ]);
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }
    public function add_env(Request $request)
    {
        $request->validate([
            'release_env_name' => 'required|string|max:191',
            'release_env_description' => 'required|string|max:191',
        ]);
        $env = ReleaseEnv::create([
            'name' => $request->release_env_name,
            'description' => $request->release_env_description,
            'created_by' => auth()->user()->id,
        ]);
        if ($env) {
            return response()->json([
                'status' => 200,
            ],);
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }
    public function deleteEnvId($id)
    {
        $env = Releaseenv::find($id);
        return response()->json(
            [
                'env' => $env,
                'status' => 200
            ]
        );
    }
    public function deleteEnv($id)
    {
        $env = Releaseenv::find($id);
        if ($env) {
            $env->delete();
            return response()->json([
                'status' => 200,
            ]);
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }
    public function add_reject_reason(Request $request)
    {
        $request->validate([
            'reject_reason_name' => 'required|string|max:191',
            'reject_reason_description' => 'required|string|max:191',
        ]);
        $reject_reason = RejectReason::create([
            'name' => $request->reject_reason_name,
            'description' => $request->reject_reason_description,
            'created_by' => auth()->user()->id
        ]);
        if ($reject_reason) {
            return response()->json([
                'status' => 200,
            ]);
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }
    public function deleteRejectReasonId($id)
    {
        $reject_reason = RejectReason::find($id);
        return response()->json(
            [
                'reject_reason' => $reject_reason,
                'status' => 200
            ]
        );
    }
    public function deleteRejectReason($id)
    {
        $reject_reason = RejectReason::find($id);
        if ($reject_reason) {
            $reject_reason->delete();
            return response()->json([
                'status' => 200,
            ]);
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }
    public function add_dept(Request $request)
    {
        $request->validate([
            'department_name' => 'required|string|max:191',
            'role' => 'required'
        ]);
        $dept = Department::create([
            'name' => $request->department_name,
            'role_id' => $request->role,
            'created_by' => auth()->user()->id
        ]);
        if ($dept) {
            return response()->json([
                'status' => 200,
            ]);
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }
    public function deleteDeptId($id)
    {
        $dept = Department::find($id);
        return response()->json(
            [
                'dept' => $dept,
                'status' => 200
            ]
        );
    }
    public function deleteDept($id)
    {
        $dept = Department::find($id);
        if ($dept) {
            $dept->delete();
            return response()->json([
                'status' => 200,
            ]);
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }
    public function add_rule(Request $request)
    {
        $request->validate([
            'rule_name' => 'required|string|max:191|unique:power_point_rules,name',
        ]);
        $rule = PowerPointRule::create([
            'name' => $request->rule_name,
            'created_by' => auth()->user()->id,
        ]);
        if ($rule) {
            return response()->json([
                'status' => 200,
            ],);
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }
    public function deleteRuleId($id)
    {
        $rule = PowerPointRule::find($id);
        return response()->json(
            [
                'rule' => $rule,
                'status' => 200
            ]
        );
    }
    public function deleteRule($id)
    {
        $rule = PowerPointRule::find($id);
        if ($rule) {
            $rule->delete();
            return response()->json([
                'status' => 200,
            ]);
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }
    }
}
