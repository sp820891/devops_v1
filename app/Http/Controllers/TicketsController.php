<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Sprint;
use App\Models\Ticket;
use App\Models\Project;
use App\Models\SpendTime;
use Illuminate\Console\View\Components\Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class TicketsController extends Controller
{
    function show(Request $request) {

        $assignee = User::all();
        $project = Project::all();
        $sprint = Sprint::all();

        if ($request->ajax()) {
            $data = Ticket::with('projects','users','creator')->get();
            return DataTables::of($data)
                    ->setRowId(function ($data) {
                        return $data->id;
                    })
                    ->addColumn('Sprint_goal', function ($data) {
                      return  $data->subject.'('.$data->issue_no.')';
                    })
                    ->rawColumns(['sprint_goal'])
                    ->make(true);
        }

        return view('tickets.tickets',compact('assignee','project','sprint'));
    }
    function store(Request $request) {
        // dd($request->all());
        $ticket = new Ticket();
        $ticket->subject = $request->subject;
        $ticket->issue_no =random_int(6000, 7200);
        $ticket->project_id = $request->project;
        $ticket->sprint_id = $request->sprint;
        $ticket->assignee = $request->assignee;
        $ticket->created_by = Auth::user()->id;
        $ticket->status = $request->status;
        $ticket->save();
        return redirect()->back();
    }

    function ticket_view($id) {
            log::info('id:'.$id);
            $ticket = Ticket::select('tickets.*','tickets.id as id')->with('projects','users','creator')->where('tickets.id',$id)->get();
            log::info($ticket);
            $spend_time = SpendTime::where('ticket_id',$id)->orderBy('created_at', 'DESC')->first();
            log::info($spend_time);
            return view('tickets.ticket-view',compact('ticket','spend_time'));
    }

    function update(Request $request){
        log::info($request);
        $spend =$request->spend_time;
         $time = new SpendTime;
            $time->ticket_id = $request->id;
            $time->spend_time = $spend;
            $time->sprint = $request->sprint;
            $time->done = $request->done;
            $time->save();
            $ticket = Ticket::find($request->id);
            $ticket->estimate_time = $request->estimate_time;
            $ticket->update();
        return redirect()->back();
    }
}
