<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use App\Http\Requests\Auth\LoginRequest;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {
        $request->authenticate();
        $request->session()->regenerate();
        $user = User::find(auth()->user()->id);
        if ($user->hasPermissionTo('can access dashboard')) {
            return redirect()->intended(RouteServiceProvider::HOME)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access merge requests')) {
            return redirect()->intended(RouteServiceProvider::MERGE)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access support requests')) {
            return redirect()->intended(RouteServiceProvider::SUPPORT)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access a project')) {
            return redirect()->intended(RouteServiceProvider::PROJECT)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access sprint')) {
            return redirect()->intended(RouteServiceProvider::SPRINT)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access environment')) {
            return redirect()->intended(RouteServiceProvider::ENV)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access priority')) {
            return redirect()->intended(RouteServiceProvider::PRIORITY)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access reject reasons')) {
            return redirect()->intended(RouteServiceProvider::REJECTREASONS )->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access logs')) {
            return redirect()->intended(RouteServiceProvider::LOG)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access status')) {
            return redirect()->intended(RouteServiceProvider::STATUS)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access users')) {
            return redirect()->intended(RouteServiceProvider::USERS)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access a roles')) {
            return redirect()->intended(RouteServiceProvider::ROLE)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else {
            Auth::logout();
            return redirect("/")->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.error("You Have No Permissions To Access DevOps Portal");</script>');
        }



    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/')->withMessage('<script>toastr.options = {
            "positionClass": "toast-bottom-right",
        }
        toastr.success("Logged Out Successfully");</script>');
    }
}
