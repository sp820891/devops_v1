<?php
namespace App\Http\Controllers;
use App\Models\Support;
use App\Models\supportDoucument;
use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Status;
use App\Models\Releaseenv;
use App\Models\priority;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Illuminate\Support\Facades\File;

class SupportRequestController extends Controller
{
    public function store(Request $request){
            $request->validate([
                'issue_no' => 'required',
            ]);
            $support = new Support();
            $support->notes=$request->input('supportnotes');
            $support->issue_url=$request->input('issue_no');
            $support->priority_id=1;
            $support->status_id=1;
            $support->project_id=1;
            $support->releaseenv_id=1;
            $support->created_by = auth()->user()->id;
            $support->save();
            if($request->file) {
                foreach ($request->file as $key => $value) {
                 $document = new supportDoucument();
                 $fileName = time() . '-' . $value->getClientOriginalName();
                 $value->move(public_path("support-uploads"), $fileName);
                 $document->Filepath = $fileName;
                 $document->supportrequest_id = $support->id;
                 $document->created_by=Auth::user()->id;
                 $document->save();
                }
             }
            return response()->json([
                'status'=>200 ,
                'message'=>'request',
                'support' => Support::with('user')->find($support->id),
                'created_id' => auth()->user()->id,
            ]);
}
public function fetchSupports(){
    $user = User::find(auth()->user()->id);
    $data = Support::with('project','user','status','releaseenv','priority')->orderBy('created_at' ,'desc')->get();
    if(request()->ajax()) {
        return datatables()->of($data)
        ->addColumn('action', function($row) use ($user){
            $btn = '';
            if ($user->hasPermissionTo('can edit support requests')) {
                $btn =   $btn . '<button type="button" name="editSupport" class="editSupport text-primary" id="' . $row->id . '"class="edit text-primary modal-user-edit" style="border:none; background:none;">
                <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M11.4925 2.78906H7.75349C4.67849 2.78906 2.75049 4.96606 2.75049 8.04806V16.3621C2.75049 19.4441 4.66949 21.6211 7.75349 21.6211H16.5775C19.6625 21.6211 21.5815 19.4441 21.5815 16.3621V12.3341" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                <path fill-rule="evenodd" clip-rule="evenodd" d="M8.82812 10.921L16.3011 3.44799C17.2321 2.51799 18.7411 2.51799 19.6721 3.44799L20.8891 4.66499C21.8201 5.59599 21.8201 7.10599 20.8891 8.03599L13.3801 15.545C12.9731 15.952 12.4211 16.181 11.8451 16.181H8.09912L8.19312 12.401C8.20712 11.845 8.43412 11.315 8.82812 10.921Z"
                stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path><path d="M15.1655 4.60254L19.7315 9.16854" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path></svg></button>';
            }
            if ($user->hasPermissionTo('can delete support requests')) {
                $btn =   $btn . '<button type="button" name="DeleteSupport"  id="'.$row->id.'"class="deleteSupport text-danger " style="border:none; background:none;">
                <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" stroke="currentColor">
                <path d="M19.3248 9.46826C19.3248 9.46826 18.7818 16.2033 18.4668 19.0403C18.3168 20.3953 17.4798 21.1893 16.1088 21.2143C13.4998 21.2613 10.8878 21.2643 8.27979 21.2093C6.96079 21.1823 6.13779 20.3783 5.99079 19.0473C5.67379 16.1853 5.13379 9.46826 5.13379 9.46826" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                <path d="M20.708 6.23975H3.75" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                <path d="M17.4406 6.23973C16.6556 6.23973 15.9796 5.68473 15.8256 4.91573L15.5826 3.69973C15.4326 3.13873 14.9246 2.75073 14.3456 2.75073H10.1126C9.53358 2.75073 9.02558 3.13873 8.87558 3.69973L8.63258 4.91573C8.47858 5.68473 7.80258 6.23973 7.01758 6.23973" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
            </svg>
                </button>';}
                return $btn;
        })
        ->rawColumns(['action'])
        ->addIndexColumn()
          ->setRowId(function ($data) {
            return $data->id;
        })
        ->make(true);
    }
}
public function editSupport($id){
    $support = Support::find($id);
     $supportdocument = supportDoucument::where('supportrequest_id',$id)->get();
    return response()->json(
        [
            'support' => $support,
            'projects' => Project::all(),
            'document'=> $supportdocument,
            'users' => User::all(),
            'statuses' => Status::all(),
            'priority' => priority::all(),
            'releaseenv' => Releaseenv::all(),
            'status' => 200
        ]
    );
}
public function updateSupport($id, Request $request)
{
        $support = Support::with('user')->find($id);
        // dd($support->all());
        $support->notes=$request->notes_edit;
        $support->project_id=$request->project_edit;
        $support->releaseenv_id=$request->releaseenv_edit;
        $support->issue_url=$request->issueurl_edit;
        $support->priority_id=$request->priority_edit;
        $support->save();
        if($request->file_edit) {
            foreach ($request->file_edit as $key => $value) {
             $document = new supportDoucument();
             $fileName = time() . '-' . $value->getClientOriginalName();
             $value->move(public_path("support-uploads"), $fileName);
             $document->Filepath = $fileName;
             $document->supportrequest_id = $support->id;
             $document->created_by=Auth::user()->id;
             $document->save();
            }
         }
        return response()->json([
            'status'=>200 ,
            'support'=> $support,
            'created_id' => auth()->user()->id,
            'message'=>'successfully'
        ]);
}
public function deleteSupport($id){
        $support = Support::find($id);
        $documents = supportDoucument::where('supportrequest_id',$id);
        // foreach($documents as $key=>$val) {
        //     if(File::exists($val->path)) {
        //         File::delete($val->path);
        //     }
        // }
        $documents->delete();
        $support->delete();
        return response()->json([
        'status' => 200,
        ]);
}
public function supportView($id){
    $supportrequest=Support::with('Project','Status','Releaseenv','priority')->where('id',$id)->first();
    $supportdocument = supportDoucument::where('supportrequest_id',$id)->get();
    return view('dashboards.supportView',compact('supportrequest','supportdocument'));

}


public function supportApprove($id)
    {
        $support = Support::find($id);
        $user = Auth::user()->full_name;

        // Support approve//
        if ($support->status_id == 1) {
            $support->status_id = 2;
            $support->engineer_report = "Approved by-$user";
            $support->update();
            return response()->json([
                'status' => 200,
                'support' => $support,
                'approved_by' => auth()->user()->full_name,
                'created_id' => auth()->user()->id,
            ]);

        }

    }
public function supportReject($id)
    {
        $support = Support::find($id);
        $user = Auth::user()->full_name;

        // Support approve//
        if ($support->status_id == 1) {
            $support->status_id = 3;
            $support->engineer_report = "Approved by-$user";
            $support->update();
            return response()->json([
                'status' => 200,
                'support' => $support,
                'rejected_by' => auth()->user()->full_name,
                'created_id' => auth()->user()->id,
            ]);

        }

    }
    public function docdeleted($id){
    $support = supportDoucument::find($id);
    $support->delete();
    return redirect()->back()->with('success', 'Support Request documentation  successfully deleted');
 }
}

