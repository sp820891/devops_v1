<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ApplicationNotification extends Notification
{
    use Queueable;
    public $notify_id,$url_name,$message,$created_by;
    /**
     * Create a new notification instance.
     */
    public function __construct($notify_id,$url_name,$message,$created_by)
    {
        $this->notify_id = $notify_id;
        $this->url_name = $url_name;
        $this->message = $message;
        $this->created_by = $created_by;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            "notify_id" => $this->notify_id,
            "url_name" => $this->url_name,
            "message" => $this->message,
            "created_by" => $this->created_by,
        ];
    }
}
