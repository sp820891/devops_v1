<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NotificationEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $notify_id,$url_name,$message,$created_by,$notification_from,$created_id;
    /**
     * Create a new event instance.
     */
    public function __construct($notify_id,$url_name,$message,$created_by,$notification_from,$created_id)
    {
        $this->notify_id = $notify_id;
        $this->url_name = $url_name;
        $this->message = $message;
        $this->created_by = $created_by;
        $this->notification_from = $notification_from;
        $this->created_id = $created_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return array<int, \Illuminate\Broadcasting\Channel>
     */
    public function broadcastOn(): array
    {
        return [
            new Channel('notification-channel'),
        ];
    }
    public function broadcastAs() {
        return "notification-broadcast";
    }
}
