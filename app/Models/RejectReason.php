<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RejectReason extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'description',
        'status',
        'created_by'
    ];
    public function users()
    {
        return $this->belongsTo(User::class,'created_by','id');
    }
}
