<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SprintRetrospective extends Model
{
    use HasFactory;
    protected   $table = 'sprint_retrospective';

    public function users()
    {
        return $this->belongsTo(user::class, 'user_id', 'id');
    }

    public function sprint()
    {
        return $this->belongsTo(Sprint::class, 'sprint_id', 'id');
    }

    public function rules()
    {
        return $this->belongsTo(PowerPointRule::class, 'pp_rule_id', 'id');
    }

}
