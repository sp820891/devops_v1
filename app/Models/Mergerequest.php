<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
class Mergerequest extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    use HasFactory;
    public function document()
    {
       return $this->hasMany(Document::class,'mergerequest_id','id');
    }
    public function projects()
    {
        return $this->belongsTo(Project::class,'project_id','id');
    }

         /**
         * Get the user that owns the Mergrequest
         *
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function release_env()
    {
        return $this->belongsTo(Releaseenv::class, 'releaseenv_id', 'id');
    }
    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id', 'id');
    }
    public function priority()
    {
        return $this->belongsTo(priority::class, 'priority_id', 'id');
    }


}
