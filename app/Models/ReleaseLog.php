<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
class Releaselog extends Model implements Auditable
{
    use SoftDeletes;
    use HasApiTokens, HasFactory, Notifiable, HasRoles;
    use \OwenIt\Auditing\Auditable;
    protected $fillable = [
        'mergerequest_id',
        'rejectreason_id',
        'rejected_by'
    ];
    public function users()
    {
        return $this->belongsTo(User::class,'rejected_by','id');
    }
    public function mergerequest()
    {
        return $this->belongsTo(Mergerequest::class,'mergerequest_id','id');
    }
    public function reason()
    {
        return $this->belongsTo(RejectReason::class,'rejectreason_id','id');
    }
}
