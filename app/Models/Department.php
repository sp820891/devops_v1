<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\Permission\Traits\HasRoles;

class Department extends Model implements Auditable
{
    use SoftDeletes;
    use HasApiTokens, HasFactory, Notifiable, HasRoles;
    use \OwenIt\Auditing\Auditable;
    protected $fillable = [
        'name',
        'description',
        'role_id',
        'status',
        'created_by'
    ];
    public function users()
    {
        return $this->belongsTo(User::class,'created_by','id');
    }
    public function roles()
    {
        return $this->belongsTo(Role::class,'role_id','id');
    }
}
