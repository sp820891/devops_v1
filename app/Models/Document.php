<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
class Document extends Model implements Auditable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles,SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    protected $fillable = [
        'mergerequest_id ',
        'name',
        'description',
        'path',
        'status',
        'created_by'
    ];
}
