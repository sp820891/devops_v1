<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Project;
use App\Models\Status;
use App\Models\ReleaseEnv;
use App\Models\Priority;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
class Support extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    use HasFactory;

    protected $table="supportrequest";
    protected $fillable = [
        'notes','status_id','project_id','releaseenv_id','issue_url','priority_id','Filepath',
     ];
      public function project(){
        return $this->belongsTo(Project::class,'project_id');
      }
      public function status(){
        return $this->belongsTo(SupportRequestStatus::class,'status_id');
      }
      public function releaseenv(){
        return $this->belongsTo(ReleaseEnv::class,'releaseenv_id');
      }
      public function priority(){
        return $this->belongsTo(Priority::class,'priority_id');
      }
      public function user() {
        return $this->belongsTo(User::class,'created_by');
      }
}
