<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PowerPointComments extends Model
{
    use HasFactory;
    use SoftDeletes;
    public function users()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
}
