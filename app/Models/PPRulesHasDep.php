<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PPRulesHasDep extends Model
{
    use HasFactory;

    protected $table = 'power_point_rules_has_departments';

    public function rules() {

            return $this->belongsTo(PowerPointRule::class, 'powerpoint_rule_id' , 'id');
    }

}
