<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
class Sprint extends Model implements Auditable
{
    use SoftDeletes;
    use HasApiTokens, HasFactory, Notifiable, HasRoles;
    use \OwenIt\Auditing\Auditable;
    protected $fillable = [
        'name',
        'start_date',
        'end_date',
        'goal',
        'owner',
        'status'
    ];
    public function users()
    {
        return $this->belongsTo(User::class,'owner','id');
    }
}
