<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;

    public function projects()
    {
        return $this->belongsTo(Project::class,'project_id','id');
    }
    public function users()
    {
        return $this->belongsTo(User::class, 'assignee', 'id');
    }
    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
    public function sprint()
    {
        return $this->belongsTo(Sprint::class, 'sprint_id', 'id');
    }
}
